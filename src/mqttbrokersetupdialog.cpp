#include <QString>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QDebug>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

#include "settings.h"

#include "mqttbrokersetupdialog.h"
#include "ui_mqttbrokersetupdialog.h"

MqttBrokerSetupDialog::MqttBrokerSetupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MqttBrokerSetupDialog)
{
    ui->setupUi(this);
    setWindowTitle(tr("MQTT broker setup"));

    _originalHostname = Settings::instance().mqttBroker();
    _originalPort = static_cast<int16_t>(Settings::instance().mqttPort());
    _originalTlsIsEnabled = Settings::instance().tlsEnabled();
    _originalCAFilePath = Settings::instance().caFilePath();
    _originalCADirPath = Settings::instance().caDirPath();

    ui->hostnameLineEdit->setText(_originalHostname);
    ui->portSpinBox->setMaximum(std::numeric_limits<int16_t>::max());
    ui->portSpinBox->setMinimum(0);
    ui->portSpinBox->setValue(_originalPort);

    ui->useTlsCheckBox->setChecked(_originalTlsIsEnabled);
    ui->tlsGroupBox->setEnabled(_originalTlsIsEnabled);
    connect(ui->useTlsCheckBox, &QCheckBox::stateChanged, this, &MqttBrokerSetupDialog::enableTls);

    ui->caFileLineEdit->setText(_originalCAFilePath);
    ui->caPathLineEdit->setText(_originalCADirPath);

    connect(ui->searchCAFilePushButton, &QPushButton::clicked, this, &MqttBrokerSetupDialog::searchCAFile);
    connect(ui->searchCAPathPushButton, &QPushButton::clicked, this, &MqttBrokerSetupDialog::searchCAPath);

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

MqttBrokerSetupDialog::~MqttBrokerSetupDialog()
{
    delete ui;
}

void MqttBrokerSetupDialog::enableTls(int state)
{
    if(state == Qt::Unchecked)
        ui->tlsGroupBox->setEnabled(false);
    else
        ui->tlsGroupBox->setEnabled(true);
}

void MqttBrokerSetupDialog::accept()
{
    if((ui->hostnameLineEdit->text() != _originalHostname) ||
       (ui->portSpinBox->value() != _originalPort) ||
       (ui->useTlsCheckBox->isChecked() != _originalTlsIsEnabled) ||
       (ui->caFileLineEdit->text() != _originalCAFilePath) ||
       (ui->caPathLineEdit->text() != _originalCADirPath))
    {
        if(ui->useTlsCheckBox->isChecked() &&
           ui->caFileLineEdit->text().isEmpty() &&
           ui->caPathLineEdit->text().isEmpty())
        {
            QMessageBox::warning(this, tr("Configuration error detected."),
                                 tr("When TLS is enabled either the CA file path "
                                    "or the CA dir path should not be empty."));
            return;
        }

        Settings::instance().setMqttBroker(ui->hostnameLineEdit->text());
        Settings::instance().setMqttPort(static_cast<int16_t>(ui->portSpinBox->value()));

        Settings::instance().setTlsEnabled(ui->useTlsCheckBox->isChecked());
        Settings::instance().setCAFilePath(ui->caFileLineEdit->text());
        Settings::instance().setCADirPath(ui->caPathLineEdit->text());

        QMessageBox::information(this, tr("MQTT broker setup with changes - restart is required."),
                                 tr("Changes to the configuration where detected, you need to "
                                    "restart the application in order to get them to work."));
    }

    deleteLater();
    QDialog::accept();
}

void MqttBrokerSetupDialog::reject()
{
    deleteLater();
    QDialog::reject();
}

void MqttBrokerSetupDialog::searchCAFile()
{
    QString path = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).size() > 0
                   ? QStandardPaths::standardLocations(QStandardPaths::HomeLocation).at(0) : "/home";
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select CA certificate"),
                                                    path);
    ui->caFileLineEdit->setText(fileName);
}

void MqttBrokerSetupDialog::searchCAPath()
{
    QString path = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).size() > 0
                   ? QStandardPaths::standardLocations(QStandardPaths::HomeLocation).at(0) : "/home";
    QString dir = QFileDialog::getExistingDirectory(this, tr("Select directory with CA certificates"), path);

    ui->caPathLineEdit->setText(dir);
}
