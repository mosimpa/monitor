#ifndef SOUNDALARMHANDLER_H
#define SOUNDALARMHANDLER_H

#include <QObject>

class QSound;
class QTimer;

class SoundAlarmHandler : public QObject
{
    Q_OBJECT
public:
    explicit SoundAlarmHandler(QObject *parent = nullptr);

public slots:
    void handleInternmentDeviceAlarmStatus(bool playDeviceAlarm, bool playIntermentAlarm);
    void handleDatakeeperOfflineAlarmStatus(bool isOffline);

private:
    void playSoundAlarms();
    bool checkAlarmsTimerState();

    /*
     * By some strange reason QSound sometimes blocks, sometimes segfaults if
     * run trough SSH or while running tests while building the packages.
     *
     * We don't need them for tests, so let's just not use them.
     */
#ifndef MONITOR_TEST
    QSound * mInternmentAlarmSound;
    QSound * mDeviceAlarmSound;
    QSound * mDatakeeperOfflineSound;
#endif

    bool mPlayInternmentAlarm;
    bool mPlayDeviceAlarm;
    bool mPlayDatakeeperOfflineAlarm;
    const int ALARM_PERIOD_MS = 3000;
    int mAlarmCounter;
    QTimer * mAlarmTimer;

    friend class SoundAlarmHandlerTests;
};

#endif // SOUNDALARMHANDLER_H
