#ifndef MUTEBUTTON_H
#define MUTEBUTTON_H

#include <QWidget>
#include <QPushButton>
#include <QModelIndex>

#include <mosimpaqt/definitions.h>

using namespace MoSimPa;

class MuteButton : public QPushButton
{
    Q_OBJECT

public:
    MuteButton(db_id_t internmentId, bool muted, QWidget * parent = nullptr);

    db_id_t internmentId() const { return _internmentId; }

private:
    void changeIcon(bool checked);
    db_id_t _internmentId;
};

#endif // MUTEBUTTON_H
