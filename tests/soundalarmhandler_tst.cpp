#include <QMetaType>
#include <QString>
#include <QTest>
#include <QVector>
#include <QAbstractItemModel>
#include <QScopedPointer>
#include <QVariant>
#include <QColor>
#include <QSignalSpy>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "../src/soundalarmhandler.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "soundalarmhandler_tst.h"

void SoundAlarmHandlerTests::checkCheckAlarmsTimerState()
{
    auto im = new SoundAlarmHandler(this);

    // All alarms off.
    im->mPlayDeviceAlarm = false;
    im->mPlayInternmentAlarm = false;
    im->mPlayDatakeeperOfflineAlarm = false;
    QVERIFY2(im->checkAlarmsTimerState() == false, "There should not be a running timer.");

    /*
     * Tests without datakeeper offline alarm.
     */

    // The device alarm alone should trigger the timer.
    im->mPlayDeviceAlarm = true;
    QVERIFY2(im->checkAlarmsTimerState() == true, "There should be a running timer.");

    // Which keeps being true if the internment alarm is also setted.
    im->mPlayInternmentAlarm = true;
    QVERIFY2(im->checkAlarmsTimerState() == true, "There should be a running timer.");

    // Check the other combination.
    im->mPlayDeviceAlarm = false;
    QVERIFY2(im->checkAlarmsTimerState() == true, "There should be a running timer.");

    // Put them back to turned off.
    im->mPlayInternmentAlarm = false;
    QVERIFY2(im->checkAlarmsTimerState() == false, "There should not be a running timer.");

    /*
     * Tests with datakeeper offline alarm.
     */
    im->mPlayDeviceAlarm = false;
    im->mPlayInternmentAlarm = false;
    im->mPlayDatakeeperOfflineAlarm = true;

    // The datakeeper offline alarm should trigger the timer.
    QVERIFY2(im->checkAlarmsTimerState() == true, "There should be a running timer.");

    // All the alarms on should also trigger the timer.
    im->mPlayDeviceAlarm = true;
    im->mPlayInternmentAlarm = true;
    QVERIFY2(im->checkAlarmsTimerState() == true, "There should be a running timer.");
}

QTEST_MAIN(SoundAlarmHandlerTests)
#include "soundalarmhandler_tst.moc"
