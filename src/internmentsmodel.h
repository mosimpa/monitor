#ifndef INTERNMENTSMODEL_H
#define INTERNMENTSMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QChar>
#include <QString>
#include <QVector>
#include <QHash>
#include <QFlags>
#include <QColor>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>

#include <readsparser.h>

#include "datakeeperalarmsparser.h"

using namespace MoSimPa;

class InternmentsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    /// Bitwise flags.
    enum AlarmFlag {
        NoAlarm = 0x0,
        BatteryLow = 0x1,
        DeviceMissing = 0x2,
    };

    Q_DECLARE_FLAGS(AlarmsFlags, AlarmFlag)
    Q_FLAG(AlarmsFlags)

    enum Columns {
        InternmentID = 0,
        Age,
        Gender,
        LocDesc,
        LocType,
        DeviceMAC,
        DeviceAlarms,
        MuteDeviceButton,
        SpO2,
        SpO2MuteButton,
        HR,
        HRMuteButton,
        BT,
        BTMuteButton,
        PlotButton,
        AlarmsSetupButton,
        RequestReportButton,
        Max, // The rest of the columns are not shown.
        /*
         * We seems not able to show patient's name and surname due to current
         * argentinian legislation...
         */
        Surname,
        Name,
        // We will currently not show blood pressure.
        BPSys,
        BPDias
    };

    struct InternmentData {
        int32_t internmentId;
        time_t fromTime;

        QString device;
        DatakeeperAlarmsParser::DeviceData deviceData;

        QString locDesc;
        QString locType;

        QString name;
        QString surname;
        uint8_t age;
        QChar gender;

        ReadsParser::SpO2Values spo2Values;
        ReadsParser::HeartRateValues heartRateValues;
        ReadsParser::BloodPressureValues bloodPressureValues;
        ReadsParser::BodyTempValues bodyTempValues;

        DatakeeperAlarmsParser::AlarmSeverity spO2Severity;
        DatakeeperAlarmsParser::AlarmSeverity heartRateSeverity;
        DatakeeperAlarmsParser::AlarmSeverity bodyTempSeverity;
        DatakeeperAlarmsParser::AlarmSeverity bloodPressureSeverity;

        bool spo2AlarmMuted;
        bool hrAlarmMuted;
        bool btAlarmMuted;
        bool deviceAlarmMuted;
    };

    explicit InternmentsModel(QObject *parent = nullptr);
    ~InternmentsModel();

    [[nodiscard]] bool parsePayload(const mosquitto_message *msg);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    struct InternmentData device(const int row) const;
    QVector<QString> devices();

    void updateInstantValues(const ReadsParser & parser);
    void updateInternmentsAlarms(const QVector<DatakeeperAlarmsParser::InternmentsAlarms> & alarms);
    void updateDevicesAlarms(QVector<DatakeeperAlarmsParser::DeviceData> devices);

    void muteSpO2Alarm(const db_id_t internmentId, const bool mute);
    void muteHRAlarm(const db_id_t internmentId, const bool mute);
    void muteBTAlarm(const db_id_t internmentId, const bool mute);
    void muteDeviceAlarm(const db_id_t internmentId, const bool mute);

    int32_t internmentIdFromMac(QString mac) const;

signals:
    void currentSeverity(const DatakeeperAlarmsParser::AlarmSeverity severity);
    void playSoundAlarms(bool playDeviceAlarm, bool playIntermentAlarm);

private:
    void clearData();
    void checkAlarmSoundsState();
    static QVariant severityToColor(const DatakeeperAlarmsParser::AlarmSeverity severity);
    static QColor colorNoData() { return QColor("gray"); }

    QString mId;
    QVector<InternmentData*> mData;
    QHash<QString, InternmentData*> mMacHash;
    QHash<db_id_t, InternmentData*> mInternmentIdHash;

    friend class InternmentsModelTests;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(InternmentsModel::AlarmsFlags)
Q_DECLARE_METATYPE(InternmentsModel::AlarmFlag)

#endif // INTERNMENTSMODEL_H
