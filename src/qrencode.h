#ifndef QRENCODE_H
#define QRENCODE_H

#include <QString>
#include <QObject>
#include <QProcess>
#include <QPixmap>

class QTemporaryFile;

class QREncode : public QObject
{
    Q_OBJECT
public:

    enum ErrorCorrectionLevel {
        LevelL = 0,
        LevelM,
        LevelQ,
        LevelH
    };

    explicit QREncode(QObject *parent = nullptr);

    void encode(QString string, int dotSize = 5, ErrorCorrectionLevel level = LevelH);

signals:
    void qrCodeGenerated(const QPixmap & image);

private slots:
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    QProcess * mProcess;
    QTemporaryFile * mFile;
};

#endif // QRENCODE_H
