#include <QPushButton>

#include "mutebutton.h"

/**
 * @brief MuteButton::MuteButton
 * @param index The index of the model it refers to.
 * @param parent
 */
MuteButton::MuteButton(db_id_t internmentId, bool muted, QWidget *parent) : QPushButton(parent), _internmentId(internmentId)
{
    setCheckable(true);
    setChecked(muted);
    changeIcon(muted);
    connect(this, &MuteButton::clicked, this, &MuteButton::changeIcon);
}

void MuteButton::changeIcon(bool checked)
{
    if(checked)
        setIcon(QIcon(":/images/no_sound.png"));
    else
        setIcon(QIcon(":/images/sound.png"));
}
