#ifndef DATAKEEPERALARMSPARSER_H
#define DATAKEEPERALARMSPARSER_H

#include <QObject>
#include <QFlags>
#include <QString>
#include <QDateTime>
#include <QVector>
#include <QMap>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>

using namespace MoSimPa;

class DatakeeperAlarmsParser : public QObject
{
    Q_OBJECT
public:
    enum TypeOfAlarm {
        AlarmUnknown = 0,
        AlarmInternments,
        AlarmDevices,
    };
    Q_ENUM(TypeOfAlarm)

    enum AlarmSeverity {
        Green = 0,
        Yellow = 1,
        Orange = 2,
        Red = 3
    };
    Q_ENUM(AlarmSeverity)

    enum AlarmsValues {
        SpO2 = 0,
        HeartRate,
        BodyTemp,
        BloodPressure
    };
    Q_ENUM(AlarmsValues)

    struct InternmentsAlarms {
        db_id_t internmentId;
        QMap<AlarmsValues, AlarmSeverity> alarms;
    };

    struct DeviceData {
        QString mac;
        QDateTime lastSeen;
        uint16_t battmV;
        AlarmSeverity devMissingAlarmLevel;
        AlarmSeverity battLowAlarmLevel;
    };

    static QString topic() { return QStringLiteral("datakeeper/alarms"); }
    static TypeOfAlarm typeOfAlarm(const mosquitto_message *msg);
    static QVector<InternmentsAlarms> parseInternmentsAlarmsMessage(const mosquitto_message * msg);
    static QVector<DeviceData> parseDeviceAlarmMessage(const mosquitto_message *msg);
    static AlarmSeverity parseAlarmSeverity(const QString & severity);
    static AlarmSeverity mostSeverityAlarm(const DeviceData & data);

};

#endif // DATAKEEPERALARMSPARSER_H
