#include <QWidget>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QDialogButtonBox>
#include <QDebug>

#include <qwt_slider.h>

#include <mosimpaqt/scaling.h>
#include <mosimpaqt/definitions.h>

#include "dataengine.h"
#include "alarmsdialog.h"
#include "ui_alarmsdialog.h"

/**
 * @brief AlarmsDialog::AlarmsDialog
 * @param dataEngine Pointer to active DataEngine. This class will not take
 * property of it, the engine must be valid at all times.
 * @param data
 * @param parent
 *
 * \todo Set group ticks in sliders.
 */
AlarmsDialog::AlarmsDialog(DataEngine *dataEngine, const InternmentsModel::InternmentData &data, const DatakeeperQueriesParser::AlarmsRanges &alarmsRanges, QWidget *parent) :
    QDialog(parent), ui(new Ui::AlarmsDialog), mDataEngine(dataEngine), mData(data), mAlarmsRanges(alarmsRanges)
{
    int counter;
    int value;
    ui->setupUi(this);
    setWindowTitle(tr("Alarms configuration"));

    connect(mDataEngine, &DataEngine::alarmsThresholdsReceived, this, &AlarmsDialog::applyCurrentThresholds);
    // Request current alarm values.
    mDataEngine->requestAlarmsThresholds(data.internmentId);

    QString patient = QString("%1, %2").arg(data.surname).arg(data.name);
    setInformation(data.age, data.gender,
                   QString("%1, %2").arg(data.locDesc).arg(data.locType),
                   data.device);

    ui->oxygenSatSpinBox->setDecimals(0);
    ui->heartRateLTSpinBox->setDecimals(0);
    ui->heartRateGTSpinBox->setDecimals(0);
    ui->bodyTemperatureLTSpinBox->setDecimals(1);
    ui->bodyTemperatureGTSpinBox->setDecimals(1);

    ui->oxygenDelayComboBox->setEditable(false);
    ui->heartRateDelayComboBox->setEditable(false);
    ui->bodyTempDelayComboBox->setEditable(false);

    // Set ranges.
    ui->oxygenSatSlider->setScale(alarmsRanges.spO2EqOrLessPercBot, alarmsRanges.spO2EqOrLessPercTop);
    ui->oxygenSatSpinBox->setRange(alarmsRanges.spO2EqOrLessPercBot, alarmsRanges.spO2EqOrLessPercTop);

    counter = 0;
    value = alarmsRanges.spO2DelaySMin;
    while(value <= alarmsRanges.spO2DelaySMax)
    {
        ui->oxygenDelayComboBox->insertItem(counter, QString("%1").arg(value));
        counter++;
        value += ALARM_DELAY_STEP;
    }

    ui->heartRateLTSlider->setScale(alarmsRanges.heartRateEqOrLessBPMBot, alarmsRanges.heartRateEqOrLessBPMTop);
    ui->heartRateLTSpinBox->setRange(alarmsRanges.heartRateEqOrLessBPMBot, alarmsRanges.heartRateEqOrLessBPMTop);

    ui->heartRateGTSlider->setScale(alarmsRanges.heartRateEqOrMoreBPMBot, alarmsRanges.heartRateEqOrMoreBPMTop);
    ui->heartRateGTSpinBox->setRange(alarmsRanges.heartRateEqOrMoreBPMBot, alarmsRanges.heartRateEqOrMoreBPMTop);

    counter = 0;
    value = alarmsRanges.heartRateDelaySMin;
    while(value <= alarmsRanges.heartRateDelaySMax)
    {
        ui->heartRateDelayComboBox->insertItem(counter, QString("%1").arg(value));
        counter++;
        value += ALARM_DELAY_STEP;
    }

    ui->bodyTemperatureLTSlider->setScale(alarmsRanges.bodyTempEqOrLessCelsBot, alarmsRanges.bodyTempEqOrLessCelsTop);
    ui->bodyTemperatureLTSpinBox->setRange(alarmsRanges.bodyTempEqOrLessCelsBot, alarmsRanges.bodyTempEqOrLessCelsTop);

    ui->bodyTemperatureGTSlider->setScale(alarmsRanges.bodyTempEqOrMoreCelsBot, alarmsRanges.bodyTempEqOrMoreCelsTop);
    ui->bodyTemperatureGTSpinBox->setRange(alarmsRanges.bodyTempEqOrMoreCelsBot, alarmsRanges.bodyTempEqOrMoreCelsTop);

    counter = 0;
    value = alarmsRanges.bodyTempDelaySMin;
    while(value <= alarmsRanges.bodyTempDelaySMax)
    {
        ui->bodyTempDelayComboBox->insertItem(counter, QString("%1").arg(value));
        counter++;
        value += ALARM_DELAY_STEP;
    }

    // Signals and slots.
    connect(ui->oxygenSatSlider, &QwtSlider::valueChanged, ui->oxygenSatSpinBox, &QDoubleSpinBox::setValue);
    connect(ui->oxygenSatSpinBox,  SIGNAL(valueChanged(double)), ui->oxygenSatSlider, SLOT(setValue(double)));

    connect(ui->heartRateLTSlider, &QwtSlider::valueChanged, this, &AlarmsDialog::heartRateLTChanged);
    connect(ui->heartRateLTSpinBox, SIGNAL(valueChanged(double)), this, SLOT(heartRateLTChanged(double)));

    connect(ui->heartRateGTSlider, &QwtSlider::valueChanged, this, &AlarmsDialog::heartRateGTChanged);
    connect(ui->heartRateGTSpinBox, SIGNAL(valueChanged(double)), this, SLOT(heartRateGTChanged(double)));

    connect(ui->bodyTemperatureLTSlider, &QwtSlider::valueChanged, this, &AlarmsDialog::bodyTemperatureLTChanged);
    connect(ui->bodyTemperatureLTSpinBox, SIGNAL(valueChanged(double)), this, SLOT(bodyTemperatureLTChanged(double)));

    connect(ui->bodyTemperatureGTSlider, &QwtSlider::valueChanged, this, &AlarmsDialog::bodyTemperatureGTChanged);
    connect(ui->bodyTemperatureGTSpinBox, SIGNAL(valueChanged(double)), this, SLOT(bodyTemperatureGTChanged(double)));

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &AlarmsDialog::save);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &AlarmsDialog::reject);

    ui->spO2GroupBox->setEnabled(false);
    ui->heartRateGoupBox->setEnabled(false);
    ui->bodyTempGroupBox->setEnabled(false);
}

AlarmsDialog::~AlarmsDialog()
{
    delete ui;
}

void AlarmsDialog::applyCurrentThresholds(DatakeeperQueriesParser::AlarmsThresholds thresholds)
{
    int index;
    if(thresholds.internmentId != mData.internmentId)
        return;

    mThresholds = thresholds;

    ui->spO2GroupBox->setEnabled(true);
    ui->heartRateGoupBox->setEnabled(true);
    ui->bodyTempGroupBox->setEnabled(true);

    // Set current values.
    ui->oxygenSatSlider->setValue(Scaling::spO2WireToPercentage(mThresholds.spO2Lt));
    ui->oxygenSatSpinBox->setValue(Scaling::spO2WireToPercentage(mThresholds.spO2Lt));

    index = (thresholds.spO2DelayS - mAlarmsRanges.spO2DelaySMin) / ALARM_DELAY_STEP;
    ui->oxygenDelayComboBox->setCurrentIndex(index);

    setHeartRateLTValue(Scaling::heartRateWireToBPM(mThresholds.hrLt));
    setHeartRateGTValue(Scaling::heartRateWireToBPM(mThresholds.hrGt));
    index = (thresholds.hRDelayS - mAlarmsRanges.heartRateDelaySMin) / ALARM_DELAY_STEP;
    ui->heartRateDelayComboBox->setCurrentIndex(index);

    setBodyTemperatureLTValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTLt));
    setBodyTemperatureGTValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTGt));

    ui->bodyTemperatureLTSlider->setValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTLt));
    ui->bodyTemperatureLTSpinBox->setValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTLt));
    ui->bodyTemperatureGTSlider->setValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTGt));
    ui->bodyTemperatureGTSpinBox->setValue(Scaling::bodyTemperatureWireToDegCelsius(mThresholds.bTGt));
    index = (thresholds.bTDelayS - mAlarmsRanges.bodyTempDelaySMin) / ALARM_DELAY_STEP;
    ui->bodyTempDelayComboBox->setCurrentIndex(index);
}

void AlarmsDialog::heartRateLTChanged(double value)
{
    if(value > mAlarmsRanges.heartRateEqOrLessBPMTop)
        value = mAlarmsRanges.heartRateEqOrLessBPMTop;

    if(value >= mHeartRateGT - 10)
        value = mHeartRateGT - 10;

    setHeartRateLTValue(value);
}

void AlarmsDialog::heartRateGTChanged(double value)
{
    if(value <= mHeartRateLT + 10)
        value = mHeartRateLT + 10;

    setHeartRateGTValue(value);
}

void AlarmsDialog::setHeartRateLTValue(double value)
{
    mHeartRateLT = value;

    ui->heartRateLTSlider->setValue(mHeartRateLT);
    ui->heartRateLTSpinBox->setValue(mHeartRateLT);
}

void AlarmsDialog::setHeartRateGTValue(double value)
{
    mHeartRateGT = value;

    ui->heartRateGTSlider->setValue(mHeartRateGT);
    ui->heartRateGTSpinBox->setValue(mHeartRateGT);
}

void AlarmsDialog::bodyTemperatureLTChanged(double value)
{
    if(value >= mBodyTempGT -1)
        value = mBodyTempGT -1;

    setBodyTemperatureLTValue(value);
}

void AlarmsDialog::bodyTemperatureGTChanged(double value)
{
    if(value <= mBodyTempLT + 1)
        value = mBodyTempLT + 1;

    setBodyTemperatureGTValue(value);
}

void AlarmsDialog::setBodyTemperatureLTValue(double value)
{
    mBodyTempLT = value;

    ui->bodyTemperatureLTSlider->setValue(value);
    ui->bodyTemperatureLTSpinBox->setValue(value);
}

void AlarmsDialog::setBodyTemperatureGTValue(double value)
{
    mBodyTempGT = value;

    ui->bodyTemperatureGTSlider->setValue(value);
    ui->bodyTemperatureGTSpinBox->setValue(value);
}

void AlarmsDialog::save()
{
    mThresholds.spO2Lt = Scaling::spO2PercentageToWire(ui->oxygenSatSlider->value());
    mThresholds.spO2DelayS = ui->oxygenDelayComboBox->currentIndex() * ALARM_DELAY_STEP + mAlarmsRanges.spO2DelaySMin;

    mThresholds.hrLt = Scaling::heartRateBPMToWire(ui->heartRateLTSlider->value());
    mThresholds.hrGt = Scaling::heartRateBPMToWire(ui->heartRateGTSlider->value());
    mThresholds.hRDelayS = ui->heartRateDelayComboBox->currentIndex() * ALARM_DELAY_STEP + mAlarmsRanges.heartRateDelaySMin;

    mThresholds.bTLt = Scaling::bodyTemperatureDegCelsiusToWire(ui->bodyTemperatureLTSlider->value());
    mThresholds.bTGt = Scaling::bodyTemperatureDegCelsiusToWire(ui->bodyTemperatureGTSlider->value());
    mThresholds.bTDelayS = ui->bodyTempDelayComboBox->currentIndex() * ALARM_DELAY_STEP + mAlarmsRanges.bodyTempDelaySMin;

    mDataEngine->sendAlarmsThresholds(mThresholds);
    emit dialogClosed();
}

void AlarmsDialog::setInformation(const uint8_t age, const QString &gender, const QString &bed, const QString &deviceId)
{
    ui->ageLabel->setText(tr("Age: %1").arg(age));
    ui->genderLabel->setText(tr("Gender: %1").arg(gender));
    ui->bedLabel->setText(tr("Bed: %1").arg(bed));
    ui->deviceIdLabel->setText(tr("Dev. ID: %1").arg(deviceId));
}
