#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QNetworkInterface>
#include <QString>
#include <QAbstractItemModel>
#include <QDateTime>

#include <readsparser.h>
#include <mosquittoclient.h>

#include "datakeeperqueriesparser.h"
#include "datakeeperalarmsparser.h"
#include "signalsparser.h"
#include "internmentsmodel.h"
#include "dataengine.h"
#include "settings.h"
#include "soundalarmhandler.h"

DataEngine::DataEngine(QObject *parent) : QObject(parent)
{
    mMosqClient = new MosquittoClient(false, this);
    mInternments = new InternmentsModel(this);
    mSoundAlarmHandler = new SoundAlarmHandler(this);

    QObject::connect(mInternments, &InternmentsModel::playSoundAlarms,
                     mSoundAlarmHandler, &SoundAlarmHandler::handleInternmentDeviceAlarmStatus);

    mHBTimer = new QTimer(this);
    mHBTimer->setSingleShot(true);
    QObject::connect(mHBTimer, &QTimer::timeout, this, &DataEngine::heartBeatTimedOut);
    mIsDatakeeperOnline = false;
    mMAC = QString("ff:ff:ff:ff:ff:ff");
    mQueryCounter = 0;

    QObject::connect(mMosqClient, &MosquittoClient::connectionTried, this, &DataEngine::onConnectionTried);
    QObject::connect(mMosqClient, &MosquittoClient::errorWhileLooping, this, &DataEngine::onErrorWhileLooping);
    QObject::connect(mMosqClient, &MosquittoClient::messageReceived, this, &DataEngine::onMessageReceived);

    // Get the MAC.
    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    for(int i = 0; i < interfaces.size(); i++)
    {
        if(!(interfaces.at(1).flags() & QNetworkInterface::IsLoopBack) &&
           (interfaces.at(i).hardwareAddress() != QStringLiteral("00:00:00:00:00:00")))
        {
            mMAC = interfaces.at(i).hardwareAddress();
            break;
        }
    }
    mMAC.remove(':');
    mMAC = mMAC.toLower();
}

void DataEngine::connect()
{
    if(Settings::instance().tlsEnabled())
        mMosqClient->tlsSet(Settings::instance().caFilePath(),
                            Settings::instance().caDirPath());

    mMosqClient->connect(Settings::instance().mqttBroker(),
                         Settings::instance().mqttPort());
}

void DataEngine::requestInternments()
{
    QString query = QString("{\"mac\":\"%1\",\"command\":\"internments\",\"id\":\"%2\"}")
                           .arg(mMAC)
                           .arg(mQueryCounter);
    mQueryCounter++;

    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);
}

void DataEngine::requestInternmentHistory(db_id_t internmentId, time_t fromTime, int32_t numSamples)
{
    QString query;

    // SpO2
    query = QString("{\"mac\":\"%1\",\"command\":\"spo2\",\"id\":\"%2\","
                    "\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                    .arg(mMAC)
                    .arg(mQueryCounter)
                    .arg(internmentId)
                    .arg(numSamples)
                    .arg(fromTime);
    mQueryCounter++;
    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);

    // Heart rate
    query = QString("{\"mac\":\"%1\",\"command\":\"heartR\",\"id\":\"%2\","
                    "\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                    .arg(mMAC)
                    .arg(mQueryCounter)
                    .arg(internmentId)
                    .arg(numSamples)
                    .arg(fromTime);
    mQueryCounter++;
    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);

    // Blood pressure
    query = QString("{\"mac\":\"%1\",\"command\":\"bloodP\",\"id\":\"%2\","
                    "\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                    .arg(mMAC)
                    .arg(mQueryCounter)
                    .arg(internmentId)
                    .arg(numSamples)
                    .arg(fromTime);
    mQueryCounter++;
    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);

    // Body temperature
    query = QString("{\"mac\":\"%1\",\"command\":\"bodyT\",\"id\":\"%2\","
                    "\"internment_id\":%3,\"num_of_sam\":%4,\"from_time\":%5}")
                    .arg(mMAC)
                    .arg(mQueryCounter)
                    .arg(internmentId)
                    .arg(numSamples)
                    .arg(fromTime);
    mQueryCounter++;
    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);
}

void DataEngine::requestAlarmsThresholds(db_id_t internmentId)
{
    QString query;
    query = QString("{\"mac\":\"%1\",\"command\":\"alarms_thresholds\","
                    "\"id\":\"%2\",\"internment_id\":%3}")
            .arg(mMAC).arg(mQueryCounter).arg(internmentId);
    mQueryCounter++;
    mMosqClient->publish(QStringLiteral("datakeeper/query"), query.toUtf8(), 1, false);
}

void DataEngine::requestAlarmsRanges()
{
    QString query;
    query = QString("{\"mac\":\"%1\",").arg(mMAC);
    query.append(QString("\"id\":\"%1\",").arg(mQueryCounter));
    mQueryCounter++;
    query.append(QString("\"command\":\"alarms_ranges\"}"));

    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);
}

void DataEngine::requestReport(const db_id_t internmentId, const time_t fromTime, const time_t toTime)
{
    QString query;
    query = QString("{\"mac\":\"%1\",").arg(mMAC);
    query.append(QString("\"id\":\"%1\",").arg(mQueryCounter));
    mQueryCounter++;
    query.append("\"command\":\"gen_report\",");
    query.append(QString("\"internment_id\":%1,").arg(internmentId));
    query.append(QString("\"from_time\":%1,").arg(fromTime));
    query.append(QString("\"to_time\":%1}").arg(toTime));

    mMosqClient->publish("datakeeper/query", query.toUtf8(), 1, false);
}

void DataEngine::sendAlarmsThresholds(const DatakeeperQueriesParser::AlarmsThresholds &thresholds)
{
    QString msg;
    msg = QString("{\"mac\":\"%1\",\"command\":\"set_alarms\","
                  "\"id\":\"%2\",\"internment_id\":%3,"
                  "\"alarms\":{\"last_update\":\"%4\","
                  "\"spo2_lt\":%5,\"spo2_delay_s\":%6,"
                  "\"hr_lt\":%7,\"hr_gt\":%8,\"hr_delay_s\":%9,"
                  "\"bt_lt\":%10,\"bt_gt\":%11,\"bt_delay_s\":%12,"
                  "\"bp_sys_lt\":%13,\"bp_sys_gt\":%14,\"bp_delay_s\":%15}}")
            .arg(mMAC)
            .arg(mQueryCounter).arg(thresholds.internmentId)
            .arg(thresholds.lastUpdate)
            .arg(thresholds.spO2Lt).arg(thresholds.spO2DelayS)
            .arg(thresholds.hrLt).arg(thresholds.hrGt).arg(thresholds.hRDelayS)
            .arg(thresholds.bTLt).arg(thresholds.bTGt).arg(thresholds.bTDelayS)
            .arg(thresholds.bPSysLt).arg(thresholds.bPSysGt).arg(thresholds.bPDelayS);

    mQueryCounter++;

    mMosqClient->publish(QStringLiteral("datakeeper/query"), msg.toUtf8(), 1, false);
}

QVector<DatakeeperQueriesParser::SpO2> DataEngine::spO2HistoricData(const int32_t internmentId)
{
    QVector<struct DatakeeperQueriesParser::SpO2> retval;

    if(!mSpO2Data.contains(internmentId))
        return retval;

    retval = mSpO2Data[internmentId];
    mSpO2Data.remove(internmentId);

    return retval;
}

QVector<DatakeeperQueriesParser::HeartRate> DataEngine::heartRateHistoricData(const int32_t internmentId)
{
    QVector<struct DatakeeperQueriesParser::HeartRate> retval;

    if(!mHeartRateData.contains(internmentId))
        return retval;

    retval = mHeartRateData[internmentId];
    mHeartRateData.remove(internmentId);

    return retval;
}

QVector<DatakeeperQueriesParser::BloodPressure> DataEngine::bloodPressureHistoricData(const int32_t internmentId)
{
    QVector<struct DatakeeperQueriesParser::BloodPressure> retval;

    if(!mHeartRateData.contains(internmentId))
        return retval;

    retval = mBloodPressureData[internmentId];
    mHeartRateData.remove(internmentId);

    return retval;
}

QVector<DatakeeperQueriesParser::BodyTemperature> DataEngine::bodyTemperatureHistoricData(const int32_t internmentId)
{
    QVector<struct DatakeeperQueriesParser::BodyTemperature> retval;

    if(!mBodyTemperatureData.contains(internmentId))
        return retval;

    retval = mBodyTemperatureData[internmentId];
    mBodyTemperatureData.remove(internmentId);

    return retval;
}

void DataEngine::onConnectionTried(int result)
{
    if(result != MOSQ_ERR_SUCCESS)
    {
        qDebug() << "Connection failed! Error: " << result;
        QTimer::singleShot(1000, this, &DataEngine::connect);
        return;
    }

    QString monitorTopic = QString("monitor/%1").arg(mMAC);
    mMosqClient->subscribe(monitorTopic.toUtf8(), 1);
    emit subscribedToMonitor();

    mMosqClient->subscribe(SignalsParser::topic(), 1);
    mMosqClient->subscribe(DatakeeperAlarmsParser::topic(), 1);

    // Subscribe to heart beat and start the timer.
    mMosqClient->subscribe(heartBeatTopic(), 1);
    mHBTimer->start(HB_TIMER_TIMEOUT_MS);
}

void DataEngine::onErrorWhileLooping(int error)
{
    qDebug() << "Error while looping, error: " << error;
    QTimer::singleShot(1000, this, &DataEngine::connect);
}

void DataEngine::onMessageReceived()
{
    QVector<struct mosquitto_message*> msgs = mMosqClient->messages();

    for(int i = 0; i < msgs.size(); i++)
    {
        const struct mosquitto_message * msg = msgs.at(i);

        QString topic(msg->topic);

        if(topic == QString("monitor/%1").arg(mMAC))
        {
            QVector<DatakeeperQueriesParser::SpO2> spo2Data;
            QVector<DatakeeperQueriesParser::HeartRate> hRData;
            QVector<DatakeeperQueriesParser::BloodPressure> bPData;
            QVector<DatakeeperQueriesParser::BodyTemperature> bTData;
            int32_t internmentId;
            DatakeeperQueriesParser::CommandResult cmdResult;
            DatakeeperQueriesParser::AlarmsThresholds alarmsThresholds;
            DatakeeperQueriesParser::AlarmsRanges alarmsRanges;
            DatakeeperQueriesParser::Report report;

            switch(DatakeeperQueriesParser::typeOfReply(msg)) {
            case DatakeeperQueriesParser::InternmentsReply:
                if(!mInternments->parsePayload(msg))
                    break;

                mMosqClient->unsubscribe("reads/#");
                subscribeToDevices(mInternments->devices());
                emit internmentsUpdated();

                break;

            case DatakeeperQueriesParser::SpO2Reply:
                internmentId = DatakeeperQueriesParser::parseSpO2SensorData(msg, spo2Data);
                if(internmentId < 0)
                {
                    spo2Data.clear();
                    break;
                }

                mSpO2Data.insert(internmentId, spo2Data);
                spo2Data.clear();
                emit spO2HistoricDataAvailable(internmentId);
                break;

            case DatakeeperQueriesParser::HeartRateReply:
                internmentId = DatakeeperQueriesParser::parseHeartRateData(msg, hRData);
                if(internmentId < 0)
                {
                    hRData.clear();
                    break;
                }

                mHeartRateData.insert(internmentId, hRData);
                hRData.clear();
                emit heartRateHistoricDataAvailable(internmentId);
                break;

            case DatakeeperQueriesParser::BloodPressureReply:
                internmentId = DatakeeperQueriesParser::parseBloodPressureData(msg, bPData);
                if(internmentId < 0)
                {
                    bPData.clear();
                    break;
                }

                mBloodPressureData.insert(internmentId, bPData);
                bPData.clear();
                emit bloodPressureHistoricDataAvailable(internmentId);
                break;

            case DatakeeperQueriesParser::BodyTemperatureReply:
                internmentId = DatakeeperQueriesParser::parseBodyTemperatureData(msg, bTData);
                if(internmentId < 0)
                {
                    bTData.clear();
                    break;
                }

                mBodyTemperatureData.insert(internmentId, bTData);
                bTData.clear();
                emit bodyTemperatureHistoricDataAvailable(internmentId);
                break;

            case DatakeeperQueriesParser::CommandResultReply:
                DatakeeperQueriesParser::parseCommandResult(msg, cmdResult);
                emit commandResultReceived(cmdResult);
                break;

            case DatakeeperQueriesParser::AlarmsThresholdsReply:
                if(DatakeeperQueriesParser::parseAlarmsThresholds(msg, alarmsThresholds))
                    emit alarmsThresholdsReceived(alarmsThresholds);
                break;

            case DatakeeperQueriesParser::AlarmsRangesReply:
                DatakeeperQueriesParser::parseAlarmsRanges(msg, alarmsRanges);
                if(!alarmsRanges.id.isEmpty())
                    emit alarmsRangesReceived(alarmsRanges);
                break;

            case DatakeeperQueriesParser::GenerateReportReply:
                if(DatakeeperQueriesParser::parseGenerateReport(msg, report))
                    emit reportReceived(report);
                break;

            case DatakeeperQueriesParser::NotRecognizableReply:
                break;
            }
        }
        else if(topic == SignalsParser::topic())
        {
            SignalsParser::Signals signal;
            SignalsParser::parseMessage(msg, signal);

            switch(signal) {
            case SignalsParser::SignalInternmentsChanged:
                emit internmentsChanged();
                break;

            case SignalsParser::SignalNone:
                break;
            }
        }
        else if(topic == DatakeeperAlarmsParser::topic())
        {
            switch(DatakeeperAlarmsParser::typeOfAlarm(msg)) {
            case DatakeeperAlarmsParser::AlarmUnknown:
                break;

            case DatakeeperAlarmsParser::AlarmDevices:
                mInternments->updateDevicesAlarms(DatakeeperAlarmsParser::parseDeviceAlarmMessage(msg));
                break;

            case DatakeeperAlarmsParser::AlarmInternments:
                mInternments->updateInternmentsAlarms(DatakeeperAlarmsParser::parseInternmentsAlarmsMessage(msg));
                break;
            }
        }
        else if(topic == heartBeatTopic())
        {
            mHBTimer->start(HB_TIMER_TIMEOUT_MS);
            if(!mIsDatakeeperOnline)
            {
                mSoundAlarmHandler->handleDatakeeperOfflineAlarmStatus(false);
                mIsDatakeeperOnline = true;
                emit datakeeperStatusChanged(true);
                requestInternments();
            }
        }
        else
        {
            ReadsParser parser;
            if(parser.parseMessage(msg))
            {
                mInternments->updateInstantValues(parser);
                emit readsDataReceived(parser);
            }
        }
    }

    // Delete all messages.
    for(int i = 0; i < msgs.size(); i++)
        mosquitto_message_free(&(msgs[i]));
}

void DataEngine::subscribeToDevices(const QVector<QString> &devices)
{
    for(int i = 0; i < devices.size(); i++)
    {
        QString topic = QString("reads/%1").arg(devices.at(i));
        mMosqClient->subscribe(topic.toUtf8(), 1);
    }
}

void DataEngine::heartBeatTimedOut()
{
    mIsDatakeeperOnline = false;
    emit datakeeperStatusChanged(false);
    mSoundAlarmHandler->handleDatakeeperOfflineAlarmStatus(true);
}
