<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AlarmsDialog</name>
    <message>
        <location filename="../alarmsdialog.ui" line="14"/>
        <source>AlarmsDialog</source>
        <translation>AlarmsDialog</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="34"/>
        <source>Age</source>
        <translation>Edad</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="46"/>
        <source>Gender</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="58"/>
        <source>Bed</source>
        <translation>Cama</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="70"/>
        <source>Device ID:</source>
        <translation>ID del dispositivo:</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="96"/>
        <source>SpO2 [%]</source>
        <translation>SpO2 [%]</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="108"/>
        <location filename="../alarmsdialog.ui" line="124"/>
        <location filename="../alarmsdialog.ui" line="170"/>
        <location filename="../alarmsdialog.ui" line="202"/>
        <source>Values equal or less than the selected here will trigegr an alarm.</source>
        <translation>Valores iguales o menores que el seleccionado dispararán una alarma.</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="133"/>
        <location filename="../alarmsdialog.ui" line="218"/>
        <location filename="../alarmsdialog.ui" line="316"/>
        <source>Delay [s]</source>
        <translation>Retraso [s]</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="148"/>
        <location filename="../alarmsdialog.ui" line="233"/>
        <source>Trigger: &lt;=</source>
        <translation>Disparo: &lt;=</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="240"/>
        <location filename="../alarmsdialog.ui" line="331"/>
        <location filename="../alarmsdialog.ui" line="338"/>
        <source>Trigger: &gt;=</source>
        <translation>Disparo: &gt;=</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="158"/>
        <source>Heart rate [bpm]</source>
        <translation>Ritmo cardíaco [lpm]</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="189"/>
        <location filename="../alarmsdialog.ui" line="209"/>
        <location filename="../alarmsdialog.ui" line="262"/>
        <location filename="../alarmsdialog.ui" line="284"/>
        <location filename="../alarmsdialog.ui" line="300"/>
        <location filename="../alarmsdialog.ui" line="307"/>
        <source>Values equal or greater than the value selected here will trigger an alarm.</source>
        <translation>Valores iguales o mayores que el seleccionado dispararán una alarma.</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.ui" line="250"/>
        <source>Body temperature [ºC]</source>
        <translation>Temperatura corporal [ºC]</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.cpp" line="31"/>
        <source>Alarms configuration</source>
        <translation>Configuración de las alarmas</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.cpp" line="245"/>
        <source>Age: %1</source>
        <translation>Edad: %1</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.cpp" line="246"/>
        <source>Gender: %1</source>
        <translation>Género: %1</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.cpp" line="247"/>
        <source>Bed: %1</source>
        <translation>Cama: %1</translation>
    </message>
    <message>
        <location filename="../alarmsdialog.cpp" line="248"/>
        <source>Dev. ID: %1</source>
        <translation>ID de dispositivo: %1</translation>
    </message>
</context>
<context>
    <name>DeviceDataWidget</name>
    <message>
        <location filename="../devicedatawidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="40"/>
        <source>Gender</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="58"/>
        <source>Bed</source>
        <translation>Cama</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="76"/>
        <source>Device ID:</source>
        <translation>ID de dispositivo:</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="99"/>
        <source>QR</source>
        <translation>QR</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="239"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="272"/>
        <source>HR bpm</source>
        <translation>RC lpm</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="325"/>
        <source>37.9</source>
        <translation>37.9</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="358"/>
        <source>BT ºC</source>
        <translation>TC ºC</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="153"/>
        <source>100.0</source>
        <translation>100.0</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.ui" line="186"/>
        <source>SpO2 %</source>
        <translation>SpO2%</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.cpp" line="56"/>
        <location filename="../devicedatawidget.cpp" line="64"/>
        <location filename="../devicedatawidget.cpp" line="71"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.cpp" line="116"/>
        <source>Gender: %1</source>
        <translation>Género: %1</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.cpp" line="117"/>
        <source>Bed: %1</source>
        <translation>Cama: %1</translation>
    </message>
    <message>
        <location filename="../devicedatawidget.cpp" line="118"/>
        <source>Dev. ID: %1</source>
        <translation>ID de dispositivo: %1</translation>
    </message>
</context>
<context>
    <name>InternmentsModel</name>
    <message>
        <location filename="../internmentsmodel.cpp" line="342"/>
        <source>Battery voltaje: %1 [mV] - Last seen: %2</source>
        <translation>Tensión de batería: %1 [mV] - Última vez visto: %2</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="358"/>
        <source>Internment</source>
        <translation>Internación</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="361"/>
        <source>Surname</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="364"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="367"/>
        <source>Age</source>
        <translation>Edad</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="370"/>
        <source>Gender</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="373"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="376"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="379"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="382"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="385"/>
        <location filename="../internmentsmodel.cpp" line="391"/>
        <location filename="../internmentsmodel.cpp" line="397"/>
        <location filename="../internmentsmodel.cpp" line="403"/>
        <source>Mute</source>
        <translation>Silenciar</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="388"/>
        <source>SpO2 %</source>
        <translation>SpO2%</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="394"/>
        <source>HR bpm</source>
        <translation>RC lpm</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="400"/>
        <source>BT ºC</source>
        <translation>TC ºC</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="406"/>
        <source>BP Sys</source>
        <translation>PS Sys</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="409"/>
        <source>BP Dias</source>
        <translation>PS Dias</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="412"/>
        <source>Plot</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="415"/>
        <source>Alarms</source>
        <translation>Alarmas</translation>
    </message>
    <message>
        <location filename="../internmentsmodel.cpp" line="418"/>
        <source>Report</source>
        <translation>Reporte</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="94"/>
        <source>Connecting to server...</source>
        <translation>Conectando al servidor...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="104"/>
        <source>Mi&amp;nimize</source>
        <translation>Mi&amp;nimizar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="107"/>
        <source>Ma&amp;ximize</source>
        <translation>Ma&amp;ximizar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="110"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="170"/>
        <source>Internments received.</source>
        <translation>Internaciones recibidas.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Requesting internments...</source>
        <translation>Pidiendo internaciones...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="326"/>
        <source>About MoSimPa Monitor %1</source>
        <translation>Acerca de Monitor MoSimPa %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="327"/>
        <source>&lt;p&gt;This application is part of ParaAyudar&apos;s MoSimPa, an effort to create devices to fight COVID-19.&lt;/p&gt;&lt;p&gt;See &lt;a href=&quot;https://mosimpa.gitlab.io&quot;&gt;the technical documentation&lt;/a&gt; for more information.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.instagram.com/paraayudardar/&quot;&gt;ParaAyudar on instagram&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Esta aplicación es parte del proyecto MoSimPa de ParaAyudar, un esfuerzo para crear dispositivos para combatir el COVID-19.&lt;/p&gt;&lt;p&gt;Para mas información vea &lt;a href=&quot;https://mosimpa.gitlab.io&quot;&gt;la documentación técnica&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.instagram.com/paraayudardar/&quot;&gt;ParaAyudar en instagram&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="345"/>
        <source>The command %1 issued for internment %2 has returned with result %3.</source>
        <translation>El comando %1 realizado para la internación %2 ha retornado con el resultado %3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="347"/>
        <source>Command failed</source>
        <translation>Falló el comando</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="462"/>
        <source>Report generation failed</source>
        <translation>Falló la generación del reporte</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="463"/>
        <source>The report generation failed.</source>
        <translation>La generación del reporte ha fallado.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="468"/>
        <source>Report created, available at %1</source>
        <translation>Reporte creado, disponible en %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="139"/>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>MoSimPa monitor</source>
        <translation>MoSimPa monitor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="64"/>
        <source>Main toolbar</source>
        <translation>Barra de herramientas principal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="140"/>
        <source>Application was minimized to system tray.</source>
        <translation>La aplicación ha sido minimizada a la bandeja del sistema.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>If you close monitor you will not be able to see or hear alarms. Are you sure you want to close the application?</source>
        <translation>Si cierra el monitor no podrá ver ni escuchar alarmas ¿Está seguro de que quiere cerrar la aplicación?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="567"/>
        <source>Can not connect to datakeeper</source>
        <translation>No se puede establecer una conexión a datakeeper</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="569"/>
        <source>Connected to datakeeper</source>
        <translation>Conectado a datakeeper</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="575"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="579"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Refrescar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="581"/>
        <source>Refresh the internments list</source>
        <translation>Refrescar la lista de internaciones</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="587"/>
        <source>&amp;Open internment view</source>
        <translation>A&amp;brir la lista de internaciones</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="589"/>
        <source>Open the view for the current internment</source>
        <translation>Abrir la vista para la internación actual</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="596"/>
        <source>Open a&amp;larms setup dialog</source>
        <translation>Abrir el diá&amp;logo de configuración de alarmas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>Open the alarms setup dialog</source>
        <translation>Abrir el diálogo de configuración de alarmas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="604"/>
        <source>Request the internment&apos;s report.</source>
        <translation>Peticionar el reporte de la internación.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="605"/>
        <source>Requests a report for the selected internment.</source>
        <translation>Peticiona un reporte de la internación seleccionada.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="612"/>
        <source>Show only internme&amp;nts with alarms</source>
        <translation>Mostrar solamente inter&amp;naciones con alarmas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="613"/>
        <source>Show only internments with alarms</source>
        <translation>Mostrar solamente internaciones con alarmas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="620"/>
        <source>Configure MQTT broker...</source>
        <translation>Configurar el broker MQTT...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="621"/>
        <source>&amp;Quit...</source>
        <translation>&amp;Salir...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="622"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="623"/>
        <source>&amp;About Qt</source>
        <translation>&amp;Acerca de Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="625"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="630"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
</context>
<context>
    <name>MqttBrokerSetupDialog</name>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="22"/>
        <source>Hostname</source>
        <translation>Nombre de host</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="39"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="54"/>
        <source>Use TLS</source>
        <translation>Usar TLS</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="61"/>
        <source>TLS setup</source>
        <translation>Configuración de TLS</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="67"/>
        <source>CA file</source>
        <translation>Archivo CA</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="80"/>
        <location filename="../mqttbrokersetupdialog.ui" line="100"/>
        <source>Search...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.ui" line="87"/>
        <source>CA path</source>
        <translation>Directorio CA</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="21"/>
        <source>MQTT broker setup</source>
        <translation>Configuración del broker MQTT</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="73"/>
        <source>Configuration error detected.</source>
        <translation>Error de configuración detectado.</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="74"/>
        <source>When TLS is enabled either the CA file path or the CA dir path should not be empty.</source>
        <translation>Cuando se habilita TLS es necesario qu el archivo CA o el directorio que contiene a los archivos CA sea especificado.</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="86"/>
        <source>MQTT broker setup with changes - restart is required.</source>
        <translation>Confiuración del broker MQTT con cambios - es necesario reiniciar.</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="87"/>
        <source>Changes to the configuration where detected, you need to restart the application in order to get them to work.</source>
        <translation>Se han detectado cambios a la configuración, debe reiniciar la aplicación para que sean usados.</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="105"/>
        <source>Select CA certificate</source>
        <translation>Elegir el certificado CA</translation>
    </message>
    <message>
        <location filename="../mqttbrokersetupdialog.cpp" line="114"/>
        <source>Select directory with CA certificates</source>
        <translation>Elegir el directorio con certificados CA</translation>
    </message>
</context>
<context>
    <name>RequestReportDialog</name>
    <message>
        <location filename="../requestreportdialog.ui" line="14"/>
        <source>Report request</source>
        <translation>Petición de reporte</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.ui" line="20"/>
        <source>Internment ID:</source>
        <translation>ID de internación:</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.ui" line="27"/>
        <source>Location:</source>
        <translation>Ubicación:</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.ui" line="36"/>
        <source>From</source>
        <translation>Desde</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.ui" line="50"/>
        <source>To</source>
        <translation>Hasta</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.ui" line="62"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; the time format is YYYY-MM-dd hh.&lt;br&gt;
For example: 2020-07-22 17 means July 22th 2020, 05 PM.</source>
        <translation>&lt;strong&gt;Nota:&lt;/strong&gt; El formato de fecha/hora es AAAA-MM-dd hh.&lt;br&gt;
Por ejemplo: 2020-07-22 17 representa 22 de Julio de 2020, 17 hs.</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.cpp" line="12"/>
        <source>&lt;strong&gt;Internment ID:&lt;/strong&gt; %1</source>
        <translation>&lt;strong&gt;ID de internación:&lt;/strong&gt; %1</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.cpp" line="14"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.cpp" line="14"/>
        <source>External</source>
        <translation>Externa</translation>
    </message>
    <message>
        <location filename="../requestreportdialog.cpp" line="15"/>
        <source>&lt;strong&gt;Location:&lt;/strong&gt; %1 - %2</source>
        <translation>&lt;strong&gt;Ubicación:&lt;/strong&gt; %1 - %2</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.cpp" line="15"/>
        <source>%1: settings are being stored in %2</source>
        <translation>%1: las configuraciones se guardan en %2</translation>
    </message>
</context>
</TS>
