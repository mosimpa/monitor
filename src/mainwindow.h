/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QItemSelectionModel>
#include <QHash>
#include <QString>
#include <QSystemTrayIcon>

#include <readsparser.h>
#include "devicedatawidget.h"
#include "datakeeperqueriesparser.h"

class DataEngine;
class QTableView;
class QAction;
class AlarmsSortFilterProxyModel;
class AlarmsDialog;
class QTimer;
class RequestReportDialog;
class QToolBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

protected:
    void closeEvent(QCloseEvent * event) override;

private slots:
    void onIntermentsUpdated();
    void openInternmentView();
    void openAlarmsSetup();
    void setOpenDeviceViewButtonEnabled(const QItemSelection &selected, const QItemSelection &deselected);
    void refreshInternments();
    void deleteDeviceDataWidget(const int32_t internmentId);
    void onSpO2HistoricDataAvailable(const int32_t internmentId);
    void onHeartRateHistoricDataAvailable(const int32_t internmentId);
    void onBloodPressureHistoricDataAvailable(const int32_t internmentId);
    void onBodyTemperatureHistoricDataAvailable(const int32_t internmentId);
    void onReadsDataReceived(const ReadsParser & parser);
    void about();
    void closeAlarmsDialog();
    void handleCommandResult(const DatakeeperQueriesParser::CommandResult &result);
    void onInternmentsChanged();
    void toggleRefreshIntermentsButtonImage();
    void alarmsRangesReceived(const DatakeeperQueriesParser::AlarmsRanges & ranges);
    void requestInternmentHistory(db_id_t internmentId, time_t fromTime, int32_t numOfSamples);
    void configureMqtt();
    void showInternmentsWithAlarm(bool checked);
    void addRowButtons();
    void openRequestReportDialog();
    void requestReport(db_id_t internmentId, time_t fromTime, time_t toTime);
    void downloadReport(DatakeeperQueriesParser::Report report);
    void muteSpO2();
    void muteHR();
    void muteBT();
    void muteDevice();
    void updateAlarmSeverity(const DatakeeperAlarmsParser::AlarmSeverity severity);
    void confirmExit();
    void systemTrayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void handleDatakeeperStatusChange(bool isOnline);

private:
    void createMenuAndToolBar();

    DataEngine * mDataEngine;
    AlarmsSortFilterProxyModel * mSortFilterProxyModel;
    QTableView * mTableView;
    QAction * mRefreshAction;
    QAction * mOpenInternmentViewAction;
    QAction * mOpenAlarmsSetupAction;
    QAction * mShowInternmentsWithAlarm;
    QAction * mRequestReportAction;
    QAction * mQuitAction;
    QHash<int32_t, DeviceDataWidget*> mDevDataWidgets;
    AlarmsDialog * mAlarmsDialog;
    RequestReportDialog * mRequestReportDialog;
    QTimer * mRefreshInternmentsTimer;
    DatakeeperQueriesParser::AlarmsRanges mAlarmsRanges;
    QSystemTrayIcon * mSysIcon;
    QToolBar * mMainToolBar;
    bool mRefreshImageIsRed;
};

#endif // MAINWINDOW_H
