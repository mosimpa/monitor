#include <QMetaType>
#include <QString>
#include <QTest>
#include <QVector>
#include <QAbstractItemModel>
#include <QUrl>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "../src/internmentsmodel.h"
#include "../src/datakeeperqueriesparser.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "datakeeperqueriesparser_tst.h"



void DatakeeperQueriesParserTests::checkTypeOfReplies_data()
{
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<DatakeeperQueriesParser::Replies>("expectedTypeOfReply");

    QTest::addRow("invalid_json") << newMosquittoMessage("foo") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("valid_json_invalid_payload") << newMosquittoMessage("{\"foo\":\"var\"}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_internments") << newMosquittoMessage("{\"internments\":[]}") << DatakeeperQueriesParser::InternmentsReply;
    QTest::addRow("type_spo2") << newMosquittoMessage("{\"spo2\":[]}") << DatakeeperQueriesParser::SpO2Reply;
    QTest::addRow("type_hr") << newMosquittoMessage("{\"heartR\":[]}") << DatakeeperQueriesParser::HeartRateReply;
    QTest::addRow("type_bp") << newMosquittoMessage("{\"bloodP\":[]}") << DatakeeperQueriesParser::BloodPressureReply;
    QTest::addRow("type_bt") << newMosquittoMessage("{\"bodyT\":[]}") << DatakeeperQueriesParser::BodyTemperatureReply;
    QTest::addRow("type_command_result") << newMosquittoMessage("{\"command\":\"foo\",\"result\":\"var\"}") << DatakeeperQueriesParser::CommandResultReply;
    QTest::addRow("type_alarms_thresholds") << newMosquittoMessage("{\"alarms_thresholds\":{}}") << DatakeeperQueriesParser::AlarmsThresholdsReply;
    QTest::addRow("type_alarms_ranges") << newMosquittoMessage("{\"command\":\"alarms_ranges\"}") << DatakeeperQueriesParser::AlarmsRangesReply;
    QTest::addRow("type_gen_report") << newMosquittoMessage("{\"command\":\"gen_report\"}") << DatakeeperQueriesParser::GenerateReportReply;

    QTest::addRow("type_internments_not_array") << newMosquittoMessage("{\"internments\":5}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_spo2_not_array") << newMosquittoMessage("{\"spo2\":\"\"}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_hr_not_array") << newMosquittoMessage("{\"heartR\":{}}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_bp_not_array") << newMosquittoMessage("{\"bloodP\":7}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_bt_not_array") << newMosquittoMessage("{\"bodyT\":5}") << DatakeeperQueriesParser::NotRecognizableReply;
    QTest::addRow("type_command_unknown") << newMosquittoMessage("{\"command\":\"unknown\"}") << DatakeeperQueriesParser::NotRecognizableReply;
}

void DatakeeperQueriesParserTests::checkTypeOfReplies()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(DatakeeperQueriesParser::Replies, expectedTypeOfReply);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto reply = DatakeeperQueriesParser::typeOfReply(&(msg->msg));

    QVERIFY2(reply == expectedTypeOfReply, "Reply must match expected type of reply.");
}

/**
 * @brief DatakeeperQueriesParserTests::checkParseInternmentsMessage_data
 *
 * The contents of Interments are tested in it's own class.
 */
void DatakeeperQueriesParserTests::checkParseInternmentsMessage_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("resultIsValid");

    QString payload;

    payload = QStringLiteral("foo");
    QTest::addRow("invalid_json") << newMosquittoMessage(payload) << false;

    payload = QStringLiteral("{\"id\":\"0\",\"internments\":["
                             "{\"internment_id\":1,\"from_time\":15455,\"alarms\":{"
                             "\"spo2_lt\":950,\"hr_lt\":600,\"hr_gt\":1000,"
                             "\"bt_lt\":9472,\"bt_gt\":9728,"
                             "\"bp_sys_lt\":120,\"bp_sys_gt\":150},"
                             "\"device\":\"002219fe4d5d\","
                             "\"location\":{\"desc\":\"local\",\"type\":\"Test\"},"
                             "\"patient\":{\"name\":\"Jhon\",\"surname\":\"Smith\","
                             "\"age\":33,\"gender\":\"m\"}},"
                             "{\"internment_id\":2,\"from_time\":15455,\"alarms\":{"
                             "\"spo2_lt\":950,\"hr_lt\":600,\"hr_gt\":1000,"
                             "\"bt_lt\":9472,\"bt_gt\":9728,"
                             "\"bp_sys_lt\":120,\"bp_sys_gt\":150},"
                             "\"device\":\"001122334455\","
                             "\"location\":{\"desc\":\"local\",\"type\":\"Test2\"},"
                             "\"patient\":{\"name\":\"Natalie\",\"surname\":\"Willson\","
                             "\"age\":27,\"gender\":\"f\"}},"
                             "{\"internment_id\":3,\"from_time\":15455,\"alarms\":{"
                             "\"spo2_lt\":710,\"hr_lt\":410,\"hr_gt\":1010,"
                             "\"bt_lt\":9083,\"bt_gt\":9856,"
                             "\"bp_sys_lt\":120,\"bp_sys_gt\":150},"
                             "\"device\":\"00500463ee4d\","
                             "\"location\":{\"desc\":\"local\",\"type\":\"BED 0\"},"
                             "\"patient\":{\"name\":\"Name 500\",\"surname\":\"Surname 500\","
                             "\"age\":0,\"gender\":\"o\"}}]}");

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << true;
}

void DatakeeperQueriesParserTests::checkParseInternmentsMessage()
{
    QFETCH(MosquittoMessage*, msg);
    QFETCH(bool, resultIsValid);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto im = new InternmentsModel(this);
    bool result = im->parsePayload(&(msg->msg));

    QVERIFY2(result == resultIsValid, "Result validity must match");
}

void DatakeeperQueriesParserTests::checkParseSpO2SensorData_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<db_id_t>("expectedInternmentId");
    QTest::addColumn<QVector<DatakeeperQueriesParser::SpO2>>("expectedValuesVector");

    QVector<DatakeeperQueriesParser::SpO2> valuesVector;
    DatakeeperQueriesParser::SpO2 values;

    /**************************************************************************
     * Valid data.
     **************************************************************************/

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.spo2 = 830;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    QString payload;
    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}");

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * InternmentId < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":%1,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(DB_ID_MIN - 1);

    QTest::addRow("internmentId_min-1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * InternmentId == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.spo2 = 830;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":%1,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(DB_ID_MIN);

    QTest::addRow("internmentId_min") << newMosquittoMessage(payload) << DB_ID_MIN << valuesVector;

    /**************************************************************************
     * InternmentId == MAX
     **************************************************************************/
    payload = QString("{\"id\":\"2\",\"internment_id\":%1,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(DB_ID_MAX);

    QTest::addRow("internmentId_max") << newMosquittoMessage(payload) << DB_ID_MAX << valuesVector;

    /**************************************************************************
     * InternmentId > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":%1,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(static_cast<int64_t>(DB_ID_MAX) + 1);

    QTest::addRow("internmentId_max+1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * Middle entry with wrong time.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry skipped in purpose.

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":-1,\"SpO2\":830},"
                      "{\"time\":3,\"SpO2\":810}]}");

    QTest::addRow("wrong_time") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * SpO2 < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":%1},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(WIRE_SPO2_MIN - 1);

    QTest::addRow("spo2_min-1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * SpO2 == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.spo2 = WIRE_SPO2_MIN;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":%1},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(WIRE_SPO2_MIN);

    QTest::addRow("spo2_min") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * SpO2 == MAX
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.spo2 = WIRE_SPO2_MAX;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":%1},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(WIRE_SPO2_MAX);

    QTest::addRow("spo2_max") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * SpO2 > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.spo2 = 520;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.spo2 = 810;
    valuesVector.append(values);

    payload = QString("{\"id\":\"2\",\"internment_id\":3,\"spo2\":["
                      "{\"time\":1,\"SpO2\":520},"
                      "{\"time\":2,\"SpO2\":%1},"
                      "{\"time\":3,\"SpO2\":810}]}").arg(static_cast<int64_t>(WIRE_SPO2_MAX) + 1);

    QTest::addRow("spo2_max+1") << newMosquittoMessage(payload) << 3 << valuesVector;
}

void DatakeeperQueriesParserTests::checkParseSpO2SensorData()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(db_id_t, expectedInternmentId);
    QFETCH(QVector<DatakeeperQueriesParser::SpO2>, expectedValuesVector);

    QVector<DatakeeperQueriesParser::SpO2> valuesVector;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto internmentId = DatakeeperQueriesParser::parseSpO2SensorData(&(msg->msg), valuesVector);

    QVERIFY2(internmentId == expectedInternmentId, "Internment ID must match.");

    if(internmentId > 0)
    {
        QVERIFY2(valuesVector.size() == expectedValuesVector.size(), "Size of vectors must match.");

        for(int i = 0; i < valuesVector.size(); i++)
        {
            QVERIFY2(valuesVector.at(i).time == expectedValuesVector.at(i).time, "Time must match");
            QVERIFY2(valuesVector.at(i).spo2 == expectedValuesVector.at(i).spo2, "SpO2 must match");
        }
    }
}

void DatakeeperQueriesParserTests::checkParseHeartRateData_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<db_id_t>("expectedInternmentId");
    QTest::addColumn<QVector<DatakeeperQueriesParser::HeartRate>>("expectedValuesVector");

    QVector<DatakeeperQueriesParser::HeartRate> valuesVector;
    DatakeeperQueriesParser::HeartRate values;

    /**************************************************************************
     * Valid data.
     **************************************************************************/

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.heartRate = 2940;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    QString payload;
    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}");

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * InternmentId < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":%1,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(DB_ID_MIN - 1);

    QTest::addRow("internmentId_min-1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * InternmentId == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.heartRate = 2940;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":%1,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(DB_ID_MIN);

    QTest::addRow("internmentId_min") << newMosquittoMessage(payload) << DB_ID_MIN << valuesVector;

    /**************************************************************************
     * InternmentId == MAX
     **************************************************************************/
    payload = QString("{\"id\":\"3\",\"internment_id\":%1,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(DB_ID_MAX);

    QTest::addRow("internmentId_max") << newMosquittoMessage(payload) << DB_ID_MAX << valuesVector;

    /**************************************************************************
     * InternmentId > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":%1,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(static_cast<int64_t>(DB_ID_MAX) + 1);

    QTest::addRow("internmentId_max+1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * Middle entry with wrong time.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":-1,\"heartR\":2940},"
                      "{\"time\":3,\"heartR\":2190}]}");

    QTest::addRow("wrong_time") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Heart Rate < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":%1},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(WIRE_HEART_RATE_MIN - 1);

    QTest::addRow("hr_min-1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Heart Rate == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.heartRate = WIRE_HEART_RATE_MIN;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":%1},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(WIRE_HEART_RATE_MIN);

    QTest::addRow("hr_min") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Heart Rate == MAX
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.heartRate = WIRE_HEART_RATE_MAX;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":%1},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(WIRE_HEART_RATE_MAX);

    QTest::addRow("hr_max") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Heart Rate > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.heartRate = 690;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.heartRate = 2190;
    valuesVector.append(values);

    payload = QString("{\"id\":\"3\",\"internment_id\":3,\"heartR\":["
                      "{\"time\":1,\"heartR\":690},"
                      "{\"time\":2,\"heartR\":%1},"
                      "{\"time\":3,\"heartR\":2190}]}").arg(static_cast<int64_t>(WIRE_HEART_RATE_MAX) + 1);

    QTest::addRow("hr_max+1") << newMosquittoMessage(payload) << 3 << valuesVector;
}

void DatakeeperQueriesParserTests::checkParseHeartRateData()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(db_id_t, expectedInternmentId);
    QFETCH(QVector<DatakeeperQueriesParser::HeartRate>, expectedValuesVector);

    QVector<DatakeeperQueriesParser::HeartRate> valuesVector;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto internmentId = DatakeeperQueriesParser::parseHeartRateData(&(msg->msg), valuesVector);

    QVERIFY2(internmentId == expectedInternmentId, "Internment ID must match.");

    if(internmentId > 0)
    {
        QVERIFY2(valuesVector.size() == expectedValuesVector.size(), "Size of vectors must match.");

        for(int i = 0; i < valuesVector.size(); i++)
        {
            QVERIFY2(valuesVector.at(i).time == expectedValuesVector.at(i).time, "Time must match");
            QVERIFY2(valuesVector.at(i).heartRate == expectedValuesVector.at(i).heartRate, "Heart rate must match");
        }
    }
}

void DatakeeperQueriesParserTests::checkParseBloodPressureData_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<db_id_t>("expectedInternmentId");
    QTest::addColumn<QVector<DatakeeperQueriesParser::BloodPressure>>("expectedValuesVector");

    QVector<DatakeeperQueriesParser::BloodPressure> valuesVector;
    DatakeeperQueriesParser::BloodPressure values;

    /**************************************************************************
     * Valid data.
     **************************************************************************/

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = 33;
    values.diastolic = 58;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    QString payload;
    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}");

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * InternmentId < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":%1,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(DB_ID_MIN - 1);

    QTest::addRow("internmentId_min-1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * InternmentId == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = 33;
    values.diastolic = 58;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":%1,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(DB_ID_MIN);

    QTest::addRow("internmentId_min") << newMosquittoMessage(payload) << DB_ID_MIN << valuesVector;

    /**************************************************************************
     * InternmentId == MAX
     **************************************************************************/
    payload = QString("{\"id\":\"4\",\"internment_id\":%1,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(DB_ID_MAX);

    QTest::addRow("internmentId_max") << newMosquittoMessage(payload) << DB_ID_MAX << valuesVector;

    /**************************************************************************
     * InternmentId > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":%1,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(static_cast<int64_t>(DB_ID_MAX) + 1);

    QTest::addRow("internmentId_max+1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * Middle entry with wrong time.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":-1,\"sys\":33,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}");

    QTest::addRow("wrong_time") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure systolic < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":%1,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_SYS_MIN - 1);

    QTest::addRow("bp_sys_min-1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure systolic == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = WIRE_BLOOD_PRESSURE_SYS_MIN;
    values.diastolic = 58;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":%1,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_SYS_MIN);

    QTest::addRow("bp_sys_min") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure systolic == MAX
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = WIRE_BLOOD_PRESSURE_SYS_MAX;
    values.diastolic = 58;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":%1,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_SYS_MAX);

    QTest::addRow("bp_sys_max") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure systolic > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":%1,\"dia\":58},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(static_cast<int64_t>(WIRE_BLOOD_PRESSURE_SYS_MAX) + 1);

    QTest::addRow("bp_sys_max+1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure diastolic < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":%1},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_DIAS_MIN - 1);

    QTest::addRow("bp_dia_min-1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure diastolic == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = 33;
    values.diastolic = WIRE_BLOOD_PRESSURE_DIAS_MIN;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":%1},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_DIAS_MIN);

    QTest::addRow("bp_dia_min") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure diastolic == MAX
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.systolic = 33;
    values.diastolic = WIRE_BLOOD_PRESSURE_DIAS_MAX;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":%1},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(WIRE_BLOOD_PRESSURE_DIAS_MAX);

    QTest::addRow("bp_dia_max") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Blood Pressure diastolic > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.systolic = 73;
    values.diastolic = 83;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.systolic = 48;
    values.diastolic = 37;
    valuesVector.append(values);

    payload = QString("{\"id\":\"4\",\"internment_id\":3,\"bloodP\":["
                      "{\"time\":1,\"sys\":73,\"dia\":83},"
                      "{\"time\":2,\"sys\":33,\"dia\":%1},"
                      "{\"time\":3,\"sys\":48,\"dia\":37}]}").arg(static_cast<int64_t>(WIRE_BLOOD_PRESSURE_DIAS_MAX) + 1);

    QTest::addRow("bp_dia_max+1") << newMosquittoMessage(payload) << 3 << valuesVector;
}

void DatakeeperQueriesParserTests::checkParseBloodPressureData()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(db_id_t, expectedInternmentId);
    QFETCH(QVector<DatakeeperQueriesParser::BloodPressure>, expectedValuesVector);

    QVector<DatakeeperQueriesParser::BloodPressure> valuesVector;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto internmentId = DatakeeperQueriesParser::parseBloodPressureData(&(msg->msg), valuesVector);

    QVERIFY2(internmentId == expectedInternmentId, "Internment ID must match.");

    if(internmentId > 0)
    {
        QVERIFY2(valuesVector.size() == expectedValuesVector.size(), "Size of vectors must match.");

        for(int i = 0; i < valuesVector.size(); i++)
        {
            QVERIFY2(valuesVector.at(i).time == expectedValuesVector.at(i).time, "Time must match");
            QVERIFY2(valuesVector.at(i).systolic == expectedValuesVector.at(i).systolic, "Systolic pressure must match");
            QVERIFY2(valuesVector.at(i).diastolic == expectedValuesVector.at(i).diastolic, "Diastolic pressure must match");
        }
    }
}

void DatakeeperQueriesParserTests::checkParseBodyTemperatureData_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<db_id_t>("expectedInternmentId");
    QTest::addColumn<QVector<DatakeeperQueriesParser::BodyTemperature>>("expectedValuesVector");

    QVector<DatakeeperQueriesParser::BodyTemperature> valuesVector;
    DatakeeperQueriesParser::BodyTemperature values;

    /**************************************************************************
     * Valid data.
     **************************************************************************/

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.temp = 342;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    QString payload;
    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}");

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * InternmentId < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":%1,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}").arg(DB_ID_MIN - 1);

    QTest::addRow("internmentId_min-1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * InternmentId == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.temp = 342;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":%1,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}").arg(DB_ID_MIN);

    QTest::addRow("internmentId_min") << newMosquittoMessage(payload) << DB_ID_MIN << valuesVector;

    /**************************************************************************
     * InternmentId == MAX
     **************************************************************************/
    payload = QString("{\"id\":\"5\",\"internment_id\":%1,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}").arg(DB_ID_MAX);

    QTest::addRow("internmentId_max") << newMosquittoMessage(payload) << DB_ID_MAX << valuesVector;

    /**************************************************************************
     * InternmentId > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":%1,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}").arg(static_cast<int64_t>(DB_ID_MAX) + 1);

    QTest::addRow("internmentId_max+1") << newMosquittoMessage(payload) << -1 << valuesVector;

    /**************************************************************************
     * Middle entry with wrong time.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":-1,\"temp\":342},"
                      "{\"time\":3,\"temp\":374}]}");

    QTest::addRow("wrong_time") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Body Temperature < MIN.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":%1},"
                      "{\"time\":3,\"temp\":374}]}").arg(WIRE_BODY_TEMP_MIN - 1);

    QTest::addRow("bt_min-1") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Body Temperature == MIN
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.temp = WIRE_BODY_TEMP_MIN;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":%1},"
                      "{\"time\":3,\"temp\":374}]}").arg(WIRE_BODY_TEMP_MIN);

    QTest::addRow("bt_min") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Body Temperature == MAX
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry.
    values.time = 2;
    values.temp = WIRE_BODY_TEMP_MAX;
    valuesVector.append(values);

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":%1},"
                      "{\"time\":3,\"temp\":374}]}").arg(WIRE_BODY_TEMP_MAX);

    QTest::addRow("bt_max") << newMosquittoMessage(payload) << 3 << valuesVector;

    /**************************************************************************
     * Body Temperature > MAX.
     **************************************************************************/
    valuesVector.clear();

    // First entry.
    values.time = 1;
    values.temp = 373;
    valuesVector.append(values);

    // Second entry skipped in purpose to cause an error if the test continues.

    // Third entry.
    values.time = 3;
    values.temp = 374;
    valuesVector.append(values);

    payload = QString("{\"id\":\"5\",\"internment_id\":3,\"bodyT\":["
                      "{\"time\":1,\"temp\":373},"
                      "{\"time\":2,\"temp\":%1},"
                      "{\"time\":3,\"temp\":374}]}").arg(static_cast<int64_t>(WIRE_BODY_TEMP_MAX) + 1);

    QTest::addRow("bt_max+1") << newMosquittoMessage(payload) << 3 << valuesVector;
}

void DatakeeperQueriesParserTests::checkParseBodyTemperatureData()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(db_id_t, expectedInternmentId);
    QFETCH(QVector<DatakeeperQueriesParser::BodyTemperature>, expectedValuesVector);

    QVector<DatakeeperQueriesParser::BodyTemperature> valuesVector;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto internmentId = DatakeeperQueriesParser::parseBodyTemperatureData(&(msg->msg), valuesVector);

    QVERIFY2(internmentId == expectedInternmentId, "Internment ID must match.");

    if(internmentId > 0)
    {
        QVERIFY2(valuesVector.size() == expectedValuesVector.size(), "Size of vectors must match.");

        for(int i = 0; i < valuesVector.size(); i++)
        {
            QVERIFY2(valuesVector.at(i).time == expectedValuesVector.at(i).time, "Time must match");
            QVERIFY2(valuesVector.at(i).temp == expectedValuesVector.at(i).temp, "Temperature must match");
        }
    }
}

void DatakeeperQueriesParserTests::checkGenerateReportReply_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("parsed");
    QTest::addColumn<DatakeeperQueriesParser::Report>("expectedReport");

    QString payload;
    DatakeeperQueriesParser::Report report;

    // Invalid JSON.
    payload = QStringLiteral("foo");
    QTest::addRow("invalid_json") << newMosquittoMessage(payload) << false << report;

    // Valid response, report could not be generated.
    report.id = "0";
    report.url = "ERROR";
    payload = QStringLiteral("{\"id\":\"0\",\"internment_id\":1,\"command\":\"gen_report\",\"result\":\"ERROR\"}");
    QTest::addRow("valid_data_result_error") << newMosquittoMessage(payload) << true << report;

    // Valid response, has been generated.
    report.url = "https://example.com/foo.pdf";
    payload = QStringLiteral("{\"id\":\"0\",\"command\":\"gen_report\",\"url\":\"https://example.com/foo.pdf\"}");
    QTest::addRow("valid_data_with_url") << newMosquittoMessage(payload) << true << report;
}

void DatakeeperQueriesParserTests::checkGenerateReportReply()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, parsed);
    QFETCH(DatakeeperQueriesParser::Report, expectedReport);
    DatakeeperQueriesParser::Report report;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueriesParser::parseGenerateReport(&(msg->msg), report);

    QVERIFY2(retval == parsed, "Parsed must match.");

    if(!retval)
        return;

    QVERIFY2(report.id == expectedReport.id, "The ID must match.");
    QVERIFY2(report.url == expectedReport.url, "The URL must match.");
}

void DatakeeperQueriesParserTests::checkParseAlarmsThresholds_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("parsed");
    QTest::addColumn<DatakeeperQueriesParser::AlarmsThresholds>("expectedThresholds");

    DatakeeperQueriesParser::AlarmsThresholds thresholds;

    thresholds.id = QStringLiteral("an_id");
    thresholds.internmentId = 1;
    thresholds.lastUpdate = QStringLiteral("2020-07-19T17:42:03-03:00");
    thresholds.spO2Lt = 950;
    thresholds.spO2DelayS = 15;

    thresholds.hrLt = 1500;
    thresholds.hrGt = 1200;
    thresholds.hRDelayS = 15;

    thresholds.bTLt = 370;
    thresholds.bTGt = 380;
    thresholds.hRDelayS = 15;

    thresholds.bPSysLt = 130;
    thresholds.bPSysGt = 150;
    thresholds.bPDelayS = 15;

    QString payload;

    payload = QString("{\"id\":\"%1\",\"internment_id\":%2,"
                      "\"alarms_thresholds\":{\"last_update\":\"%3\","
                      "\"spo2_lt\":%4,\"spo2_delay_s\":%5,"
                      "\"hr_lt\":%6,\"hr_gt\":%7,\"hr_delay_s\":%8,"
                      "\"bt_lt\":%9,\"bt_gt\":%10,\"bt_delay_s\":%11,"
                      "\"bp_sys_lt\":%12,\"bp_sys_gt\":%13,\"bp_delay_s\":%14}}")
            .arg(thresholds.id).arg(thresholds.internmentId)
            .arg(thresholds.lastUpdate)
            .arg(thresholds.spO2Lt).arg(thresholds.spO2DelayS)
            .arg(thresholds.hrLt).arg(thresholds.hrGt).arg(thresholds.hRDelayS)
            .arg(thresholds.bTLt).arg(thresholds.bTGt).arg(thresholds.bTDelayS)
            .arg(thresholds.bPSysLt).arg(thresholds.bPSysGt).arg(thresholds.bPDelayS);
    QTest::addRow("alarm_th_ok") << newMosquittoMessage(payload) << true << thresholds;

    payload = QString("{\"command\":\"alarms_thresholds\"}");
    QTest::addRow("alarm_th_no_internment_id") << newMosquittoMessage(payload) << false << thresholds;
}

void DatakeeperQueriesParserTests::checkParseAlarmsThresholds()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, parsed);
    QFETCH(DatakeeperQueriesParser::AlarmsThresholds, expectedThresholds);
    DatakeeperQueriesParser::AlarmsThresholds thresholds;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueriesParser::parseAlarmsThresholds(&(msg->msg), thresholds);

    QVERIFY2(retval == parsed, "Parsed must match.");

    if(!retval)
        return;

    QVERIFY2(thresholds.id == expectedThresholds.id, "The ID must match.");
    QVERIFY2(thresholds.internmentId == expectedThresholds.internmentId, "Internment ID must match.");

    QVERIFY2(thresholds.spO2Lt == expectedThresholds.spO2Lt, "SpO2 must match.");
    QVERIFY2(thresholds.spO2DelayS == expectedThresholds.spO2DelayS, "SpO2 delay must match.");

    QVERIFY2(thresholds.hrLt == expectedThresholds.hrLt, "HR lt must match.");
    QVERIFY2(thresholds.hrGt == expectedThresholds.hrGt, "HR gt must match.");
    QVERIFY2(thresholds.hRDelayS == expectedThresholds.hRDelayS, "HR delay must match.");

    QVERIFY2(thresholds.bTLt == expectedThresholds.bTLt, "BT lt must match.");
    QVERIFY2(thresholds.bTGt == expectedThresholds.bTGt, "BT gt must match.");
    QVERIFY2(thresholds.bTDelayS == expectedThresholds.bTDelayS, "BT delay must match.");

    QVERIFY2(thresholds.bPSysLt == expectedThresholds.bPSysLt, "BP sys lt must match.");
    QVERIFY2(thresholds.bPSysGt == expectedThresholds.bPSysGt, "BP sys gt must match.");
    QVERIFY2(thresholds.bPDelayS == expectedThresholds.bPDelayS, "BP delay must match.");
}

void DatakeeperQueriesParserTests::checkParseAlarmsRanges_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("parsed");
    QTest::addColumn<DatakeeperQueriesParser::AlarmsRanges>("expectedRanges");

    DatakeeperQueriesParser::AlarmsRanges ranges;
    QString payload;

    ranges.id = QStringLiteral("an_id");

    ranges.spO2EqOrLessPercBot = 70;
    ranges.spO2EqOrLessPercTop = 95;
    ranges.spO2DelaySMin = 15;
    ranges.spO2DelaySMax = 60;

    ranges.heartRateEqOrLessBPMBot = 40;
    ranges.heartRateEqOrLessBPMTop = 60;
    ranges.heartRateEqOrMoreBPMBot = 100;
    ranges.heartRateEqOrMoreBPMTop = 140;
    ranges.heartRateDelaySMin = 15;
    ranges.heartRateDelaySMax = 60;

    ranges.bodyTempEqOrLessCelsBot = 35;
    ranges.bodyTempEqOrLessCelsTop = 37;
    ranges.bodyTempEqOrMoreCelsBot = 38;
    ranges.bodyTempEqOrMoreCelsTop = 40;
    ranges.bodyTempDelaySMin= 15;
    ranges.bodyTempDelaySMax = 60;

    ranges.bloodPressureSysEqOrLessBot = 100;
    ranges.bloodPressureSysEqOrLessTop = 120;
    ranges.bloodPressureSysEqOrMoreBot = 150;
    ranges.bloodPressureSysEqOrMoreTop = 170;
    ranges.bloodPressureDelaySMin = 15;
    ranges.bloodPressureDelaySMax = 60;

    payload = QString("{\"id\":\"%1\",\"command\":\"alarms_ranges\","
                      "\"spo2_eq_or_less_perc\":\"%2,%3\","
                      "\"spo2_delay_s\":\"%4,%5\","
                      "\"heart_rate_eq_or_less_bpm\":\"%6,%7\","
                      "\"heart_rate_eq_or_more_bpm\":\"%8,%9\","
                      "\"heart_rate_delay_s\":\"%10,%11\","
                      "\"body_temperature_eq_or_less_cels\":\"%12,%13\","
                      "\"body_temperature_eq_or_more_cels\":\"%14,%15\","
                      "\"body_temperature_delay_s\":\"%16,%17\","
                      "\"blood_pressure_sys_eq_or_less\":\"%18,%19\","
                      "\"blood_pressure_sys_eq_or_more\":\"%20,%21\","
                      "\"blood_pressure_delay_s\":\"%22,%23\"}")
            .arg(ranges.id)
            .arg(ranges.spO2EqOrLessPercBot).arg(ranges.spO2EqOrLessPercTop)
            .arg(ranges.spO2DelaySMin).arg(ranges.spO2DelaySMax)
            .arg(ranges.heartRateEqOrLessBPMBot).arg(ranges.heartRateEqOrLessBPMTop)
            .arg(ranges.heartRateEqOrMoreBPMBot).arg(ranges.heartRateEqOrMoreBPMTop)
            .arg(ranges.heartRateDelaySMin).arg(ranges.heartRateDelaySMax)
            .arg(ranges.bodyTempEqOrLessCelsBot).arg(ranges.bodyTempEqOrLessCelsTop)
            .arg(ranges.bodyTempEqOrMoreCelsBot).arg(ranges.bodyTempEqOrMoreCelsTop)
            .arg(ranges.bodyTempDelaySMin).arg(ranges.bodyTempDelaySMax)
            .arg(ranges.bloodPressureSysEqOrLessBot).arg(ranges.bloodPressureSysEqOrLessTop)
            .arg(ranges.bloodPressureSysEqOrMoreBot).arg(ranges.bloodPressureSysEqOrMoreTop)
            .arg(ranges.bloodPressureDelaySMin).arg(ranges.bloodPressureDelaySMax);

    QTest::addRow("alarms_ranges_ok") << newMosquittoMessage(payload) << true << ranges;
}

void DatakeeperQueriesParserTests::checkParseAlarmsRanges()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, parsed);
    QFETCH(DatakeeperQueriesParser::AlarmsRanges, expectedRanges);
    DatakeeperQueriesParser::AlarmsRanges ranges;

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto retval = DatakeeperQueriesParser::parseAlarmsRanges(&(msg->msg), ranges);

    QVERIFY2(retval == parsed, "Parsed must match.");

    if(!retval)
        return;

    QVERIFY2(ranges.id == expectedRanges.id, "The ID must match.");

    QVERIFY2(ranges.spO2EqOrLessPercBot == expectedRanges.spO2EqOrLessPercBot, "SpO2 EQLT bot must match.");
    QVERIFY2(ranges.spO2EqOrLessPercTop == expectedRanges.spO2EqOrLessPercTop, "SpO2 EQLT top must match.");
    QVERIFY2(ranges.spO2DelaySMin == expectedRanges.spO2DelaySMin, "SpO2 delay s min must match.");
    QVERIFY2(ranges.spO2DelaySMax == expectedRanges.spO2DelaySMax, "SpO2 delay s max must match.");

    QVERIFY2(ranges.heartRateEqOrLessBPMBot == expectedRanges.heartRateEqOrLessBPMBot, "HR EQLT bot must match.");
    QVERIFY2(ranges.heartRateEqOrLessBPMTop == expectedRanges.heartRateEqOrLessBPMTop, "HR EQLT top must match.");
    QVERIFY2(ranges.heartRateEqOrMoreBPMBot == expectedRanges.heartRateEqOrMoreBPMBot, "HR EQGT bot must match.");
    QVERIFY2(ranges.heartRateEqOrMoreBPMTop == expectedRanges.heartRateEqOrMoreBPMTop, "HR EQGT top must match.");
    QVERIFY2(ranges.heartRateDelaySMin == expectedRanges.heartRateDelaySMin, "HR delay s min must match.");
    QVERIFY2(ranges.heartRateDelaySMax == expectedRanges.heartRateDelaySMax, "HR delay s max must match.");

    QVERIFY2(ranges.bodyTempEqOrLessCelsBot == expectedRanges.bodyTempEqOrLessCelsBot, "BT EQLT bot must match.");
    QVERIFY2(ranges.bodyTempEqOrLessCelsTop == expectedRanges.bodyTempEqOrLessCelsTop, "BT EQLT top must match.");
    QVERIFY2(ranges.bodyTempEqOrMoreCelsBot == expectedRanges.bodyTempEqOrMoreCelsBot, "BT EQGT bot must match.");
    QVERIFY2(ranges.bodyTempEqOrMoreCelsTop == expectedRanges.bodyTempEqOrMoreCelsTop, "BT EQGT top must match.");
    QVERIFY2(ranges.bodyTempDelaySMin == expectedRanges.bodyTempDelaySMin, "BT delay s min must match.");
    QVERIFY2(ranges.bodyTempDelaySMax == expectedRanges.bodyTempDelaySMax, "BT delay s max must match.");

    QVERIFY2(ranges.bloodPressureSysEqOrLessBot == expectedRanges.bloodPressureSysEqOrLessBot, "BP EQLT bot must match.");
    QVERIFY2(ranges.bloodPressureSysEqOrLessTop == expectedRanges.bloodPressureSysEqOrLessTop, "BP EQLT top must match.");
    QVERIFY2(ranges.bloodPressureSysEqOrMoreBot == expectedRanges.bloodPressureSysEqOrMoreBot, "BP EQGT bot must match.");
    QVERIFY2(ranges.bloodPressureSysEqOrMoreTop == expectedRanges.bloodPressureSysEqOrMoreTop, "BP EQGT top must match.");
    QVERIFY2(ranges.bloodPressureDelaySMin == expectedRanges.bloodPressureDelaySMin, "BP delay s min must match.");
    QVERIFY2(ranges.bloodPressureDelaySMax == expectedRanges.bloodPressureDelaySMax, "BP delay s max must match.");
}

MosquittoMessage *DatakeeperQueriesParserTests::newMosquittoMessage(const QString &payload)
{
    return new MosquittoMessage("monitor/001122334455", payload, this);
}

QTEST_MAIN(DatakeeperQueriesParserTests)
#include "datakeeperqueriesparser_tst.moc"
