/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef DATAKEEPERQUERIESPARSER_H
#define DATAKEEPERQUERIESPARSER_H

#include <QString>
#include <QAbstractItemModel>
#include <QVector>
#include <QUrl>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>

class QJsonArray;

using namespace MoSimPa;

class DatakeeperQueriesParser
{
public:

    enum Replies {
        NotRecognizableReply,
        InternmentsReply,
        SpO2Reply,
        HeartRateReply,
        BloodPressureReply,
        BodyTemperatureReply,
        CommandResultReply,
        AlarmsThresholdsReply,
        AlarmsRangesReply,
        GenerateReportReply,
    };

    struct SpO2 {
        int64_t time;
        int32_t spo2;
    };

    struct HeartRate {
        int64_t time;
        int32_t heartRate;
    };

    struct BloodPressure {
        int64_t time;
        int16_t systolic;
        int16_t diastolic;
    };

    struct BodyTemperature {
        int64_t time;
        int16_t temp;
    };

    struct CommandResult {
        QString id;
        int32_t internmentId;
        QString command;
        QString result;
    };

    struct AlarmsThresholds {
        QString id;
        db_id_t internmentId;
        QString lastUpdate;
        spo2_t spO2Lt;
        uint8_t spO2DelayS;
        heart_rate_t hrLt;
        heart_rate_t hrGt;
        uint8_t hRDelayS;
        body_temp_t bTLt;
        body_temp_t bTGt;
        uint8_t bTDelayS;
        blood_pressure_t bPSysLt;
        blood_pressure_t bPSysGt;
        uint8_t bPDelayS;
    };

    struct AlarmsRanges {
        QString id;

        int32_t spO2EqOrLessPercBot;
        int32_t spO2EqOrLessPercTop;
        uint8_t spO2DelaySMin;
        uint8_t spO2DelaySMax;

        int32_t heartRateEqOrLessBPMBot;
        int32_t heartRateEqOrLessBPMTop;
        int32_t heartRateEqOrMoreBPMBot;
        int32_t heartRateEqOrMoreBPMTop;
        uint8_t heartRateDelaySMin;
        uint8_t heartRateDelaySMax;

        int32_t bodyTempEqOrLessCelsBot;
        int32_t bodyTempEqOrLessCelsTop;
        int32_t bodyTempEqOrMoreCelsBot;
        int32_t bodyTempEqOrMoreCelsTop;
        uint8_t bodyTempDelaySMin;
        uint8_t bodyTempDelaySMax;

        int32_t bloodPressureSysEqOrLessBot;
        int32_t bloodPressureSysEqOrLessTop;
        int32_t bloodPressureSysEqOrMoreBot;
        int32_t bloodPressureSysEqOrMoreTop;
        uint8_t bloodPressureDelaySMin;
        uint8_t bloodPressureDelaySMax;
    };

    struct Report {
        QString id;
        QUrl url;
    };

    static Replies typeOfReply(const struct mosquitto_message * msg);
    static db_id_t parseSpO2SensorData(const struct mosquitto_message * msg, QVector<struct SpO2> & data);
    static db_id_t parseHeartRateData(const struct mosquitto_message * msg, QVector<struct HeartRate> & data);
    static db_id_t parseBloodPressureData(const struct mosquitto_message * msg, QVector<struct BloodPressure> & data);
    static db_id_t parseBodyTemperatureData(const struct mosquitto_message * msg, QVector<struct BodyTemperature> & data);
    static void parseCommandResult(const struct mosquitto_message * msg, CommandResult & result);
    static bool parseAlarmsThresholds(const struct mosquitto_message * msg, AlarmsThresholds &thresholds);
    static bool parseAlarmsRanges(const struct mosquitto_message * msg, AlarmsRanges & ranges);
    static bool parseGenerateReport(const struct mosquitto_message * msg, Report & report);

private:
    static bool checkForString(const QJsonObject & object, const QString & string, uint8_t & bottom, uint8_t & top);
    static bool checkForString(const QJsonObject & object, const QString & string, int32_t & bottom, int32_t & top);
    static db_id_t checkInternmentId(const QJsonObject & object);
    static time_t checkTime(const QJsonObject & object);
};

#endif // DATAKEEPERQUERIESPARSER_H
