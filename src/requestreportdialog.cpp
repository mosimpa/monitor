#include <QtWidgets>
#include <QLocale>
#include "requestreportdialog.h"
#include "ui_requestreportdialog.h"

RequestReportDialog::RequestReportDialog(const InternmentsModel::InternmentData &data, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RequestReportDialog), _internmentId(data.internmentId)
{
    ui->setupUi(this);

    ui->internmentIdLabel->setText(QString(tr("<strong>Internment ID:</strong> %1").arg(data.internmentId)));

    QString type = data.locType == QStringLiteral("local") ? tr("Local") : tr("External");
    ui->locationLabel->setText(QString(tr("<strong>Location:</strong> %1 - %2").arg(data.locDesc).arg(type)));

    const QString displayFormat("yyyy-MM-dd hh");

    // Trim down the minutes.
    auto dT = QDateTime::fromSecsSinceEpoch(data.fromTime);
    auto time = dT.time();
    time.setHMS(time.hour(), 0, 0);
    dT.setTime(time);

    ui->fromDateTimeEdit->setDateTime(dT);
    ui->fromDateTimeEdit->setMinimumDateTime(dT);
    ui->fromDateTimeEdit->setCalendarPopup(true);
    ui->fromDateTimeEdit->setDisplayFormat(displayFormat);

    // Trim down the minutes.
    dT = QDateTime::currentDateTime();
    time = dT.time();
    time.setHMS(time.hour(), 0, 0);
    dT.setTime(time);

    ui->toDateTimeEdit->setDateTime(dT);
    ui->toDateTimeEdit->setMinimumDateTime(QDateTime::fromSecsSinceEpoch(data.fromTime + 3600));
    ui->toDateTimeEdit->setCalendarPopup(true);
    ui->toDateTimeEdit->setDisplayFormat(displayFormat);

    connect(ui->fromDateTimeEdit, &QDateTimeEdit::dateTimeChanged, this, &RequestReportDialog::checkTimeSpan);
    connect(ui->toDateTimeEdit, &QDateTimeEdit::dateTimeChanged, this, &RequestReportDialog::checkTimeSpan);

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &RequestReportDialog::confirmRequest);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &RequestReportDialog::reject);
}

RequestReportDialog::~RequestReportDialog()
{
    delete ui;
}

void RequestReportDialog::checkTimeSpan()
{
    auto secs = ui->fromDateTimeEdit->dateTime().secsTo(ui->toDateTimeEdit->dateTime());

    if(secs <= 0)
    {
        ui->toDateTimeEdit->setDateTime(ui->fromDateTimeEdit->dateTime().addSecs(3600));
        return;
    }

    if((secs % 3600) == 0)
        return;

    secs = ((secs / 3600) + 1) * 3600;
    ui->toDateTimeEdit->setDateTime(ui->fromDateTimeEdit->dateTime().addSecs(secs));
}

void RequestReportDialog::confirmRequest()
{
    emit reportRequested(_internmentId,
                         ui->fromDateTimeEdit->dateTime().toSecsSinceEpoch(),
                         ui->toDateTimeEdit->dateTime().toSecsSinceEpoch());
}
