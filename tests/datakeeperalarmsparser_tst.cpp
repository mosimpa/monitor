#include <QTest>
#include <QVector>
#include <QDebug>

#include <mosquitto.h>

#include "../src/datakeeperalarmsparser.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "datakeeperalarmsparser_tst.h"

void DatakeeperAlarmsParserTests::checkTopic()
{
    QVERIFY2(DatakeeperAlarmsParser::topic() == QStringLiteral("datakeeper/alarms"),
             "Topic must match.");
}

void DatakeeperAlarmsParserTests::checkTypeOfAlarm_data()
{
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<DatakeeperAlarmsParser::TypeOfAlarm>("expectedTypeOfAlarm");

    QTest::addRow("invalid_json") << newMosquittoMessage("foo") << DatakeeperAlarmsParser::AlarmUnknown;
    QTest::addRow("unknown") << newMosquittoMessage("{\"foo\":\"bar\"}") << DatakeeperAlarmsParser::AlarmUnknown;
    QTest::addRow("internments") << newMosquittoMessage("{\"internments\":[]}") << DatakeeperAlarmsParser::AlarmInternments;
    QTest::addRow("devices") << newMosquittoMessage("{\"devices\":[]}") << DatakeeperAlarmsParser::AlarmDevices;
}

void DatakeeperAlarmsParserTests::checkTypeOfAlarm()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(DatakeeperAlarmsParser::TypeOfAlarm, expectedTypeOfAlarm);

    DatakeeperAlarmsParser::TypeOfAlarm typeOfAlarm;

    typeOfAlarm = DatakeeperAlarmsParser::typeOfAlarm(&(msg->msg));

    QVERIFY2(typeOfAlarm == expectedTypeOfAlarm, "The type of alarm must match.");
}

void DatakeeperAlarmsParserTests::checkParserInternmentsAlarms_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<QVector<DatakeeperAlarmsParser::InternmentsAlarms>>("expectedAlarms");

    QVector<DatakeeperAlarmsParser::InternmentsAlarms> alarms;

    QTest::addRow("empty_internments") << newMosquittoMessage("{\"internments\":[]}") << alarms;

    DatakeeperAlarmsParser::InternmentsAlarms intAlarms;

    intAlarms.internmentId = 1;
    intAlarms.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::AlarmSeverity::Red;
    alarms << intAlarms;

    intAlarms.internmentId = 2;
    intAlarms.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::AlarmSeverity::Red;
    alarms << intAlarms;

    intAlarms.internmentId = 3;
    intAlarms.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::AlarmSeverity::Red;
    alarms << intAlarms;

    intAlarms.internmentId = 4;
    intAlarms.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::AlarmSeverity::Red;
    alarms << intAlarms;

    QTest::addRow("four_internments")
            << newMosquittoMessage("{\"internments\":["
                                   "{\"id\":1,\"spo2\":\"critic\"},"
                                   "{\"id\":2,\"spo2\":\"critic\",\"heart_rate\":\"critic\"},"
                                   "{\"id\":3,\"spo2\":\"critic\",\"heart_rate\":\"critic\",\"body_temp\":\"critic\"},"
                                   "{\"id\":4,\"spo2\":\"critic\",\"heart_rate\":\"critic\",\"body_temp\":\"critic\",\"blood_pressure\":\"critic\"}]}")
            << alarms;
}

void DatakeeperAlarmsParserTests::checkParserInternmentsAlarms()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(QVector<DatakeeperAlarmsParser::InternmentsAlarms>, expectedAlarms);
    QVector<DatakeeperAlarmsParser::InternmentsAlarms> alarms;

    alarms = DatakeeperAlarmsParser::parseInternmentsAlarmsMessage(&(msg->msg));

    QVERIFY2(alarms.size() == expectedAlarms.size(), "The size of the alarms must match.");

    for(int i = 0; i < alarms.size(); i++)
    {
        QVERIFY(alarms.at(i).internmentId == expectedAlarms.at(i).internmentId);

        if(expectedAlarms.at(i).alarms.contains(DatakeeperAlarmsParser::SpO2))
        {
            QVERIFY(alarms.at(i).alarms.contains(DatakeeperAlarmsParser::SpO2));
            QVERIFY(alarms.at(i).alarms.value(DatakeeperAlarmsParser::SpO2) == expectedAlarms.at(i).alarms.value(DatakeeperAlarmsParser::SpO2));
        }

        if(expectedAlarms.at(i).alarms.contains(DatakeeperAlarmsParser::HeartRate))
        {
            QVERIFY(alarms.at(i).alarms.contains(DatakeeperAlarmsParser::HeartRate));
            QVERIFY(alarms.at(i).alarms.value(DatakeeperAlarmsParser::HeartRate) == expectedAlarms.at(i).alarms.value(DatakeeperAlarmsParser::HeartRate));
        }

        if(expectedAlarms.at(i).alarms.contains(DatakeeperAlarmsParser::BodyTemp))
        {
            QVERIFY(alarms.at(i).alarms.contains(DatakeeperAlarmsParser::BodyTemp));
            QVERIFY(alarms.at(i).alarms.value(DatakeeperAlarmsParser::BodyTemp) == expectedAlarms.at(i).alarms.value(DatakeeperAlarmsParser::BodyTemp));
        }

        if(expectedAlarms.at(i).alarms.contains(DatakeeperAlarmsParser::BloodPressure))
        {
            QVERIFY(alarms.at(i).alarms.contains(DatakeeperAlarmsParser::BloodPressure));
            QVERIFY(alarms.at(i).alarms.value(DatakeeperAlarmsParser::BloodPressure) == expectedAlarms.at(i).alarms.value(DatakeeperAlarmsParser::BloodPressure));
        }
    }
}

void DatakeeperAlarmsParserTests::checkParseDeviceAlarmMessage_data()
{
    QTest::addColumn<MosquittoMessage *>("msg");
    QTest::addColumn<bool>("isEmpty");
    QTest::addColumn<QVector<DatakeeperAlarmsParser::DeviceData>>("devData");

    const QString topic = DatakeeperAlarmsParser::topic();
    QString payload;
    QVector<DatakeeperAlarmsParser::DeviceData> alarms;

    DatakeeperAlarmsParser::DeviceData data1, data2, data3;

    // Everything OK.
    data1.mac = QStringLiteral("001122334455");
    data1.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data1.battmV = 8000;
    data1.devMissingAlarmLevel = DatakeeperAlarmsParser::Yellow;
    data1.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    data2.mac = QStringLiteral("112233445566");
    data2.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data2.battmV = 7680;
    data2.devMissingAlarmLevel = DatakeeperAlarmsParser::Orange;
    data2.battLowAlarmLevel = DatakeeperAlarmsParser::Orange;

    data3.mac = QStringLiteral("112233445566");
    data3.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data3.battmV = 7680;
    data3.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    data3.battLowAlarmLevel = DatakeeperAlarmsParser::Red;

    alarms << data1 << data2 << data3;

    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("everything_ok") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // Second entry has wrong mac.
    alarms.clear();
    alarms << data1 << data3;

    payload.clear();

    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg("foo").arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("second_has_wrong_mac") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has wrong date.
    alarms.clear();
    alarms << data2 << data3;

    payload.clear();

    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg("foo").arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_wrong_date") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // Third entry has battmV < 0.
    alarms.clear();
    alarms << data1 << data2;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(-1).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("third_entry_batt_minus_0") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has battmV == minimum.
    alarms.clear();
    data1.battmV = std::numeric_limits<uint16_t>::min();
    alarms << data1 << data2 << data3;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_batt_max") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has battmV == maximum.
    alarms.clear();
    data1.battmV = std::numeric_limits<uint16_t>::max();
    alarms << data1 << data2 << data3;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(data1.battmV).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_batt_max") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has battmV > maximum.
    alarms.clear();
    alarms << data2 << data3;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(std::numeric_limits<uint16_t>::max()+1).arg("yellow").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_batt_max+1") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has wrong device_missing level.
    alarms.clear();
    alarms << data2 << data3;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(std::numeric_limits<uint16_t>::max()+1).arg("wrong_level").arg("green"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_wrong_dev_missing_level") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;

    // First entry has wrong battery_low level.
    alarms.clear();
    alarms << data2 << data3;

    payload.clear();
    payload = QString("{\"devices\":[");
    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data1.mac).arg(data1.lastSeen.toString(Qt::ISODateWithMs)).arg(std::numeric_limits<uint16_t>::max()+1).arg("yellow").arg("wrong_level"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"},")
                   .arg(data2.mac).arg(data2.lastSeen.toString(Qt::ISODateWithMs)).arg(data2.battmV).arg("orange").arg("orange"));

    payload.append(QString("{\"mac\":\"%1\",\"last_seen\":\"%2\",\"battmV\":%3,\"device_missing\":\"%4\",\"battery_low\":\"%5\"}")
                   .arg(data3.mac).arg(data3.lastSeen.toString(Qt::ISODateWithMs)).arg(data3.battmV).arg("green").arg("red"));

    payload.append(QString("]}"));

    QTest::addRow("first_entry_wrong_dev_missing_level") << new MosquittoMessage(topic, payload.toUtf8(), this) << false << alarms;
}

void DatakeeperAlarmsParserTests::checkParseDeviceAlarmMessage()
{
    QFETCH(MosquittoMessage *, msg);
    QFETCH(bool, isEmpty);
    QFETCH(QVector<DatakeeperAlarmsParser::DeviceData>, devData);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    auto parsedData = DatakeeperAlarmsParser::parseDeviceAlarmMessage(&(msg->msg));

    QVERIFY2(isEmpty == parsedData.isEmpty(), "Emptiness should be as expected.");

    if(isEmpty)
        return;

    QVERIFY2(parsedData.size() == devData.size(), "Vector sizes must match.");

    for(int i = 0; i < devData.size(); i++)
    {
        const auto received = parsedData.at(i);
        const auto expected = devData.at(i);

        QVERIFY2(received.mac == expected.mac, "MAC must match");
        QVERIFY2(received.lastSeen == expected.lastSeen, "Last seen must match");
        QVERIFY2(received.battmV == expected.battmV, "Battery voltaje must match");
        QVERIFY2(received.devMissingAlarmLevel == expected.devMissingAlarmLevel, "Device missing alarm level must match");
        QVERIFY2(received.battLowAlarmLevel == expected.battLowAlarmLevel, "Battery low alarm level must match");
    }
}

MosquittoMessage *DatakeeperAlarmsParserTests::newMosquittoMessage(const QString &payload)
{
    return new MosquittoMessage("monitor/001122334455", payload, this);
}

QTEST_MAIN(DatakeeperAlarmsParserTests)
#include "datakeeperalarmsparser_tst.moc"
