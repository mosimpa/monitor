#include <QVector>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QDateTime>
#include <QVariant>

#include "datakeeperalarmsparser.h"

DatakeeperAlarmsParser::TypeOfAlarm DatakeeperAlarmsParser::typeOfAlarm(const mosquitto_message *msg)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return AlarmUnknown;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(mainObject.contains("internments") && mainObject["internments"].isArray())
        return AlarmInternments;

    if(mainObject.contains("devices") && mainObject["devices"].isArray())
        return AlarmDevices;

    return AlarmUnknown;
}

QVector<DatakeeperAlarmsParser::InternmentsAlarms> DatakeeperAlarmsParser::parseInternmentsAlarmsMessage(const mosquitto_message *msg)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);
    QVector<DatakeeperAlarmsParser::InternmentsAlarms> alarms;

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return alarms;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("internments") || !mainObject["internments"].isArray())
        return alarms;

    QJsonArray internments = mainObject["internments"].toArray();

    for(int i = 0; i < internments.size(); i++)
    {
        InternmentsAlarms intAlarms;

        if(!internments.at(i).isObject())
            continue;

        auto internment = internments.at(i).toObject();

        if(!internment.contains("id") && !internment["id"].isDouble())
            continue;

        intAlarms.internmentId = static_cast<db_id_t>(internment["id"].toDouble(-1.0));

        if(internment.contains("spo2") && internment["spo2"].isString())
        {
            auto severity = internment["spo2"].toString("");
            if(severity == QStringLiteral("critic"))
                intAlarms.alarms[SpO2] = AlarmSeverity::Red;
            else
                intAlarms.alarms[SpO2] = AlarmSeverity::Green;
        }

        if(internment.contains("heart_rate") && internment["heart_rate"].isString())
        {
            auto severity = internment["heart_rate"].toString("");
            if(severity == QStringLiteral("critic"))
                intAlarms.alarms[HeartRate] = AlarmSeverity::Red;
            else
                intAlarms.alarms[HeartRate] = AlarmSeverity::Green;
        }

        if(internment.contains("body_temp") && internment["body_temp"].isString())
        {
            auto severity = internment["body_temp"].toString("");
            if(severity == QStringLiteral("critic"))
                intAlarms.alarms[BodyTemp] = AlarmSeverity::Red;
            else
                intAlarms.alarms[BodyTemp] = AlarmSeverity::Green;
        }

        if(internment.contains("blood_pressure") && internment["blood_pressure"].isString())
        {
            auto severity = internment["blood_pressure"].toString("");
            if(severity == QStringLiteral("critic"))
                intAlarms.alarms[BloodPressure] = AlarmSeverity::Red;
            else
                intAlarms.alarms[BloodPressure] = AlarmSeverity::Green;
        }

        alarms.append(intAlarms);
    }

    return alarms;
}

QVector<DatakeeperAlarmsParser::DeviceData> DatakeeperAlarmsParser::parseDeviceAlarmMessage(const mosquitto_message *msg)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);
    QVector<DeviceData> data;

    QRegExp macRx("^([0-9a-f]{2}){6}$");
    Q_ASSERT_X(macRx.isValid(), __FUNCTION__, "The MAC regular expression must be valid.");

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return data;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("devices") || !mainObject["devices"].isArray())
        return data;

    QJsonArray devices = mainObject["devices"].toArray();

    for(int i = 0; i < devices.size(); i++)
    {
        if(!devices.at(i).isObject())
            continue;

        auto dev = devices.at(i).toObject();

        if(!dev.contains("mac") || !dev["mac"].isString() ||
           !dev.contains("last_seen") || !dev["last_seen"].isString() ||
           !dev.contains("battmV") || !dev["battmV"].isDouble() ||
           !dev.contains("device_missing") || !dev["device_missing"].isString() ||
           !dev.contains("battery_low") || !dev["battery_low"].isString())
            continue;

        DeviceData devData;

        // MAC.
        devData.mac = dev["mac"].toString("");
        if(devData.mac.isEmpty() || !macRx.exactMatch(devData.mac))
            continue;

        // Last seen.
        devData.lastSeen = QDateTime::fromString(dev["last_seen"].toString(""), Qt::ISODate);
        if(!devData.lastSeen.isValid())
            continue;

        // Battery voltage in mV.
        auto batt = dev["battmV"].toDouble(-1.0);

        if((batt < 0.0) || (batt > std::numeric_limits<uint16_t>::max()))
            continue;

        devData.battmV = static_cast<uint16_t>(batt);

        // Alarm levels.
        devData.devMissingAlarmLevel = parseAlarmSeverity(dev["device_missing"].toString());
        devData.battLowAlarmLevel = parseAlarmSeverity(dev["battery_low"].toString());

        data.append(devData);
    }

    return data;
}

DatakeeperAlarmsParser::AlarmSeverity DatakeeperAlarmsParser::parseAlarmSeverity(const QString &severity)
{
    if(severity == QStringLiteral("green"))
        return Green;

    if(severity == QStringLiteral("yellow"))
        return Yellow;

    if(severity == QStringLiteral("orange"))
        return Orange;

    /*if(severity == QStringLiteral("red"))*/ // Default value.
    return Red;
}

DatakeeperAlarmsParser::AlarmSeverity DatakeeperAlarmsParser::mostSeverityAlarm(const DatakeeperAlarmsParser::DeviceData & data)
{
    return (data.devMissingAlarmLevel > data.battLowAlarmLevel) ?
            data.devMissingAlarmLevel : data.battLowAlarmLevel;
}
