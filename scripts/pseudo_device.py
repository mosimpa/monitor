#!/usr/bin/python3

'''
Documentation, License etc.

@package fake_data_deliverer

In order to run this script on Debian you need to install some packages:

  sudo apt install python3-netifaces python3-paho-mqtt

'''
import paho.mqtt.client as mqtt # Import the MQTT library
import time # The time library is useful for delays
import calendar
import json
import os
import netifaces
import random
import math
from datetime import timezone, datetime

from threading import Timer,Thread,Event

class perpetualTimer():
   def __init__(self,t,hFunction):
      self.t=t
      self.counter = 0
      self.hFunction = hFunction
      self.thread = Timer(self.t,self.handle_function)

   def handle_function(self):
      self.counter = self.counter + 1
      self.hFunction(self.counter)
      self.thread = Timer(self.t,self.handle_function)
      self.thread.start()

   def start(self):
      self.thread.start()

   def cancel(self):
      self.thread.cancel()

def unixEpoch():
    return int(datetime.now(tz=timezone.utc).timestamp())

def publishRead(payload):
    topic = "reads/"+mac.replace(':','')
    print("Publishing to topic: " + topic + " with payload: " + payload + "\n")
    ourClient.publish(topic=topic, payload=payload)

def sendInfo(counter):
    print("Publicando en PID" + str(os.getpid()))
    payload = '{{"WiFiMAC":"{}","battmV":{},"time":{},"msgId":{}}}'.format(mac.replace(':',''), random.randrange(7300, 8800, 100), unixEpoch(), counter)
    topic = "devices/info"
    print("Publishing to topic: " + topic + " with payload: " + payload + "\n")
    ourClient.publish(topic=topic, payload=payload)

def sendData(counter):
    seconds = calendar.timegm(time.gmtime())
    print("Publicando en PID" + str(os.getpid()))
    spo2 = 950 + int(20 * math.sin(2*math.pi*(seconds % 86400)/86400) + random.gauss(0, 1))
    hr = 1300 + int(-200*math.cos(2*math.pi*(seconds % 86400)/86400) + random.triangular(-50, 50))

    sign = -1*math.cos(4*math.pi*(seconds % 86400)/86400)
    rand = int(20*(seconds % 86400)/86400 + random.gauss(0, 1))
    if sign > 0:
        bt = 370 + rand
    else:
        bt = 370 - rand

    payload = '{{"spo2":[{{"time":{},"SpO2":{}}}],"heartR":[{{"time":{},"heartR":{}}}],"bodyT":[{{"time":{},"temp":{}}}]}}'.format(seconds, spo2, seconds, hr, seconds, bt)
    publishRead(payload)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    topic = "monitor/"+mac.replace(':','')
    client.subscribe(topic)

    sendInfo(0);

    tInfo = perpetualTimer(60, sendInfo)
    tInfo.start()

    tData = perpetualTimer(6, sendData)
    tData.start()

# Our "on message" event
def on_message (client, userdata, message):
  print("Message received")
  topic = str(message.topic)
  msg = str(message.payload.decode("utf-8"))
  print(topic + ": " + msg)

# Main program loop
def main():
  print("Program started.")

  random.seed(55)

  # \todo Use the subscribed topic as client id.
  global ourClient
  ourClient = mqtt.Client("test"+str(os.getpid()), clean_session = True) # Create a MQTT client object
  ourClient.enable_logger()
  ourClient.on_connect = on_connect
  ourClient.on_message = on_message
  # \todo Should we also set on_disconnect()?
  # \todo Do we set a will with will_set()?
  print("Ready to connect.")
  ourClient.connect(host = "raspberrypi", port = 1883, keepalive = 60) # Connect to the test MQTT broker
  print("Connection issued.")

  interfaces = netifaces.interfaces()
  if not interfaces:
      print("No ethernet interfaces found, can not use the script.")
      return;

  for i in interfaces:
      iface = i
      if iface != 'lo':
          break

  global mac
  #mac = netifaces.ifaddresses(iface)[netifaces.AF_LINK][0]["addr"]
  mac = '002219fe4d5d'
  print("Using interface " + iface + " with MAC addr " + mac)

  # Blocking call that processes network traffic, dispatches callbacks and
  # handles reconnecting.
  # Other loop*() functions are available that give a threaded interface and a
  # manual interface.
  #
  # Except for the first connection attempt when using connect_async, use
  # retry_first_connection=True to make it retry the first connection. Warning:
  # This might lead to situations where the client keeps connecting to an non
  # existing host without failing.
  ourClient.loop_forever(retry_first_connection = True)

if __name__ == '__main__':
  main()
