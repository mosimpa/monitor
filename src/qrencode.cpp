#include <QCoreApplication>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTemporaryFile>
#include <QDebug>
#include <QDir>
#include <QPixmap>

#include "qrencode.h"

QREncode::QREncode(QObject *parent) : QObject(parent)
{
    mProcess = new QProcess(this);
    connect(mProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(processFinished(int, QProcess::ExitStatus)));

    mFile = new QTemporaryFile(QString(QDir::tempPath() + "/" + QCoreApplication::applicationName() + "_XXXXXX.png"), this);
}

void QREncode::encode(QString string, int dotSize, QREncode::ErrorCorrectionLevel level)
{
    QStringList arguments;

    arguments << QString("-s%1").arg(dotSize);

    switch(level){
    case LevelL:
        arguments << QStringLiteral("-lL");
        break;

    case LevelM:
        arguments << QStringLiteral("-lM");
        break;

    case LevelQ:
        arguments << QStringLiteral("-lQ");
        break;

    case LevelH:
        arguments << QStringLiteral("-lH");
        break;
    }

    if(!mFile->open())
    {
        processFinished(1, QProcess::ExitStatus::NormalExit);
        return;
    }
    mFile->close();

    arguments << "-o" << mFile->fileName();

    arguments << QString("%1").arg(string);

    mProcess->start("qrencode", arguments);
}

void QREncode::processFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{
    auto img = QPixmap(mFile->fileName());

    emit qrCodeGenerated(img);
}
