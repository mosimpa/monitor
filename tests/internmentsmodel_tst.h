#ifndef INTERNMENTSMODEL_TST_H
#define INTERNMENTSMODEL_TST_H

#include <QObject>
#include <QtTest/QTest>

#include <mosquitto.h>
#include "mosquittomessage.h"
#include "../src/internmentsmodel.h"

Q_DECLARE_METATYPE(InternmentsModel::InternmentData);

class InternmentsModelTests : public QObject
{
    Q_OBJECT

private slots:
    void checkParsePayload_data();
    void checkParsePayload();
    void checkSeverityToColor();
    void checkUpdateInternmentsAlarms();
    void checkUpdateDevicesAlarms();

    void checkDefaultUIStatus();

private:
    void createInternments(InternmentsModel * im);
    MosquittoMessage * newMosquittoMessage(const QString & payload);
};

#endif // INTERNMENTSMODEL_TST_H
