/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QTimer>
#include <QDebug>
#include <QPen>
#include <QPixmap>
#include <QDateTime>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_series_data.h>
#include <qwt_point_data.h>
#include <qwt_symbol.h>
#include <qwt_plot_marker.h>

#include <mosimpaqt/scaling.h>

#include "qrencode.h"
#include "dataengine.h"
#include "devicedatawidget.h"
#include "ui_devicedatawidget.h"


DeviceDataWidget::DeviceDataWidget(DataEngine *dataEngine, const InternmentsModel::InternmentData &data, QWidget *parent) : QDialog(parent), mData(data),
    ui(new Ui::DeviceDataWidget), mDataEngine(dataEngine)
{
    mHistoryMinutes = 5;

    // Request current alarm values.
    connect(mDataEngine, &DataEngine::alarmsThresholdsReceived, this, &DeviceDataWidget::applyCurrentThresholds);
    mDataEngine->requestAlarmsThresholds(data.internmentId);

    ui->setupUi(this);
    QString patient = QString("%1, %2").arg(data.surname).arg(data.name);
    setInformation(patient, data.gender,
                   QString("%1, %2").arg(data.locDesc).arg(data.locType),
                   data.device);
    setWindowTitle(QString("%3, %4").arg(data.locDesc).arg(data.locType));

    ui->oxigenSatQwtPlot->setCanvasBackground(Qt::black);
    ui->oxigenSatQwtPlot->setAxisTitle(QwtPlot::xBottom, tr("Seconds"));
    ui->oxigenSatQwtPlot->enableAxis(QwtPlot::yRight, true);
    ui->oxigenSatQwtPlot->enableAxis(QwtPlot::yLeft, false);
    ui->oxigenSatQwtPlot->setAxisAutoScale(QwtPlot::yRight, false);
    // https://gitlab.com/mosimpa/documentation/-/issues/127#note_468190447
    ui->oxigenSatQwtPlot->setAxisScale(QwtPlot::yRight, 80.0, 100.0, 5.0);

    ui->heartBeatQwtPlot->setCanvasBackground(Qt::black);
    ui->heartBeatQwtPlot->setAxisTitle(QwtPlot::xBottom, tr("Seconds"));
    ui->heartBeatQwtPlot->enableAxis(QwtPlot::yRight, true);
    ui->heartBeatQwtPlot->enableAxis(QwtPlot::yLeft, false);
    ui->heartBeatQwtPlot->setAxisAutoScale(QwtPlot::yRight, false);
    ui->heartBeatQwtPlot->setAxisScale(QwtPlot::yRight, 30.0, 160.0, 10.0);

    ui->bodyTemperatureQwtPlot->setCanvasBackground(Qt::black);
    ui->bodyTemperatureQwtPlot->setAxisTitle(QwtPlot::xBottom, tr("Seconds"));
    ui->bodyTemperatureQwtPlot->enableAxis(QwtPlot::yRight, true);
    ui->bodyTemperatureQwtPlot->enableAxis(QwtPlot::yLeft, false);
    ui->bodyTemperatureQwtPlot->setAxisAutoScale(QwtPlot::yRight, false);
    // https://gitlab.com/mosimpa/documentation/-/issues/127#note_468489994
    ui->bodyTemperatureQwtPlot->setAxisScale(QwtPlot::yRight, 35.0, 41.0, 1.0);

    mOxigenSatCurve = new QwtPlotCurve(QString(""));
    mOxigenSatCurve->setPen(Qt::green, 4);
    mHeartBeatsCurve = new QwtPlotCurve(QString(""));
    mHeartBeatsCurve->setPen(Qt::cyan, 4);
    mBodyTemperatureCurve = new QwtPlotCurve(QString(""));
    mBodyTemperatureCurve->setPen(Qt::yellow, 4);

    mQR = new QREncode(this);
    connect(mQR, &QREncode::qrCodeGenerated, this, &DeviceDataWidget::setQr);
    mQR->encode(QString("{\"mac\":\"%1\"}").arg(mData.device));

    // Initialize the values in 0.
    ui->oxigenSatValueLabel->setText(QString("0.0"));
    ui->heartBeatValueLabel->setText(QString("0.0"));
    ui->bodyTemperatureValueLabel->setText(QString("0.0"));
}

void DeviceDataWidget::prepareHorizontalMarker(QwtPlotMarker *marker, double y, QColor color, int width)
{
    marker->setValue(0.0, y);
    marker->setLineStyle(QwtPlotMarker::HLine);
    marker->setLabelAlignment(Qt::AlignRight|Qt::AlignBottom);
    marker->setLinePen(color, width, Qt::DashLine);
}

DeviceDataWidget::~DeviceDataWidget()
{
    delete mOxigenSatCurve;
    delete mHeartBeatsCurve;
    delete mBodyTemperatureCurve;

    delete ui;
}

void DeviceDataWidget::setInformation(const QString & patient, const QString & gender,
                                      const QString & bed, const QString & deviceId)
{
    Q_UNUSED(patient)
    ui->genderLabel->setText(tr("Gender: %1").arg(gender));
    ui->bedLabel->setText(tr("Bed: %1").arg(bed));
    ui->deviceIdLabel->setText(tr("Dev. ID: %1").arg(deviceId));
}

void DeviceDataWidget::addOxigenSaturation(const time_t time, const spo2_t spo2)
{
    if(spo2 <= mThresholds.spO2Lt)
        ui->oxigenSatValueLabel->setStyleSheet(redBackgroundStyleSheet());
    else
        ui->oxigenSatValueLabel->setStyleSheet(greenBackgroundStyleSheet());

    const double SCALED_SPO2 = Scaling::spO2WireToPercentage(spo2);

    ui->oxigenSatValueLabel->setText(QString("%1").arg(SCALED_SPO2, 0, 'f', 1));

    mOxigenTimeData.append(time);
    mOxigenData.append(SCALED_SPO2);

    // Resize the X axis.
    QVector<double> xData = mOxigenTimeData;
    for(int i = xData.size() - 1; i >= 0; i--)
    {
        xData[i] = xData.at(i) - mOxigenTimeData.last();

        if(xData[i] < mHistoryMinutes * -60)
        {
            xData = xData.mid(i);
            mOxigenTimeData = mOxigenTimeData.mid(i);
            mOxigenData = mOxigenData.mid(i);
            break;
        }
    }

    auto data = new QwtPointArrayData(xData, mOxigenData);
    mOxigenSatCurve->setData(data);
    mOxigenSatCurve->setYAxis(QwtPlot::yRight);
    mOxigenSatCurve->attach(ui->oxigenSatQwtPlot);
    ui->oxigenSatQwtPlot->setAxisScale(QwtPlot::xBottom, xData.first(), xData.last());
    ui->oxigenSatQwtPlot->replot();
}

void DeviceDataWidget::addOxigenSaturation(QVector<DatakeeperQueriesParser::SpO2> spo2)
{
    struct {
            bool operator()(const DatakeeperQueriesParser::SpO2 a, const DatakeeperQueriesParser::SpO2 b) const
            {
                return a.time < b.time;
            }
    } customLess;
    std::sort(spo2.begin(), spo2.end(), customLess);

    for(int i = 0; i < spo2.size(); i++)
        addOxigenSaturation(spo2.at(i).time, spo2.at(i).spo2);
}

void DeviceDataWidget::addHeartRate(const time_t time, const heart_rate_t heartRate)
{
    if((heartRate > mThresholds.hrLt) && (heartRate < mThresholds.hrGt))
        ui->heartBeatValueLabel->setStyleSheet(greenBackgroundStyleSheet());
    else
        ui->heartBeatValueLabel->setStyleSheet(redBackgroundStyleSheet());

    const double SCALED_HR = Scaling::heartRateWireToBPM(heartRate);

    ui->heartBeatValueLabel->setText(QString("%1").arg(SCALED_HR));

    mHeartRateTimeData.append(time);
    mHeartRateData.append(SCALED_HR);

    // Resize the X axis.
    QVector<double> xData = mHeartRateTimeData;
    for(int i = xData.size() - 1; i >= 0; i--)
    {
        xData[i] = xData.at(i) - mHeartRateTimeData.last();

        if(xData[i] < mHistoryMinutes * -60)
        {
            xData = xData.mid(i);
            mHeartRateTimeData = mHeartRateTimeData.mid(i);
            mHeartRateData = mHeartRateData.mid(i);
            break;
        }
    }

    auto data = new QwtPointArrayData(xData, mHeartRateData);
    mHeartBeatsCurve->setData(data);
    mHeartBeatsCurve->setYAxis(QwtPlot::yRight);
    mHeartBeatsCurve->attach(ui->heartBeatQwtPlot);
    ui->heartBeatQwtPlot->setAxisScale(QwtPlot::xBottom, xData.first(), xData.last());
    ui->heartBeatQwtPlot->replot();
}

void DeviceDataWidget::addHeartRate(QVector<DatakeeperQueriesParser::HeartRate> heartRate)
{
    struct {
            bool operator()(const DatakeeperQueriesParser::HeartRate a, const DatakeeperQueriesParser::HeartRate b) const
            {
                return a.time < b.time;
            }
    } customLess;
    std::sort(heartRate.begin(), heartRate.end(), customLess);

    for(int i = 0; i < heartRate.size(); i++)
        addHeartRate(heartRate.at(i).time, heartRate.at(i).heartRate);
}

void DeviceDataWidget::addBodyTemperature(const time_t time, const body_temp_t temp)
{
    if((temp > mThresholds.bTLt) && (temp < mThresholds.bTGt))
        ui->bodyTemperatureValueLabel->setStyleSheet(greenBackgroundStyleSheet());
    else
        ui->bodyTemperatureValueLabel->setStyleSheet(redBackgroundStyleSheet());

    const double SCALED_TEMP = Scaling::bodyTemperatureWireToDegCelsius(temp);

    ui->bodyTemperatureValueLabel->setText(QString("%1").arg(SCALED_TEMP, 0, 'f', 1));

    mBodyTemperatureTimeData.append(time);
    mBodyTemperatureData.append(SCALED_TEMP);

    // Resize the X axis.
    QVector<double> xData = mBodyTemperatureTimeData;
    for(int i = xData.size() - 1; i >= 0; i--)
    {
        xData[i] = xData.at(i) - mBodyTemperatureTimeData.last();

        if(xData[i] < mHistoryMinutes * -60)
        {
            xData = xData.mid(i);
            mBodyTemperatureTimeData = mBodyTemperatureTimeData.mid(i);
            mBodyTemperatureData = mBodyTemperatureData.mid(i);
            break;
        }
    }

    auto data = new QwtPointArrayData(xData, mBodyTemperatureData);
    mBodyTemperatureCurve->setData(data);
    mBodyTemperatureCurve->setYAxis(QwtPlot::yRight);
    mBodyTemperatureCurve->attach(ui->bodyTemperatureQwtPlot);
    ui->bodyTemperatureQwtPlot->setAxisScale(QwtPlot::xBottom, xData.first(), xData.last());
    ui->bodyTemperatureQwtPlot->replot();
}

void DeviceDataWidget::addBodyTemperature(QVector<DatakeeperQueriesParser::BodyTemperature> bodyTemp)
{
    struct {
            bool operator()(const DatakeeperQueriesParser::BodyTemperature a, const DatakeeperQueriesParser::BodyTemperature b) const
            {
                return a.time < b.time;
            }
    } customLess;
    std::sort(bodyTemp.begin(), bodyTemp.end(), customLess);

    for(int i = 0; i < bodyTemp.size(); i++)
        addBodyTemperature(bodyTemp.at(i).time, bodyTemp.at(i).temp);
}

void DeviceDataWidget::addData(const ReadsParser &parser)
{
    for(int i = 0; i < parser.spO2Values().size(); i++)
        addOxigenSaturation(parser.spO2Values().at(i).time, parser.spO2Values().at(i).spO2);

    for(int i = 0; i < parser.heartRateValues().size(); i++)
        addHeartRate(parser.heartRateValues().at(i).time, parser.heartRateValues().at(i).heartR);

    for(int i = 0; i < parser.bodyTempValues().size(); i++)
        addBodyTemperature(parser.bodyTempValues().at(i).time, parser.bodyTempValues().at(i).bodyTemp);
}

void DeviceDataWidget::closeEvent(QCloseEvent *event)
{
    emit closed(mData.internmentId);
    QDialog::closeEvent(event);
}

void DeviceDataWidget::setQr(const QPixmap &image)
{
    ui->qrLabel->setText("");
    ui->qrLabel->setPixmap(image);
}

void DeviceDataWidget::applyCurrentThresholds(DatakeeperQueriesParser::AlarmsThresholds thresholds)
{
    mThresholds = thresholds;

    qDebug() << Scaling::spO2WireToPercentage(thresholds.spO2Lt) << Scaling::heartRateWireToBPM(thresholds.hrLt) << Scaling::heartRateWireToBPM(thresholds.hrGt);

    /// \todo Should these be removed by hand?
    mOxigenLowThresholdMarker = new QwtPlotMarker();
    mOxigenLowThresholdMarker->setYAxis(QwtPlot::yRight);
    prepareHorizontalMarker(mOxigenLowThresholdMarker, Scaling::spO2WireToPercentage(thresholds.spO2Lt), Qt::red, 1);
    mOxigenLowThresholdMarker->attach(ui->oxigenSatQwtPlot);

    mHeartRateLowThresholdMarker = new QwtPlotMarker();
    mHeartRateLowThresholdMarker->setYAxis(QwtPlot::yRight);
    prepareHorizontalMarker(mHeartRateLowThresholdMarker, Scaling::heartRateWireToBPM(thresholds.hrLt), Qt::red, 1);
    mHeartRateLowThresholdMarker->attach(ui->heartBeatQwtPlot);

    mHeartRateHighThresholdMarker = new QwtPlotMarker();
    mHeartRateHighThresholdMarker->setYAxis(QwtPlot::yRight);
    prepareHorizontalMarker(mHeartRateHighThresholdMarker, Scaling::heartRateWireToBPM(thresholds.hrGt), Qt::red, 1);
    mHeartRateHighThresholdMarker->attach(ui->heartBeatQwtPlot);

    mBodyTempLowThresholdMarker = new QwtPlotMarker();
    mBodyTempLowThresholdMarker->setYAxis(QwtPlot::yRight);
    prepareHorizontalMarker(mBodyTempLowThresholdMarker, Scaling::bodyTemperatureWireToDegCelsius(thresholds.bTLt), Qt::red, 1);
    mBodyTempLowThresholdMarker->attach(ui->bodyTemperatureQwtPlot);

    mBodyTempHighThresholdMarker = new QwtPlotMarker();
    mBodyTempHighThresholdMarker->setYAxis(QwtPlot::yRight);
    prepareHorizontalMarker(mBodyTempHighThresholdMarker, Scaling::bodyTemperatureWireToDegCelsius(thresholds.bTGt), Qt::red, 1);
    mBodyTempHighThresholdMarker->attach(ui->bodyTemperatureQwtPlot);
}
