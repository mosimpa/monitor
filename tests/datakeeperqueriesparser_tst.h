#ifndef DATAKEEPERQUERIESPARSERTESTS_H
#define DATAKEEPERQUERIESPARSERTESTS_H

#include <QObject>
#include <QtTest/QTest>

#include <mosquitto.h>
#include "mosquittomessage.h"
#include "../src/datakeeperqueriesparser.h"

Q_DECLARE_METATYPE(DatakeeperQueriesParser::Replies);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::SpO2);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::HeartRate);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::BloodPressure);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::BodyTemperature);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::AlarmsThresholds);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::AlarmsRanges);
Q_DECLARE_METATYPE(DatakeeperQueriesParser::Report)

class DatakeeperQueriesParserTests : public QObject
{
    Q_OBJECT

private slots:
    void checkTypeOfReplies_data();
    void checkTypeOfReplies();

    /// \todo See what to do with internemts parsing, as it's also part of Internments.
    void checkParseInternmentsMessage_data();
    void checkParseInternmentsMessage();

    void checkParseSpO2SensorData_data();
    void checkParseSpO2SensorData();

    void checkParseHeartRateData_data();
    void checkParseHeartRateData();

    void checkParseBloodPressureData_data();
    void checkParseBloodPressureData();

    void checkParseBodyTemperatureData_data();
    void checkParseBodyTemperatureData();

    void checkGenerateReportReply_data();
    void checkGenerateReportReply();

    void checkParseAlarmsThresholds_data();
    void checkParseAlarmsThresholds();

    void checkParseAlarmsRanges_data();
    void checkParseAlarmsRanges();

private:
    MosquittoMessage * newMosquittoMessage(const QString & payload);
};

#endif // DATAKEEPERQUERIESPARSERTESTS_H
