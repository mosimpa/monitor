/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>

#include <mosquittoclient.h>
#include "signalsparser.h"

void SignalsParser::parseMessage(const mosquitto_message * msg, Signals &signal)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    signal = SignalNone;

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("signal") || !mainObject["signal"].isString())
        return;

    if(mainObject["signal"].toString() == QStringLiteral("internments_changed"))
        signal = SignalInternmentsChanged;
}
