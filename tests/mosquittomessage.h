#ifndef MOSQUITTOMESSAGE_H
#define MOSQUITTOMESSAGE_H

#include <QObject>
#include <QByteArray>
#include <QString>

#include <mosquitto.h>

class MosquittoMessage : public QObject
{
    Q_OBJECT
public:
    MosquittoMessage(const QString &topic, const QString &payload, QObject *parent = nullptr);
    ~MosquittoMessage();

    mosquitto_message msg;
};

#endif // MOSQUITTOMESSAGE_H
