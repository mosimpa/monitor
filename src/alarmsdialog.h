#ifndef ALARMASDIALOG_H
#define ALARMASDIALOG_H

#include <QWidget>
#include <QDialog>

#include "internmentsmodel.h"
#include "datakeeperqueriesparser.h"

namespace Ui {
class AlarmsDialog;
}

class DataEngine;

class AlarmsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AlarmsDialog(DataEngine * dataEngine, const InternmentsModel::InternmentData & data, const DatakeeperQueriesParser::AlarmsRanges & alarmsRanges, QWidget *parent = nullptr);
    ~AlarmsDialog();

signals:
    void dialogClosed();

private slots:
    void applyCurrentThresholds(DatakeeperQueriesParser::AlarmsThresholds thresholds);
    void heartRateLTChanged(double value);
    void heartRateGTChanged(double value);
    void setHeartRateLTValue(double value);
    void setHeartRateGTValue(double value);

    void bodyTemperatureLTChanged(double value);
    void bodyTemperatureGTChanged(double value);
    void setBodyTemperatureLTValue(double value);
    void setBodyTemperatureGTValue(double value);

    void save();

private:
    void setInformation(const uint8_t patient, const QString &gender, const QString &bed, const QString &deviceId);
    Ui::AlarmsDialog *ui;
    DataEngine * mDataEngine;
    InternmentsModel::InternmentData mData;
    DatakeeperQueriesParser::AlarmsThresholds mThresholds;

    double mHeartRateLT;
    double mHeartRateGT;
    double mBodyTempLT;
    double mBodyTempGT;

    static const int ALARM_DELAY_STEP = 15;

    DatakeeperQueriesParser::AlarmsRanges mAlarmsRanges;
};

#endif // ALARMASDIALOG_H
