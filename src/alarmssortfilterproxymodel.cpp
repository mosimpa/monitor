#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QDebug>

#include <mosimpaqt/scaling.h>

#include "internmentsmodel.h"

#include "alarmssortfilterproxymodel.h"

AlarmsSortFilterProxyModel::AlarmsSortFilterProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    _showOnlyAlarms = false;
}

void AlarmsSortFilterProxyModel::showOnlyInternmentsWithAlarms(const bool show)
{
    _showOnlyAlarms = show;
    invalidateFilter();
}

bool AlarmsSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if(!_showOnlyAlarms)
        return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);

    QModelIndex deviceIndex = sourceModel()->index(sourceRow, InternmentsModel::Columns::DeviceMAC, sourceParent);
    QModelIndex spo2Index = sourceModel()->index(sourceRow, InternmentsModel::Columns::SpO2, sourceParent);
    QModelIndex heartRateIndex = sourceModel()->index(sourceRow, InternmentsModel::Columns::HR, sourceParent);
    QModelIndex bodyTempIndex = sourceModel()->index(sourceRow, InternmentsModel::Columns::BT, sourceParent);

    auto internmentsM = dynamic_cast<InternmentsModel*>(sourceModel());

    // Hide columns with no alarms.
    if((internmentsM->data(deviceIndex, Qt::BackgroundRole) == QVariant()) &&
       (internmentsM->data(spo2Index, Qt::BackgroundRole) == QVariant()) &&
       (internmentsM->data(heartRateIndex, Qt::BackgroundRole) == QVariant()) &&
       (internmentsM->data(bodyTempIndex, Qt::BackgroundRole) == QVariant()))
        return false;

    return true;
}
