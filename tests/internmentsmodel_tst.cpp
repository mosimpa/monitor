#include <QMetaType>
#include <QString>
#include <QTest>
#include <QVector>
#include <QAbstractItemModel>
#include <QScopedPointer>
#include <QVariant>
#include <QColor>
#include <QSignalSpy>

#include <mosquitto.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "../src/internmentsmodel.h"
#include "../src/settings.h"

#include "mosquittomessage.h"
#include "internmentsmodel_tst.h"

void InternmentsModelTests::checkParsePayload_data()
{
    QTest::addColumn<MosquittoMessage*>("msg");
    QTest::addColumn<bool>("expectedRetval");
    QTest::addColumn<QString>("expectedID");
    QTest::addColumn<QVector<InternmentsModel::InternmentData>>("expectedData");
    QString payload;
    QString internmentStr;
    InternmentsModel::InternmentData data;
    QVector<InternmentsModel::InternmentData> dataVector;

    payload = QStringLiteral("foo");
    QTest::addRow("invalid_json") << newMosquittoMessage(payload) << false << QStringLiteral("0") << dataVector;

    payload = QStringLiteral("{\"id\":\"0\",\"internments\":[");

    // First entry.
    data.internmentId = 1;
    data.fromTime = 15454;
    data.device = QStringLiteral("002219fe4d5d");
    data.locDesc = QStringLiteral("Test");
    data.locType = QStringLiteral("local");
    data.name = QStringLiteral("Jhon");
    data.surname = QStringLiteral("Doe");
    data.age = 33;
    data.gender = 'm';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}},")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    // Second entry.
    data.internmentId = 2;
    data.fromTime = 15944;
    data.device = QStringLiteral("001122334455");
    data.locDesc = QStringLiteral("Test2");
    data.locType = QStringLiteral("local");
    data.name = QStringLiteral("Natalie");
    data.surname = QStringLiteral("Willson");
    data.age = 45;
    data.gender = 'f';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}},")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    // Third entry.
    data.internmentId = 3;
    data.fromTime = 15444;
    data.device = QStringLiteral("00500463ee4d");
    data.locDesc = QStringLiteral("Test3");
    data.locType = QStringLiteral("external");
    data.name = QStringLiteral("Filipus");
    data.surname = QStringLiteral("Granger");
    data.age = 68;
    data.gender = 'o';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}}")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    payload.append(QStringLiteral("]}"));

    QTest::addRow("valid_data") << newMosquittoMessage(payload) << true << QStringLiteral("0") << dataVector;
}

void InternmentsModelTests::checkParsePayload()
{
    QFETCH(MosquittoMessage*, msg);
    QFETCH(bool, expectedRetval);
    QFETCH(QString, expectedID);
    QFETCH(QVector<InternmentsModel::InternmentData>, expectedData);

    QVERIFY2(msg != nullptr, "Message should not be a null pointer");

    QScopedPointer<InternmentsModel> im(new InternmentsModel(this));

    auto retval = im->parsePayload(&(msg->msg));
    QVERIFY2(retval == expectedRetval, "retval and expectedRetval must match.");

    if(retval == false)
        return;

    auto devices = im->devices();

    QVERIFY2(devices.size() == expectedData.size(),
             "The number of expected data and number of devices parsed must match.");
    QVERIFY2(devices.size() == im->rowCount(QModelIndex()),
             "The number of devices parsed and rowCount must match.");

    for(int i = 0; i < devices.size(); i++)
    {
        auto dev = im->device(i);

        QVERIFY2(dev.internmentId == expectedData.at(i).internmentId, "Internment ID must match.");

        QVERIFY2(dev.device == expectedData.at(i).device, "device must match.");
        QVERIFY2(dev.device == devices.at(i), "device must match in the list.");

        QVERIFY2(dev.locDesc == expectedData.at(i).locDesc, "locDesc must match.");
        QVERIFY2(dev.locType == expectedData.at(i).locType, "locType must match.");

        QVERIFY2(dev.name == expectedData.at(i).name, "name must match.");
        QVERIFY2(dev.surname == expectedData.at(i).surname, "surname must match.");
        QVERIFY2(dev.age == expectedData.at(i).age, "age must match.");
        QVERIFY2(dev.gender == expectedData.at(i).gender, "gender must match.");

        // ReadsParser's structs are instant values that do not come from the parser.
    }

}

void InternmentsModelTests::checkSeverityToColor()
{
    QVariant result;

    result = InternmentsModel::severityToColor(DatakeeperAlarmsParser::AlarmSeverity::Green);
    QVERIFY(result == QVariant());

    result = InternmentsModel::severityToColor(DatakeeperAlarmsParser::AlarmSeverity::Yellow);
    QVERIFY(result == QVariant(QColor("#ffff00")));

    result = InternmentsModel::severityToColor(DatakeeperAlarmsParser::AlarmSeverity::Orange);
    QVERIFY(result == QVariant(QColor("#ffaa00")));

    result = InternmentsModel::severityToColor(DatakeeperAlarmsParser::AlarmSeverity::Red);
    QVERIFY(result == QVariant(QColor("#ff0000")));
}

void InternmentsModelTests::checkUpdateInternmentsAlarms()
{
    auto im = new InternmentsModel(this);
    createInternments(im);

    QSignalSpy playSoundAlarmsSpy(im, SIGNAL(playSoundAlarms(bool, bool)));

    // Create the data to be received.
    DatakeeperAlarmsParser::InternmentsAlarms alarm1, alarm2, alarm3;
    QVector<DatakeeperAlarmsParser::InternmentsAlarms> alarms;

    alarm1.internmentId = 1;
    alarm1.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Red;
    alarm1.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Orange;
    alarm1.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Yellow;
    alarm1.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Green;

    alarm2.internmentId = 2;
    alarm2.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Green;

    alarm3.internmentId = 3;
    alarm3.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Red;
    alarm3.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Red;
    alarm3.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Red;
    alarm3.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Red;

    alarms << alarm1 << alarm2 << alarm3;

    // Update the alarms.
    im->updateInternmentsAlarms(alarms);

    // Check that the internment sound alarm is set.
    QCOMPARE(playSoundAlarmsSpy.count(), 1);
    QList<QVariant> arguments = playSoundAlarmsSpy.takeFirst();
    QCOMPARE(arguments.size(), 2);
    QVERIFY(arguments.at(1).toBool() == true);

    // Now clear all the alarms.
    playSoundAlarmsSpy.clear();

    alarm1.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Green;
    alarm1.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Green;
    alarm1.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Green;
    alarm1.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Green;

    alarm2.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Green;
    alarm2.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Green;

    alarm3.alarms[DatakeeperAlarmsParser::SpO2] = DatakeeperAlarmsParser::Green;
    alarm3.alarms[DatakeeperAlarmsParser::HeartRate] = DatakeeperAlarmsParser::Green;
    alarm3.alarms[DatakeeperAlarmsParser::BodyTemp] = DatakeeperAlarmsParser::Green;
    alarm3.alarms[DatakeeperAlarmsParser::BloodPressure] = DatakeeperAlarmsParser::Green;

    alarms.clear();
    alarms << alarm1 << alarm2 << alarm3;

    // Update the alarms.
    im->updateInternmentsAlarms(alarms);

    // Check that the internment sound alarm is not set.
    QCOMPARE(playSoundAlarmsSpy.count(), 1);
    arguments = playSoundAlarmsSpy.takeFirst();
    QCOMPARE(arguments.size(), 2);
    QVERIFY(arguments.at(1).toBool() == false);
}

void InternmentsModelTests::checkUpdateDevicesAlarms()
{
    auto im = new InternmentsModel(this);
    createInternments(im);

    QSignalSpy playSoundAlarmsSpy(im, SIGNAL(playSoundAlarms(bool, bool)));

    // Create the data to be received.
    DatakeeperAlarmsParser::DeviceData data1, data2, data3;
    QVector<DatakeeperAlarmsParser::DeviceData> alarms;

    data1.mac = QStringLiteral("001122334455");
    data1.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data1.battmV = 8000;
    data1.devMissingAlarmLevel = DatakeeperAlarmsParser::Yellow;
    data1.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    data2.mac = QStringLiteral("112233445566");
    data2.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data2.battmV = 7680;
    data2.devMissingAlarmLevel = DatakeeperAlarmsParser::Orange;
    data2.battLowAlarmLevel = DatakeeperAlarmsParser::Orange;

    data3.mac = QStringLiteral("223344556677");
    data3.lastSeen = QDateTime::fromString("2020-07-14T17:19:10+03:00", Qt::ISODate);
    data3.battmV = 7680;
    data3.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    data3.battLowAlarmLevel = DatakeeperAlarmsParser::Red;


    alarms << data1 << data2 << data3;

    // Add the data to the interments.
    im->updateDevicesAlarms(alarms);

    // Now check that the correct background color is set.
    auto intBackgroundColor = im->data(im->index(0, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(intBackgroundColor == QColor("#ffff00"), "Color must match");

    intBackgroundColor = im->data(im->index(1, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(intBackgroundColor == QColor("#ffaa00"), "Color must match");

    intBackgroundColor = im->data(im->index(2, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(intBackgroundColor == QColor("#ff0000"), "Color must match");

    // Check that the device sound alarm is set.
    QCOMPARE(playSoundAlarmsSpy.count(), 1);
    QList<QVariant> arguments = playSoundAlarmsSpy.takeFirst();
    QCOMPARE(arguments.size(), 2);
    QVERIFY(arguments.at(0).toBool() == true);

    /**************************************************************************
     * Clear alarms.
     **************************************************************************/

    playSoundAlarmsSpy.clear();

    data1.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    data1.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    data2.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    data2.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    data3.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    data3.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    alarms.clear();

    alarms << data1 << data2 << data3;

    // Add the data to the interments.
    im->updateDevicesAlarms(alarms);

    // Now check that the correct background color is set.
    intBackgroundColor = im->data(im->index(0, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(!intBackgroundColor.isValid(), "Color must match");

    intBackgroundColor = im->data(im->index(2, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(!intBackgroundColor.isValid(), "Color must match");

    intBackgroundColor = im->data(im->index(3, InternmentsModel::Columns::DeviceMAC), Qt::BackgroundRole).value<QColor>();
    QVERIFY2(!intBackgroundColor.isValid(), "Color must match");

    // Check that the device sound alarm is not set.
    QCOMPARE(playSoundAlarmsSpy.count(), 1);
    arguments = playSoundAlarmsSpy.takeFirst();
    QCOMPARE(arguments.size(), 2);
    QVERIFY(arguments.at(0).toBool() == false);
}

void InternmentsModelTests::checkDefaultUIStatus()
{
    QScopedPointer<InternmentsModel> im(new InternmentsModel(this));
    bool ok;

    // Verify that the recently constructed InternmentsModel has no data in it.
    QVERIFY2(im->rowCount() == 0, "No rows in recently constructed InternmentsModel");
    QVERIFY2(im->columnCount() == InternmentsModel::Columns::Max, "The number of columns should be setted already.");

    // Now add a row.
    auto internmentStr = QString("{\"id\":\"0\",\"internments\":["
                                 "{\"internment_id\":1,\"from_time\":2,"
                                 "\"device\":\"112233445566\","
                                 "\"location\":{\"type\":\"local\",\"desc\":\"foo\"},"
                                 "\"patient\":{\"name\":\"Test\",\"surname\":\"UI\","
                                 "\"age\":15,\"gender\":\"m\"}}]}");

    // Now parse the interments data.
    auto mosqMsg = newMosquittoMessage(internmentStr);
    QVERIFY2(im->parsePayload(&(mosqMsg->msg)), "Parsing must suceed.");

    // Now we must check the default status of every column.

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == (InternmentsModel::DeviceMissing | InternmentsModel::BatteryLow), "Alarms == DeviceMissing | BatteryLow");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toString() == "--", "SpO2 == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toString() == "--", "HR == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::DisplayRole).toString() == "--", "BT == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Red), "Device MAC red background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Request report button no data color");

    /* Tests with battLowAlarmLevel == Green */

    // Now we decrease the device alarm's severity level to orange.
    im->mData.at(0)->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::Orange;
    im->mData.at(0)->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::DeviceMissing, "Alarms == DeviceMissing");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toString() == "--", "SpO2 == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toString() == "--", "HR == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::DisplayRole).toString() == "--", "BT == --");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Orange), "Device MAC orange background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Request report button no data color");

    // Now we decrease the alarm's severity level to yellow.
    im->mData.at(0)->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::Yellow;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::DeviceMissing, "Alarms == DeviceMissing");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toDouble(&ok)), "SpO2 == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "HR == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "BT == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Yellow), "Device MAC yellow background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Request report button no data color");

    // Now we decrease the alarm's severity level to green.
    im->mData.at(0)->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::NoAlarm, "Alarms == NoAlarm");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toDouble(&ok)), "SpO2 == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "HR == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "BT == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole).isValid() == false, "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole).isValid() == false, "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole).isValid() == false, "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole).isValid() == false, "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole).isValid() == false, "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Green), "Device MAC green background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole).isValid() == false, "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole).isValid() == false, "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole).isValid() == false, "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole).isValid() == false, "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole).isValid() == false, "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole).isValid() == false, "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole).isValid() == false, "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole).isValid() == false, "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole).isValid() == false, "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole).isValid() == false, "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole).isValid() == false, "Request report button no data color");

    /* Tests with devMissingAlarmLevel == Green */

    // Now we decrease the device alarm's severity level to orange.
    im->mData.at(0)->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::Green;
    im->mData.at(0)->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::Orange;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::BatteryLow, "Alarms == BatteryLow");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toDouble(&ok)), "SpO2 == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "HR == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::BT), Qt::DisplayRole).toDouble(&ok)), "BT == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Orange), "Device MAC orange background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Request report button no data color");

    // Now we decrease the alarm's severity level to yellow.
    im->mData.at(0)->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::Yellow;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::BatteryLow, "Alarms == BatteryLow");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toDouble(&ok)), "SpO2 == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "HR == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "BT == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Yellow), "Device MAC yellow background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole) == InternmentsModel::colorNoData(), "Request report button no data color");

    // Now we decrease the alarm's severity level to green.
    im->mData.at(0)->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::Green;

    // Display Role
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::DisplayRole).toInt() == 1, "Internment ID == 1");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::DisplayRole).toInt() == 15, "Age == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::DisplayRole).toString() == 'm', "Gender == 15");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::DisplayRole).toString() == "foo", "Location == foo");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::DisplayRole).toString() == "local", "Location type == local");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::DisplayRole).toString() == "112233445566", "Device MAC == 112233445566");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::DisplayRole).toUInt() == InternmentsModel::NoAlarm, "Alarms == NoAlarm");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::DisplayRole).toBool() == false, "Mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::SpO2), Qt::DisplayRole).toDouble(&ok)), "SpO2 == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::DisplayRole).toBool() == false, "SpO2 mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "HR == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::DisplayRole).toBool() == false, "HR mute device button == false");
    QVERIFY2(qFuzzyIsNull(im->data(im->index(0, InternmentsModel::HR), Qt::DisplayRole).toDouble(&ok)), "BT == 0.0");
    QVERIFY(ok);
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::DisplayRole).toBool() == false, "BT mute device button == false");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::DisplayRole).isValid() == false, "Plot button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::DisplayRole).isValid() == false, "Alarms setup button == not valid QVariant");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::DisplayRole).isValid() == false, "Request report button == not valid QVariant");

    // Background role.
    QVERIFY2(im->data(im->index(0, InternmentsModel::InternmentID), Qt::BackgroundRole).isValid() == false, "ID no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Age), Qt::BackgroundRole).isValid() == false, "Age no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::Gender), Qt::BackgroundRole).isValid() == false, "Gender no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocDesc), Qt::BackgroundRole).isValid() == false, "LocDesc no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::LocType), Qt::BackgroundRole).isValid() == false, "Location type no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceMAC), Qt::BackgroundRole) == InternmentsModel::severityToColor(DatakeeperAlarmsParser::Green), "Device MAC green background");
    QVERIFY2(im->data(im->index(0, InternmentsModel::DeviceAlarms), Qt::BackgroundRole).isValid() == false, "Alarms no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::MuteDeviceButton), Qt::BackgroundRole).isValid() == false, "Mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2), Qt::BackgroundRole).isValid() == false, "SpO2 no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::SpO2MuteButton), Qt::BackgroundRole).isValid() == false, "SpO2 mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HR), Qt::BackgroundRole).isValid() == false, "HR no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::HRMuteButton), Qt::BackgroundRole).isValid() == false, "HR mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BT), Qt::BackgroundRole).isValid() == false, "BT no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::BTMuteButton), Qt::BackgroundRole).isValid() == false, "BT mute device button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::PlotButton), Qt::BackgroundRole).isValid() == false, "Plot button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::AlarmsSetupButton), Qt::BackgroundRole).isValid() == false, "Alarms setup button no data color");
    QVERIFY2(im->data(im->index(0, InternmentsModel::RequestReportButton), Qt::BackgroundRole).isValid() == false, "Request report button no data color");
}

void InternmentsModelTests::createInternments(InternmentsModel * im)
{
    QString payload = QStringLiteral("{\"id\":\"0\",\"internments\":[");
    QVector<InternmentsModel::InternmentData> dataVector;
    QString internmentStr;
    InternmentsModel::InternmentData data;

    // First entry.
    data.internmentId = 1;
    data.device = QStringLiteral("001122334455");
    data.locDesc = QStringLiteral("Test");
    data.locType = QStringLiteral("local");
    data.name = QStringLiteral("Jhon");
    data.surname = QStringLiteral("Doe");
    data.age = 33;
    data.gender = 'm';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}},")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    // Second entry.
    data.internmentId = 2;
    data.device = QStringLiteral("112233445566");
    data.locDesc = QStringLiteral("Test2");
    data.locType = QStringLiteral("local");
    data.name = QStringLiteral("Natalie");
    data.surname = QStringLiteral("Willson");
    data.age = 45;
    data.gender = 'f';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}},")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    // Third entry.
    data.internmentId = 3;
    data.device = QStringLiteral("223344556677");
    data.locDesc = QStringLiteral("Test3");
    data.locType = QStringLiteral("external");
    data.name = QStringLiteral("Filipus");
    data.surname = QStringLiteral("Granger");
    data.age = 68;
    data.gender = 'o';

    internmentStr = QString("{\"internment_id\":%1,\"from_time\":%2,"
                           "\"device\":\"%10\","
                           "\"location\":{\"type\":\"%11\",\"desc\":\"%12\"},"
                           "\"patient\":{\"name\":\"%13\",\"surname\":\"%14\","
                           "\"age\":%15,\"gender\":\"%16\"}}")
            .arg(data.internmentId)
            .arg(data.fromTime)
            .arg(data.device)
            .arg(data.locType).arg(data.locDesc)
            .arg(data.name).arg(data.surname)
            .arg(data.age).arg(data.gender);
    payload.append(internmentStr);
    dataVector << data;

    payload.append(QStringLiteral("]}"));

    // Now parse the interments data.
    auto mosqMsg = newMosquittoMessage(payload);
    QVERIFY2(im->parsePayload(&(mosqMsg->msg)), "Parsing must suceed.");
}

MosquittoMessage *InternmentsModelTests::newMosquittoMessage(const QString &payload)
{
    return new MosquittoMessage("monitor/001122334455", payload, this);
}

QTEST_MAIN(InternmentsModelTests)
#include "internmentsmodel_tst.moc"
