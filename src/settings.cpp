#include <QObject>
#include <QSettings>
#include <QTextStream>
#include <QCoreApplication>
#include <QString>

#include "settings.h"

Settings::Settings(QObject *parent) : QObject(parent)
{
    if(mSettings == nullptr)
        mSettings = new QSettings();

    QTextStream stdoutStream(stdout);
    stdoutStream << tr("%1: settings are being stored in %2").arg(qApp->applicationName())
                                                             .arg(mSettings->fileName())
                 << endl;

    load();
}

Settings::~Settings()
{
    mSettings->sync();
}

/**
 * @brief Settings::instance Singleton instance.
 * @return Reference to the instance.
 */
Settings &Settings::instance()
{
    static Settings instance;
    return instance;
}

void Settings::setMqttBroker(const QString &broker)
{
    mSettings->beginGroup("mqtt");
    mMqttBroker = broker;
    mSettings->setValue(QStringLiteral("broker"), mMqttBroker);
    mSettings->endGroup();
}

void Settings::setMqttPort(const int16_t port)
{
    mSettings->beginGroup("mqtt");
    mMqttPort = port;
    mSettings->setValue(QStringLiteral("port"), mMqttPort);
    mSettings->endGroup();
}

void Settings::load()
{
    mSettings->beginGroup("mqtt");
    mMqttBroker = readOrSet(QStringLiteral("broker"), QStringLiteral("localhost"));
    mMqttPort = readOrSet(QStringLiteral("port"), 1883);

    if(mSettings->contains("tlsEnabled"))
        mTlsEnabled = mSettings->value("tlsEnabled").toBool();
    else
    {
        mTlsEnabled = false;
        mSettings->setValue("tlsEnabled", mTlsEnabled);
    }

    mCAFilePath = readOrSet(QStringLiteral("caFilePath"), QStringLiteral(""));
    mCADirPath = readOrSet(QStringLiteral("caDirPath"), QStringLiteral(""));

    mSettings->endGroup();
}

QString Settings::readOrSet(const QString &key, const QString & defaultValue)
{
    if(mSettings->contains(key))
        return mSettings->value(key).toString();

    mSettings->setValue(key, defaultValue);
    return defaultValue;
}

int16_t Settings::readOrSet(const QString &key, const int16_t defaultValue)
{
    if(mSettings->contains(key))
        return static_cast<int16_t>(mSettings->value(key).toUInt());

    mSettings->setValue(key, defaultValue);
    return defaultValue;
}

void Settings::setCADirPath(const QString &cADirPath)
{
    mSettings->beginGroup("mqtt");
    mCADirPath = cADirPath;
    mSettings->setValue("caDirPath", mCADirPath);
    mSettings->endGroup();
}

void Settings::setCAFilePath(const QString &cAFilePath)
{
    mSettings->beginGroup("mqtt");
    mCAFilePath = cAFilePath;
    mSettings->setValue("caFilePath", mCAFilePath);
    mSettings->endGroup();
}

void Settings::setTlsEnabled(bool tlsEnabled)
{
    mSettings->beginGroup("mqtt");
    mTlsEnabled = tlsEnabled;
    mSettings->setValue("tlsEnabled", mTlsEnabled);
    mSettings->endGroup();
}
