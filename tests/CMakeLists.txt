add_definitions(-DMONITOR_TEST="1")

# Datakeeper Query Parser tests.
add_executable(datakeeperqueriesparser_tst
  ../src/internmentsmodel.cpp
  ../src/datakeeperqueriesparser.cpp
  ../src/datakeeperalarmsparser.cpp
  datakeeperqueriesparser_tst.cpp
  mosquittomessage.cpp
)

target_link_libraries(datakeeperqueriesparser_tst
  Qt5::Test
  Qt5::Core
  Qt5::Gui
  Qt5::Multimedia
  MosimpaQt
  PkgConfig::MOSQUITTO
)

add_test(datakeeperqueriesparser_tst datakeeperqueriesparser_tst)

# InternmentsModel tests.

add_executable(internmentsmodel_tst
  ../src/internmentsmodel.cpp
  ../src/datakeeperalarmsparser.cpp
  internmentsmodel_tst.cpp
  mosquittomessage.cpp
)

target_link_libraries(internmentsmodel_tst
  Qt5::Test
  Qt5::Core
  Qt5::Gui
  Qt5::Multimedia
  MosimpaQt
  PkgConfig::MOSQUITTO
)

add_test(internmentsmodel_tst internmentsmodel_tst)

# SoundAlarmHandler tests.

add_executable(soundalarmhandler_tst
  ../src/soundalarmhandler.cpp
  ../src/datakeeperalarmsparser.cpp
  soundalarmhandler_tst.cpp
)

target_link_libraries(soundalarmhandler_tst
  Qt5::Test
  Qt5::Core
  Qt5::Gui
  Qt5::Multimedia
  MosimpaQt
  PkgConfig::MOSQUITTO
)

add_test(soundalarmhandler_tst soundalarmhandler_tst)

# Datakeeper Alarms Parser tests.
add_executable(datakeeperalarmsparser_tst
  ../src/datakeeperalarmsparser.cpp
  datakeeperalarmsparser_tst.cpp
  mosquittomessage.cpp
)

target_link_libraries(datakeeperalarmsparser_tst
  Qt5::Test
  Qt5::Core
  Qt5::Gui
  MosimpaQt
  PkgConfig::MOSQUITTO
)

add_test(datakeeperalarmsparser_tst datakeeperalarmsparser_tst)
