#ifndef DATAKEEPERALARMSPARSERTESTS_H
#define DATAKEEPERALARMSPARSERTESTS_H

#include <QObject>
#include <QtTest/QTest>

#include <mosquitto.h>
#include "mosquittomessage.h"
#include "../src/datakeeperalarmsparser.h"

Q_DECLARE_METATYPE(DatakeeperAlarmsParser::DeviceData);
Q_DECLARE_METATYPE(DatakeeperAlarmsParser::InternmentsAlarms);

class DatakeeperAlarmsParserTests : public QObject
{
    Q_OBJECT

private slots:
    void checkTopic();
    void checkTypeOfAlarm_data();
    void checkTypeOfAlarm();
    void checkParserInternmentsAlarms_data();
    void checkParserInternmentsAlarms();
    void checkParseDeviceAlarmMessage_data();
    void checkParseDeviceAlarmMessage();

private:
    MosquittoMessage * newMosquittoMessage(const QString & payload);
};

#endif // DATAKEEPERALARMSPARSERTESTS_H
