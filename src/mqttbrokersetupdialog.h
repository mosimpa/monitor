#ifndef MQTTBROKERSETUPDIALOG_H
#define MQTTBROKERSETUPDIALOG_H

#include <QDialog>

namespace Ui {
class MqttBrokerSetupDialog;
}

class MqttBrokerSetupDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MqttBrokerSetupDialog(QWidget *parent = nullptr);
    ~MqttBrokerSetupDialog();

private Q_SLOTS:
    void enableTls(int state);
    void accept();
    void reject();
    void searchCAFile();
    void searchCAPath();

private:
    Ui::MqttBrokerSetupDialog *ui;

    QString _originalHostname;
    int16_t _originalPort;
    bool _originalTlsIsEnabled;
    QString _originalCAFilePath;
    QString _originalCADirPath;
};

#endif // MQTTBROKERSETUPDIALOG_H
