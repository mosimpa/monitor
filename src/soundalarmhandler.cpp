#include <QSound>
#include <QTimer>

#include "soundalarmhandler.h"

SoundAlarmHandler::SoundAlarmHandler(QObject *parent) : QObject(parent)
{

#ifndef MONITOR_TEST
    mInternmentAlarmSound = new QSound(":/audios/internment_alarm.wav");
    mDeviceAlarmSound = new QSound(":/audios/device_alarm.wav");
    mDatakeeperOfflineSound = new QSound(":/audios/datakeeper_offline_alarm.wav");
#endif

    mAlarmTimer = new QTimer(this);
    mAlarmTimer->setSingleShot(false);
    mPlayDeviceAlarm = false;
    mPlayInternmentAlarm = false;
    mPlayDatakeeperOfflineAlarm = false;
    connect(mAlarmTimer, &QTimer::timeout, this, &SoundAlarmHandler::playSoundAlarms);
    mAlarmCounter = 0;
}

void SoundAlarmHandler::handleInternmentDeviceAlarmStatus(bool playDeviceAlarm, bool playIntermentAlarm)
{
    mPlayDeviceAlarm = playDeviceAlarm;
    mPlayInternmentAlarm = playIntermentAlarm;
    checkAlarmsTimerState();
}

void SoundAlarmHandler::handleDatakeeperOfflineAlarmStatus(bool isOffline)
{
    mPlayDatakeeperOfflineAlarm = isOffline;
}

void SoundAlarmHandler::playSoundAlarms()
{
#ifndef MONITOR_TEST
    if(mPlayDatakeeperOfflineAlarm)
    {
        if((mAlarmCounter%2) == 0)
            mDatakeeperOfflineSound->play();
    }
    else
    {
        if((mAlarmCounter%2) == 0)
        {
            if(mPlayInternmentAlarm)
                mInternmentAlarmSound->play();
        }
        else
        {
            if(mPlayDeviceAlarm)
                mDeviceAlarmSound->play();
        }
    }

    mAlarmCounter++;
#endif
}

bool SoundAlarmHandler::checkAlarmsTimerState()
{
    if(mPlayInternmentAlarm || mPlayDeviceAlarm || mPlayDatakeeperOfflineAlarm)
    {
        if(!mAlarmTimer->isActive())
        {
            mAlarmTimer->start(ALARM_PERIOD_MS);
            playSoundAlarms();
        }

        return true;
    }

    if(mAlarmTimer->isActive())
            mAlarmTimer->stop();

    return false;
}
