#ifndef SOUNDALARMHANDLER_TST_H
#define SOUNDALARMHANDLER_TST_H

#include <QObject>
#include <QtTest/QTest>

#include "../src/soundalarmhandler.h"

class SoundAlarmHandlerTests : public QObject
{
    Q_OBJECT

private slots:
    void checkCheckAlarmsTimerState();
};

#endif // SOUNDALARMHANDLER_TST_H
