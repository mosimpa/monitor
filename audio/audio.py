#!/usr/bin/python3
import math
import wave
import struct

def append_silence(audio, sample_rate = 44100.0, duration_milliseconds=500):
    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(0.0)

    return

def append_squarewave(audio, sample_rate = 44100.0, freq=440.0, duration_milliseconds=500, volume=1.0, rise_per=16):

    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    fundamental = 0
    onethird = 0
    oneseptimus = 0

    for x in range(int(num_samples)):
        if x < int(num_samples*rise_per/100):
            rise = 100*x/(num_samples*rise_per)
        else:
            rise = 1

        fundamental = 0.6 * rise * volume * math.sin(2 * math.pi * freq * ( x / sample_rate ))

        if (x % (sample_rate/freq)) < sample_rate/freq/4:
            onethird = 0.3 * rise * volume
        else:
            onethird = -0.3 * rise * volume

        if (x % (sample_rate/freq)) < sample_rate/freq/14:
            oneseptimus = 0.1 * rise * volume
        else:
            oneseptimus = -0.1* rise * volume

        audio.append(fundamental + onethird + oneseptimus)

    return

def append_sinewave(audio, sample_rate = 44100.0, freq=440.0, duration_milliseconds=500, volume=1.0, rise_per=16):

    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        if x < int(num_samples*rise_per/100):
            rise = 100*x/(num_samples*rise_per)
        else:
            rise = 1

        fundamental = 0.6 * rise * volume * math.sin(2 * math.pi * freq * ( x / sample_rate ))
        onethird = 0.3 * rise * volume * math.sin(2 * math.pi * freq * 3 * ( x / sample_rate ))
        oneseptimus = 0.1 * rise * volume * math.sin(2 * math.pi * freq * 7 * ( x / sample_rate ))
        audio.append(fundamental + onethird + oneseptimus)

    return


def save_wav(audio, file_name, sample_rate=44100.0):
    # Open up a wav file
    wav_file = wave.open(file_name,"w")

    # wav params
    nchannels = 1

    sampwidth = 2

    nframes = len(audio)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((nchannels, sampwidth, sample_rate, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theorically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in audio:
        wav_file.writeframes(struct.pack('h', int(sample * 32767.0)))

    wav_file.close()

    return


def main():
    audio = []
    sample_rate = 44100.0

    # Internment alarm.
    append_squarewave(audio, sample_rate, freq=330, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)
    append_squarewave(audio, sample_rate, freq=330, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)
    append_squarewave(audio, sample_rate, freq=330, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)

    save_wav(audio, "internment_alarm.wav")

    # Device alarm.
    audio = []
    append_squarewave(audio, sample_rate, freq=262, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)
    append_squarewave(audio, sample_rate, freq=294, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)
    append_squarewave(audio, sample_rate, freq=262, duration_milliseconds=200)
    append_silence(audio, sample_rate, duration_milliseconds=250)

    save_wav(audio, "device_alarm.wav")

    # Datakeeper offline alarm.
    audio = []
    append_squarewave(audio, sample_rate, freq=262, duration_milliseconds=100)
    append_silence(audio, sample_rate, duration_milliseconds=100)
    append_squarewave(audio, sample_rate, freq=262, duration_milliseconds=100)
    append_silence(audio, sample_rate, duration_milliseconds=100)
    append_squarewave(audio, sample_rate, freq=262, duration_milliseconds=100)
    append_silence(audio, sample_rate, duration_milliseconds=100)

    save_wav(audio, "datakeeper_offline_alarm.wav")

if __name__ == '__main__':
    main()
