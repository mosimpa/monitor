#ifndef INTERNMENTSDELEGATE_H
#define INTERNMENTSDELEGATE_H

#include <QStyledItemDelegate>
#include <QIcon>

class InternmentsDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    using QStyledItemDelegate::QStyledItemDelegate;

    InternmentsDelegate(QObject * parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;

    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const override;

private:
    QIcon _checkMarkIcon;
    QIcon _batteryIcon;
    QIcon _locationIcon;
    QIcon _batteryAndLocationIcon;
    const int _gap = 25;
};

#endif // INTERNMENTSDELEGATE_H
