#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QString>

/**
 * @brief The Settings class
 *
 * Check https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
 * on how the singleton pattern has been used.
 */

class Settings : public QObject
{
    Q_OBJECT
public:
    Settings(Settings const &) = delete;
    void operator=(Settings const &) = delete;
    static Settings & instance();

    QString mqttBroker() { return mMqttBroker; }
    int16_t mqttPort() { return mMqttPort; }

    bool tlsEnabled() { return mTlsEnabled; }
    QString caFilePath() { return mCAFilePath; }
    QString caDirPath() { return mCADirPath; }


    void setMqttBroker(const QString & broker);
    void setMqttPort(const int16_t port);

    void setTlsEnabled(bool tlsEnabled);
    void setCAFilePath(const QString &cAFilePath);
    void setCADirPath(const QString &cADirPath);

private:
    Settings(QObject *parent = 0);
    ~Settings();

    void load();
    QString readOrSet(const QString & key, const QString &defaultValue);
    int16_t readOrSet(const QString & key, const int16_t defaultValue);

    QString mMqttBroker;
    int16_t mMqttPort;
    bool mTlsEnabled;
    QString mCAFilePath;
    QString mCADirPath;

    QSettings * mSettings;
};

#endif // SETTINGS_H
