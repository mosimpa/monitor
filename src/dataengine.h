#ifndef DATAENGINE_H
#define DATAENGINE_H

#include <QObject>
#include <QVector>
#include <QString>
#include <QVector>
#include <QHash>
#include <QTimer>

#include <mosquitto.h>

#include <readsparser.h>
#include "datakeeperqueriesparser.h"
#include "internmentsmodel.h"
#include "soundalarmhandler.h"

class MosquittoClient;

class DataEngine : public QObject
{
    Q_OBJECT
public:
    explicit DataEngine(QObject *parent = nullptr);

    void connect();
    void requestInternments();
    void requestInternmentHistory(db_id_t internmentId, time_t fromTime, int32_t numSamples);
    void requestAlarmsThresholds(db_id_t internmentId);
    void requestAlarmsRanges();
    void requestReport(const db_id_t internmentId, const time_t fromTime, const time_t toTime);

    void sendAlarmsThresholds(const DatakeeperQueriesParser::AlarmsThresholds & thresholds);

    QVector<DatakeeperQueriesParser::SpO2> spO2HistoricData(const int32_t internmentId);
    QVector<DatakeeperQueriesParser::HeartRate> heartRateHistoricData(const int32_t internmentId);
    QVector<DatakeeperQueriesParser::BloodPressure> bloodPressureHistoricData(const int32_t internmentId);
    QVector<DatakeeperQueriesParser::BodyTemperature> bodyTemperatureHistoricData(const int32_t internmentId);

    InternmentsModel * internments() { return mInternments; }

signals:
    void subscribedToMonitor();
    void readsDataReceived(const ReadsParser & parser);
    void internmentsUpdated();
    void spO2HistoricDataAvailable(int32_t internmentId);
    void heartRateHistoricDataAvailable(int32_t internmentId);
    void bloodPressureHistoricDataAvailable(int32_t internmentId);
    void bodyTemperatureHistoricDataAvailable(int32_t internmentId);
    void commandResultReceived(DatakeeperQueriesParser::CommandResult result);
    void alarmsThresholdsReceived(DatakeeperQueriesParser::AlarmsThresholds thresholds);
    void alarmsRangesReceived(DatakeeperQueriesParser::AlarmsRanges ranges);
    void internmentsChanged();
    void reportReceived(DatakeeperQueriesParser::Report report);
    void datakeeperStatusChanged(bool isOnline);

private slots:
    void onConnectionTried(int result);
    void onErrorWhileLooping(int error);
    void onMessageReceived();
    void subscribeToDevices(const QVector<QString> & devices);
    void heartBeatTimedOut();

private:
    static const inline QString heartBeatTopic() { return QStringLiteral("datakeeper/heartbeat"); }
    MosquittoClient * mMosqClient;
    InternmentsModel * mInternments;
    SoundAlarmHandler * mSoundAlarmHandler;

    QString mMAC;
    int mQueryCounter;
    QHash<int32_t, QVector<DatakeeperQueriesParser::SpO2>> mSpO2Data;
    QHash<int32_t, QVector<DatakeeperQueriesParser::HeartRate>> mHeartRateData;
    QHash<int32_t, QVector<DatakeeperQueriesParser::BloodPressure>> mBloodPressureData;
    QHash<int32_t, QVector<DatakeeperQueriesParser::BodyTemperature>> mBodyTemperatureData;

    QTimer * mHBTimer;
    static const int HB_TIMER_TIMEOUT_MS = 5000;
    bool mIsDatakeeperOnline;
};

#endif // DATAENGINE_H
