#ifndef ALARMSSORTFILTERPROXYMODEL_H
#define ALARMSSORTFILTERPROXYMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

class AlarmsSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    AlarmsSortFilterProxyModel(QObject *parent = nullptr);

public slots:
    void showOnlyInternmentsWithAlarms(const bool show);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    bool _showOnlyAlarms;
};

#endif // ALARMSSORTFILTERPROXYMODEL_H
