#include <QObject>
#include <QAbstractTableModel>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>
#include <QDebug>
#include <QtGlobal>
#include <QBrush>
#include <QFontDatabase>
#include <QSound>
#include <QTimer>

#include <mosquitto.h>

#include <mosimpaqt/scaling.h>

#include "datakeeperalarmsparser.h"
#include "internmentsmodel.h"

InternmentsModel::InternmentsModel(QObject *parent) : QAbstractTableModel(parent)
{
    Q_ASSERT_X(BPSys < BPDias, "InternmentsModel::InternmentsModel()", "BPSys > BPDias, thus breaking table views updates with dataChanged.");
    Q_ASSERT_X(std::abs(BPSys - BPDias) == 1, "InternmentsModel::InternmentsModel()", "BPSys and BPDias should be contiguous.");
}

InternmentsModel::~InternmentsModel()
{
    clearData();
}

bool InternmentsModel::parsePayload(const mosquitto_message *msg)
{
    beginResetModel();

    QVector<InternmentData*> data;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("id") || !mainObject["id"].isString())
        return false;

    mId = mainObject["id"].toString("");

    if(!mainObject.contains("internments") || !mainObject["internments"].isArray())
        return false;

    QJsonArray internments = mainObject["internments"].toArray();

    for(int i = 0; i < internments.size(); i++)
    {
        auto * d = new InternmentData;
        d->spo2Values.time = -1;
        d->spo2Values.spO2 = 0;
        d->heartRateValues.time = -1;
        d->heartRateValues.heartR = 0;
        d->bloodPressureValues.time = -1;
        d->bloodPressureValues.sys = 0;
        d->bloodPressureValues.dias = 0;
        d->bodyTempValues.time = -1;
        d->bodyTempValues.bodyTemp = 0;

        d->spO2Severity = DatakeeperAlarmsParser::Green;
        d->heartRateSeverity = DatakeeperAlarmsParser::Green;
        d->bodyTempSeverity = DatakeeperAlarmsParser::Green;
        d->bloodPressureSeverity = DatakeeperAlarmsParser::Green;
        d->spo2AlarmMuted = false;
        d->hrAlarmMuted = false;
        d->btAlarmMuted = false;
        d->deviceAlarmMuted = false;

        data.append(d);

        if(!internments.at(i).isObject())
            goto onerror;

        QJsonObject internment = internments.at(i).toObject();

        if(!internment.contains("internment_id") || !internment["internment_id"].isDouble())
            goto onerror;
        d->internmentId = internment["internment_id"].toInt();

        if(!internment.contains("from_time") || !internment["from_time"].isDouble())
            goto onerror;
        d->fromTime = static_cast<time_t>(internment["from_time"].toDouble());

        // Device
        if(!internment.contains("device") || !internment["device"].isString())
            goto onerror;
        d->device = internment["device"].toString();
        d->deviceData = {};
        /*
         * Ensure the correct UI startup state, see
         * https://gitlab.com/mosimpa/documentation/-/issues/163#note_568269638
         */
        d->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::Red;
        d->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::Red;

        // Location.
        if(!internment.contains("location") || !internment["location"].isObject())
            goto onerror;

        QJsonObject location = internment["location"].toObject();
        if(!location.contains("desc") || !location["desc"].isString())
            goto onerror;
        d->locDesc = location["desc"].toString();

        if(!location.contains("type") || !location["type"].isString())
            goto onerror;
        d->locType = location["type"].toString();

        // Patient.
        if(!internment.contains("patient") || !internment["patient"].isObject())
            goto onerror;

        QJsonObject patient = internment["patient"].toObject();

        if(!patient.contains("name") || !patient["name"].isString())
            goto onerror;
        d->name = patient["name"].toString();

        if(!patient.contains("surname") || !patient["surname"].isString())
            goto onerror;
        d->surname = patient["surname"].toString();

        if(!patient.contains("age") || !patient["age"].isDouble())
            goto onerror;
        d->age = static_cast<uint8_t>(patient["age"].toInt());

        if(!patient.contains("gender") || !patient["gender"].isString())
            goto onerror;

        if(patient["gender"].toString().isEmpty())
            goto onerror;
        d->gender = patient["gender"].toString().at(0);

        // Sync the alarms muted states.
        if(mInternmentIdHash.contains(d->internmentId))
        {
            d->spo2AlarmMuted = mInternmentIdHash[d->internmentId]->spo2AlarmMuted;
            d->hrAlarmMuted = mInternmentIdHash[d->internmentId]->hrAlarmMuted;
            d->btAlarmMuted = mInternmentIdHash[d->internmentId]->btAlarmMuted;
            d->deviceAlarmMuted = mInternmentIdHash[d->internmentId]->deviceAlarmMuted;
        }
    }

    // Now clear up everything and keep the new values.
    clearData();
    mData = data;

    // Fill in the hash for faster lookups.
    for(int i = 0; i < mData.size(); i++)
    {
        mMacHash.insert(mData.at(i)->device, mData.at(i));
        mInternmentIdHash.insert(mData.at(i)->internmentId, mData.at(i));
    }

    endResetModel();
    return true;

onerror:
    endResetModel();
    return false;
}

int InternmentsModel::rowCount(const QModelIndex &/*parent*/) const
{
    return mData.size();
}

int InternmentsModel::columnCount(const QModelIndex &/*parent*/) const
{
    return Columns::Max;
}

QVariant InternmentsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();

    if(index.column() >= Columns::Max)
        return QVariant();

    if(role == Qt::TextAlignmentRole)
    {
        switch(static_cast<Columns>(index.column())) {
        case Age:
        case Gender:
        case DeviceMAC:
        case SpO2:
        case HR:
        case BPSys:
        case BT:
            return Qt::AlignCenter;

        default:
            return QVariant();
        }
    }

    if(role == Qt::DisplayRole)
    {
        AlarmsFlags alarmFlags = NoAlarm;

        bool showHypen = false;

        if(mData.at(row)->deviceData.devMissingAlarmLevel >=  DatakeeperAlarmsParser::Orange)
            showHypen = true;

        switch(static_cast<Columns>(index.column())) {
        case InternmentID:
            return mData.at(row)->internmentId;

        case Surname:
            return mData.at(row)->surname;

        case Name:
            return mData.at(row)->name;

        case Age:
            return mData.at(row)->age;

        case Gender:
            return mData.at(row)->gender;

        case LocDesc:
            return mData.at(row)->locDesc;

        case LocType:
            return mData.at(row)->locType;

        case DeviceMAC:
            return mData.at(row)->device;

        case DeviceAlarms:
            if(mData.at(row)->deviceData.devMissingAlarmLevel > DatakeeperAlarmsParser::Green)
                alarmFlags.setFlag(DeviceMissing);
            if(mData.at(row)->deviceData.battLowAlarmLevel > DatakeeperAlarmsParser::Green)
                alarmFlags.setFlag(BatteryLow);
            return alarmFlags.operator unsigned int();

        case MuteDeviceButton:
            return mData.at(row)->deviceAlarmMuted;

        case SpO2:
            if(showHypen)
                break;
            return Scaling::spO2WireToPercentage(mData.at(row)->spo2Values.spO2);

        case SpO2MuteButton:
            return mData.at(row)->spo2AlarmMuted;

        case HR:
            if(showHypen)
                break;
            return Scaling::heartRateWireToBPM(mData.at(row)->heartRateValues.heartR);


        case HRMuteButton:
            return mData.at(row)->hrAlarmMuted;

        case BT:
            if(showHypen)
                break;
            return Scaling::bodyTemperatureWireToDegCelsius(mData.at(row)->bodyTempValues.bodyTemp);

        case BTMuteButton:
            return mData.at(row)->btAlarmMuted;

        case BPSys:
            if(showHypen)
                break;
            return mData.at(row)->bloodPressureValues.sys;

        case BPDias:
            if(showHypen)
                break;
            return mData.at(row)->bloodPressureValues.dias;

        case PlotButton:
        case AlarmsSetupButton:
        case RequestReportButton:
        case Max:
            return QVariant();
        }

        return QString("--");
    }

    if(role == Qt::BackgroundRole)
    {
        QVariant color;

        switch(static_cast<Columns>(index.column())) {
        case DeviceMAC:
            return severityToColor(DatakeeperAlarmsParser::mostSeverityAlarm(mData.at(row)->deviceData));

        case SpO2:
            color = severityToColor(mData.at(row)->spO2Severity);
            break;

        case HR:
            color = severityToColor(mData.at(row)->heartRateSeverity);
            break;

        case BPSys:
            color = severityToColor(mData.at(row)->bloodPressureSeverity);
            break;

        case BT:
            color = severityToColor(mData.at(row)->bodyTempSeverity);
            break;

        default:
            color = QVariant();
            break;
        }

        if(DatakeeperAlarmsParser::mostSeverityAlarm(mData.at(row)->deviceData) > DatakeeperAlarmsParser::Green)
            color = colorNoData();

        return color;
    }

    if((role == Qt::FontRole) && (static_cast<Columns>(index.column()) == DeviceMAC))
    {
        return QFontDatabase::systemFont(QFontDatabase::FixedFont);
    }

    if((role == Qt::ToolTipRole) && (static_cast<Columns>(index.column()) == DeviceAlarms))
    {
        return QString(tr("Battery voltaje: %1 [mV] - Last seen: %2").arg(mData.at(row)->deviceData.battmV).arg(mData.at(row)->deviceData.lastSeen.toString(Qt::ISODate)));
    }

    return QVariant();
}

QVariant InternmentsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Horizontal)
        return QVariant();

    switch(static_cast<Columns>(section)) {
    case InternmentID:
        return QString(tr("Internment"));

    case Surname:
        return QString(tr("Surname"));

    case Name:
        return QString(tr("Name"));

    case Age:
        return QString(tr("Age"));

    case Gender:
        return QString(tr("Gender"));

    case LocDesc:
        return QString(tr("Location"));

    case LocType:
        return QString(tr("Type"));

    case DeviceMAC:
        return QString(tr("Device"));

    case DeviceAlarms:
        return QString(tr("Status"));

    case MuteDeviceButton:
        return tr("Mute");

    case SpO2:
        return QString(tr("SpO2 %"));

    case SpO2MuteButton:
        return QString(tr("Mute"));

    case HR:
        return QString(tr("HR bpm"));

    case HRMuteButton:
        return QString(tr("Mute"));

    case BT:
        return QString(tr("BT ºC"));

    case BTMuteButton:
        return QString(tr("Mute"));

    case BPSys:
        return QString(tr("BP Sys"));

    case BPDias:
        return QString(tr("BP Dias"));

    case PlotButton:
        return QString(tr("Plot"));

    case AlarmsSetupButton:
        return QString(tr("Alarms"));

    case RequestReportButton:
        return QString(tr("Report"));

    case Max:
        return QVariant();
    }

    return QVariant();
}

InternmentsModel::InternmentData InternmentsModel::device(const int row) const
{
    struct InternmentData data;
    data.device = QString("");

    if((row < 0) || (row >= Max))
        return data;

    data = *mData.at(row);

    return data;
}

QVector<QString> InternmentsModel::devices()
{
    QVector<QString> devs;

    for(int i = 0; i < mData.size(); i++)
        devs.append(mData.at(i)->device);

    return devs;
}

void InternmentsModel::updateInstantValues(const ReadsParser &parser)
{
    if(!mMacHash.contains(parser.mac()))
        return;


    if(parser.spO2ValuesSize() > 0)
    {
        const QVector<ReadsParser::SpO2Values> values = parser.spO2Values();

        // Get the latest value.
        time_t time = -1;
        int idx = -1;

        for(int i = 0; i < values.size(); i++)
        {
            if(values.at(i).time > time)
            {
                time = values.at(i).time;
                idx = i;
            }
        }

        if(idx >= 0)
        {
            mMacHash[parser.mac()]->spo2Values.time = values.at(idx).time;
            mMacHash[parser.mac()]->spo2Values.spO2 = values.at(idx).spO2;

            emit dataChanged(index(mData.indexOf(mMacHash[parser.mac()]), SpO2), index(mData.indexOf(mMacHash[parser.mac()]), SpO2));
        }
    }

    if(parser.heartRateValuesSize() > 0)
    {
        const QVector<ReadsParser::HeartRateValues> values = parser.heartRateValues();

        // Get the latest value.
        time_t time = -1;
        int idx = -1;

        for(int i = 0; i < values.size(); i++)
        {
            if(values.at(i).time > time)
            {
                time = values.at(i).time;
                idx = i;
            }
        }

        if(idx >= 0)
        {
            mMacHash[parser.mac()]->heartRateValues.time = values.at(idx).time;
            mMacHash[parser.mac()]->heartRateValues.heartR = values.at(idx).heartR;
            emit dataChanged(index(mData.indexOf(mMacHash[parser.mac()]), HR), index(mData.indexOf(mMacHash[parser.mac()]), HR));
        }
    }

    if(parser.bloodPressureValuesSize() > 0)
    {
        const QVector<ReadsParser::BloodPressureValues> values = parser.bloodPressureValues();

        // Get the latest value.
        time_t time = -1;
        int idx = -1;

        for(int i = 0; i < values.size(); i++)
        {
            if(values.at(i).time > time)
            {
                time = values.at(i).time;
                idx = i;
            }
        }

        if(idx >= 0)
        {
            mMacHash[parser.mac()]->bloodPressureValues.time = values.at(idx).time;
            mMacHash[parser.mac()]->bloodPressureValues.sys = values.at(idx).sys;
            mMacHash[parser.mac()]->bloodPressureValues.dias = values.at(idx).dias;
            emit dataChanged(index(mData.indexOf(mMacHash[parser.mac()]), BPSys), index(mData.indexOf(mMacHash[parser.mac()]), BPDias));
        }
    }

    if(parser.bodyTempValuesSize() > 0)
    {
        const QVector<ReadsParser::BodyTempValues> values = parser.bodyTempValues();

        // Get the latest value.
        time_t time = -1;
        int idx = -1;

        for(int i = 0; i < values.size(); i++)
        {
            if(values.at(i).time > time)
            {
                time = values.at(i).time;
                idx = i;
            }
        }

        if(idx >= 0)
        {
            mMacHash[parser.mac()]->bodyTempValues.time = values.at(idx).time;
            mMacHash[parser.mac()]->bodyTempValues.bodyTemp = values.at(idx).bodyTemp;
            emit dataChanged(index(mData.indexOf(mMacHash[parser.mac()]), BT), index(mData.indexOf(mMacHash[parser.mac()]), BT));
        }
    }
}

void InternmentsModel::updateInternmentsAlarms(const QVector<DatakeeperAlarmsParser::InternmentsAlarms> & alarms)
{
    for(int i = 0; i < mData.size(); i++)
    {
        mData.at(i)->spO2Severity = DatakeeperAlarmsParser::Green;
        mData.at(i)->heartRateSeverity = DatakeeperAlarmsParser::Green;
        mData.at(i)->bodyTempSeverity = DatakeeperAlarmsParser::Green;
        mData.at(i)->bloodPressureSeverity = DatakeeperAlarmsParser::Green;

        for(int j = 0; j < alarms.size(); j++)
        {
            if(mData.at(i)->internmentId == alarms.at(j).internmentId)
            {
                if(alarms.at(j).alarms.contains(DatakeeperAlarmsParser::SpO2) &&
                   alarms.at(j).alarms[DatakeeperAlarmsParser::SpO2] != DatakeeperAlarmsParser::Green)
                    mData.at(i)->spO2Severity = DatakeeperAlarmsParser::Red;

                if(alarms.at(j).alarms.contains(DatakeeperAlarmsParser::HeartRate) &&
                   alarms.at(j).alarms[DatakeeperAlarmsParser::HeartRate] != DatakeeperAlarmsParser::Green)
                    mData.at(i)->heartRateSeverity = DatakeeperAlarmsParser::Red;

                if(alarms.at(j).alarms.contains(DatakeeperAlarmsParser::BodyTemp) &&
                   alarms.at(j).alarms[DatakeeperAlarmsParser::BodyTemp] != DatakeeperAlarmsParser::Green)
                    mData.at(i)->bodyTempSeverity = DatakeeperAlarmsParser::Red;

                if(alarms.at(j).alarms.contains(DatakeeperAlarmsParser::BloodPressure) &&
                   alarms.at(j).alarms[DatakeeperAlarmsParser::BloodPressure] != DatakeeperAlarmsParser::Green)
                    mData.at(i)->bloodPressureSeverity = DatakeeperAlarmsParser::Red;
            }
        }
    }

    checkAlarmSoundsState();

    emit dataChanged(index(0, SpO2), index(mData.size() - 1, BT), QVector<int>() << Qt::BackgroundRole);
}

void InternmentsModel::updateDevicesAlarms(QVector<DatakeeperAlarmsParser::DeviceData> devices)
{
    for(int i = 0; i < mData.size(); i++)
    {
        mData.at(i)->deviceData.devMissingAlarmLevel = DatakeeperAlarmsParser::AlarmSeverity::Red;
        mData.at(i)->deviceData.battLowAlarmLevel = DatakeeperAlarmsParser::AlarmSeverity::Red;

        for(int j = 0; j < devices.size(); j++)
        {
            if(mData.at(i)->device == devices.at(j).mac)
            {
                mData.at(i)->deviceData = devices.at(j);
                devices.removeAt(j);
                break;
            }
        }
    }

    checkAlarmSoundsState();

    emit dataChanged(index(0, DeviceMAC), index(mData.size() - 1, DeviceMAC), QVector<int>() << Qt::BackgroundRole);
}

void InternmentsModel::muteSpO2Alarm(const db_id_t internmentId, const bool mute)
{
    if(!mInternmentIdHash.contains(internmentId))
        return;

    mInternmentIdHash.value(internmentId)->spo2AlarmMuted = mute;

    checkAlarmSoundsState();
}

void InternmentsModel::muteHRAlarm(const db_id_t internmentId, const bool mute)
{
    if(!mInternmentIdHash.contains(internmentId))
        return;

    mInternmentIdHash.value(internmentId)->hrAlarmMuted = mute;

    checkAlarmSoundsState();
}

void InternmentsModel::muteBTAlarm(const db_id_t internmentId, const bool mute)
{
    if(!mInternmentIdHash.contains(internmentId))
        return;

    mInternmentIdHash.value(internmentId)->btAlarmMuted = mute;

    checkAlarmSoundsState();
}

void InternmentsModel::muteDeviceAlarm(const db_id_t internmentId, const bool mute)
{
    if(!mInternmentIdHash.contains(internmentId))
        return;

    mInternmentIdHash.value(internmentId)->deviceAlarmMuted = mute;

    checkAlarmSoundsState();
}

int32_t InternmentsModel::internmentIdFromMac(QString mac) const
{
    if(!mMacHash.contains(mac))
        return -1;

    return mMacHash[mac]->internmentId;
}

void InternmentsModel::clearData()
{
    qDeleteAll(mData);
    mData.clear();
    mMacHash.clear();
    mInternmentIdHash.clear();
}

void InternmentsModel::checkAlarmSoundsState()
{
    DatakeeperAlarmsParser::AlarmSeverity severity = DatakeeperAlarmsParser::AlarmSeverity::Green;

    bool playInternmentAlarm = false;
    bool playDeviceAlarm = false;

    for(int i = 0; i < mData.size(); i++)
    {
        const auto inte = mData.at(i);

        if(((!inte->spo2AlarmMuted) && (inte->spO2Severity != DatakeeperAlarmsParser::AlarmSeverity::Green)) ||
           ((!inte->hrAlarmMuted) && (inte->heartRateSeverity != DatakeeperAlarmsParser::AlarmSeverity::Green)) ||
           ((!inte->btAlarmMuted) && (inte->bodyTempSeverity != DatakeeperAlarmsParser::AlarmSeverity::Green)))
        {
            playInternmentAlarm = true;
            severity = DatakeeperAlarmsParser::AlarmSeverity::Red;
        }

        if((!inte->deviceAlarmMuted) &&
           (DatakeeperAlarmsParser::mostSeverityAlarm(inte->deviceData) > DatakeeperAlarmsParser::Green))
        {
            playDeviceAlarm = true;
        }

        if(playInternmentAlarm && playDeviceAlarm)
            break;
    }

    emit playSoundAlarms(playDeviceAlarm, playInternmentAlarm);
    emit currentSeverity(severity);
}

QVariant InternmentsModel::severityToColor(const DatakeeperAlarmsParser::AlarmSeverity severity)
{
    switch(severity) {
    case DatakeeperAlarmsParser::Green:
        return QVariant();

    case DatakeeperAlarmsParser::Yellow:
        return QColor("#ffff00");

    case DatakeeperAlarmsParser::Orange:
        return QColor("#ffaa00");

    case DatakeeperAlarmsParser::Red:
        return QColor("#ff0000");
    }

    return QVariant();
}
