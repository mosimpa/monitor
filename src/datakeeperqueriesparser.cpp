/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QStringList>
#include <QByteArray>

#include <mosquittoclient.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/scaling.h>

#include "internmentsmodel.h"
#include "datakeeperqueriesparser.h"

/**
 * @brief DatakeeperQueriesParser::parseMessage
 * @param msg message to be parsed.
 * @returns nullptr if the message can't be parsed, a pointer to a class
 * derived from QAbstraictItemModel otherwise.
 */
DatakeeperQueriesParser::Replies DatakeeperQueriesParser::typeOfReply(const mosquitto_message *msg)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return NotRecognizableReply;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(mainObject.contains("internments") && mainObject["internments"].isArray())
        return InternmentsReply;

    if(mainObject.contains("spo2") && mainObject["spo2"].isArray())
        return SpO2Reply;

    if(mainObject.contains("heartR") && mainObject["heartR"].isArray())
        return HeartRateReply;

    if(mainObject.contains("bloodP") && mainObject["bloodP"].isArray())
        return BloodPressureReply;

    if(mainObject.contains("bodyT") && mainObject["bodyT"].isArray())
        return BodyTemperatureReply;

    if(mainObject.contains("alarms_thresholds") && mainObject["alarms_thresholds"].isObject())
        return AlarmsThresholdsReply;

    if(mainObject.contains("command") && mainObject["command"].isString())
    {
        QString command = mainObject["command"].toString();

        if(mainObject.contains("result") && mainObject["result"].isString())
            return CommandResultReply;

        if(command == QStringLiteral("alarms_ranges"))
            return AlarmsRangesReply;

        if(command == QStringLiteral("gen_report"))
            return GenerateReportReply;
    }

    return NotRecognizableReply;
}

/**
 * @brief DatakeeperQueriesParser::parseSpO2SensorData
 * @param msg The message from which data will be parsed.
 * @param data A vector to which parsed data will be appended.
 * @return -1 if anything fails, a value > 0 represents the associated internmentId.
 */
db_id_t DatakeeperQueriesParser::parseSpO2SensorData(const mosquitto_message *msg, QVector<SpO2> &data)
{
    db_id_t internmentId = -1;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return -1;
    }

    QJsonObject mainObject = jsonDoc.object();

    // Internment ID.
    internmentId = checkInternmentId(mainObject);
    if(internmentId < 0)
        return -1;

    // SpO2.
    if(!mainObject.contains("spo2") || !mainObject["spo2"].isArray())
        return -1;

    QJsonArray spO2Array = mainObject["spo2"].toArray();

    for(int i = 0; i < spO2Array.size(); i++)
    {
        struct SpO2 spo2;
        int64_t tmp;

        if(!spO2Array.at(i).isObject())
            continue;

        QJsonObject spO2Object = spO2Array.at(i).toObject();

        // Time.
        spo2.time = checkTime(spO2Object);
        if(spo2.time < 0)
            continue;

        // SpO2.
        if(!spO2Object.contains("SpO2") || !(spO2Object["SpO2"].isDouble()))
            continue;

        tmp = static_cast<int64_t>(spO2Object["SpO2"].toDouble(WIRE_SPO2_MIN - 1));
        if((tmp < WIRE_SPO2_MIN) || (tmp > WIRE_SPO2_MAX))
            continue;

        spo2.spo2 = static_cast<spo2_t>(tmp);

        data.append(spo2);
    }

    return internmentId;
}

/**
 * @brief DatakeeperQueriesParser::parseHeartRateData
 * @param msg The message from which data will be parsed.
 * @param data A vector to which parsed data will be appended.
 * @return -1 if anything fails, a value > 0 represents the associated internmentId.
 */
db_id_t DatakeeperQueriesParser::parseHeartRateData(const mosquitto_message *msg, QVector<DatakeeperQueriesParser::HeartRate> &data)
{
    db_id_t internmentId = -1;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return -1;
    }

    QJsonObject mainObject = jsonDoc.object();

    // Internment ID.
    internmentId = checkInternmentId(mainObject);
    if(internmentId < 0)
        return -1;

    if(!mainObject.contains("heartR") || !mainObject["heartR"].isArray())
        return -1;

    QJsonArray heartRateArray = mainObject["heartR"].toArray();

    for(int i = 0; i < heartRateArray.size(); i++)
    {
        struct HeartRate heartR;
        int64_t tmp;

        if(!heartRateArray.at(i).isObject())
            continue;

        QJsonObject heartRateObject = heartRateArray.at(i).toObject();

        // Time.
        heartR.time = checkTime(heartRateObject);
        if(heartR.time < 0)
            continue;

        // Heart rate.
        if(!heartRateObject.contains("heartR") || !(heartRateObject["heartR"].isDouble()))
            continue;

        tmp = static_cast<int64_t>(heartRateObject["heartR"].toDouble(WIRE_HEART_RATE_MIN - 1));
        if((tmp < WIRE_HEART_RATE_MIN) || (tmp > WIRE_HEART_RATE_MAX))
            continue;

        heartR.heartRate = static_cast<heart_rate_t>(tmp);

        data.append(heartR);
    }

    return internmentId;
}

/**
 * @brief DatakeeperQueriesParser::parseBloodPressureData
 * @param msg The message from which data will be parsed.
 * @param data A vector to which parsed data will be appended.
 * @return -1 if anything fails, a value > 0 represents the associated internmentId.
 */
db_id_t DatakeeperQueriesParser::parseBloodPressureData(const mosquitto_message *msg, QVector<BloodPressure> &data)
{
    db_id_t internmentId = -1;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return -1;
    }

    QJsonObject mainObject = jsonDoc.object();

    // Internment ID.
    internmentId = checkInternmentId(mainObject);
    if(internmentId < 0)
        return -1;

    if(!mainObject.contains("bloodP") || !mainObject["bloodP"].isArray())
        return -1;

    QJsonArray bloodPressureArray = mainObject["bloodP"].toArray();

    for(int i = 0; i < bloodPressureArray.size(); i++)
    {
        struct BloodPressure bloodP;
        int64_t tmp;

        if(!bloodPressureArray.at(i).isObject())
            continue;

        QJsonObject bloodPObject = bloodPressureArray.at(i).toObject();

        // Time.
        bloodP.time = checkTime(bloodPObject);
        if(bloodP.time < 0)
            continue;

        // Systolic pressure.
        if(!bloodPObject.contains("sys") || !(bloodPObject["sys"].isDouble()))
            continue;

        tmp = static_cast<int64_t>(bloodPObject["sys"].toDouble(WIRE_BLOOD_PRESSURE_SYS_MIN - 1));
        if((tmp < WIRE_BLOOD_PRESSURE_SYS_MIN) || (tmp > WIRE_BLOOD_PRESSURE_SYS_MAX))
            continue;

        bloodP.systolic = static_cast<blood_pressure_t>(tmp);

        // Diastolic pressure.
        if(!bloodPObject.contains("dia") || !(bloodPObject["dia"].isDouble()))
            continue;

        tmp = static_cast<int64_t>(bloodPObject["dia"].toDouble(WIRE_BLOOD_PRESSURE_DIAS_MIN - 1));
        if((tmp < WIRE_BLOOD_PRESSURE_DIAS_MIN) || (tmp > WIRE_BLOOD_PRESSURE_DIAS_MAX))
            continue;

        bloodP.diastolic = static_cast<blood_pressure_t>(tmp);

        data.append(bloodP);
    }

    return internmentId;
}

/**
 * @brief DatakeeperQueriesParser::parseBodyTemperatureData
 * @param msg The message from which data will be parsed.
 * @param data A vector to which parsed data will be appended.
 * @return -1 if anything fails, a value > 0 represents the associated internmentId.
 */
db_id_t DatakeeperQueriesParser::parseBodyTemperatureData(const mosquitto_message *msg, QVector<DatakeeperQueriesParser::BodyTemperature> &data)
{
    db_id_t internmentId = -1;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return -1;
    }

    QJsonObject mainObject = jsonDoc.object();

    // Internment ID.
    internmentId = checkInternmentId(mainObject);
    if(internmentId < 0)
        return -1;

    if(!mainObject.contains("bodyT") || !mainObject["bodyT"].isArray())
        return -1;

    QJsonArray bodyTArray = mainObject["bodyT"].toArray();

    for(int i = 0; i < bodyTArray.size(); i++)
    {
        struct BodyTemperature bodyT;
        int64_t tmp;

        if(!bodyTArray.at(i).isObject())
            continue;

        QJsonObject bodyTObject = bodyTArray.at(i).toObject();

        // Time.
        bodyT.time = checkTime(bodyTObject);
        if(bodyT.time < 0)
            continue;

        // Temperature.
        if(!bodyTObject.contains("temp") || !(bodyTObject["temp"].isDouble()))
            continue;

        tmp = static_cast<int64_t>(bodyTObject["temp"].toDouble(WIRE_BODY_TEMP_MIN - 1));
        if((tmp < WIRE_BODY_TEMP_MIN) || (tmp > WIRE_BODY_TEMP_MAX))
            continue;

        bodyT.temp = static_cast<body_temp_t>(tmp);

        data.append(bodyT);
    }

    return internmentId;
}

void DatakeeperQueriesParser::parseCommandResult(const mosquitto_message *msg, DatakeeperQueriesParser::CommandResult &result)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return;
    }

    QJsonObject mainObject = jsonDoc.object();
    result.id.clear();
    result.internmentId = -1;
    result.command.clear();
    result.result.clear();

    if(mainObject.contains("id") && mainObject["id"].isString())
        result.id = mainObject["id"].toString("");

    if(mainObject.contains("internment_id") && mainObject["internment_id"].isDouble())
        result.internmentId = mainObject["internment_id"].toInt();

    if(mainObject.contains("command") && mainObject["command"].isString())
        result.command = mainObject["command"].toString("");

    if(mainObject.contains("result") && mainObject["result"].isString())
        result.result = mainObject["result"].toString("");
}

bool DatakeeperQueriesParser::parseAlarmsThresholds(const mosquitto_message *msg, DatakeeperQueriesParser::AlarmsThresholds & thresholds)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();

    // Message ID.
    if(!mainObject.contains("id") || !mainObject["id"].isString())
        return false;

    thresholds.id = mainObject["id"].toString();

    // Internment ID.
    thresholds.internmentId = checkInternmentId(mainObject);
    if(thresholds.internmentId < 0)
        return false;

    if(!mainObject.contains("alarms_thresholds") || !mainObject["alarms_thresholds"].isObject())
        return false;

    QJsonObject alarmThObj = mainObject["alarms_thresholds"].toObject();

    if(!alarmThObj.contains("last_update") || !alarmThObj["last_update"].isString())
        return false;

    thresholds.lastUpdate = alarmThObj["last_update"].toString();

    auto dt = QDateTime::fromString(thresholds.lastUpdate, Qt::ISODate);
    if(!dt.isValid())
        return false;

    if(!alarmThObj.contains("spo2_lt") || !alarmThObj["spo2_lt"].isDouble())
        return false;

    thresholds.spO2Lt = static_cast<spo2_t>(alarmThObj["spo2_lt"].toInt());

    if(!alarmThObj.contains("spo2_delay_s") || !alarmThObj["spo2_delay_s"].isDouble())
        return false;

    thresholds.spO2DelayS = static_cast<uint8_t>(alarmThObj["spo2_delay_s"].toInt());

    if(!alarmThObj.contains("hr_lt") || !alarmThObj["hr_lt"].isDouble())
        return false;

    thresholds.hrLt = static_cast<heart_rate_t>(alarmThObj["hr_lt"].toInt());

    if(!alarmThObj.contains("hr_gt") || !alarmThObj["hr_gt"].isDouble())
        return false;

    thresholds.hrGt = static_cast<heart_rate_t>(alarmThObj["hr_gt"].toInt());

    if(!alarmThObj.contains("hr_delay_s") || !alarmThObj["hr_delay_s"].isDouble())
        return false;

    thresholds.hRDelayS = static_cast<uint8_t>(alarmThObj["hr_delay_s"].toInt());

    if(!alarmThObj.contains("bt_lt") || !alarmThObj["bt_lt"].isDouble())
        return false;

    thresholds.bTLt = static_cast<body_temp_t>(alarmThObj["bt_lt"].toInt());

    if(!alarmThObj.contains("bt_gt") || !alarmThObj["bt_gt"].isDouble())
        return false;

    thresholds.bTGt = static_cast<body_temp_t>(alarmThObj["bt_gt"].toInt());

    if(!alarmThObj.contains("bt_delay_s") || !alarmThObj["bt_delay_s"].isDouble())
        return false;

    thresholds.bTDelayS = static_cast<uint8_t>(alarmThObj["bt_delay_s"].toInt());

    if(!alarmThObj.contains("bp_sys_lt") || !alarmThObj["bp_sys_lt"].isDouble())
        return false;

    thresholds.bPSysLt = static_cast<blood_pressure_t>(alarmThObj["bp_sys_lt"].toInt());

    if(!alarmThObj.contains("bp_sys_gt") || !alarmThObj["bp_sys_gt"].isDouble())
        return false;

    thresholds.bPSysGt = static_cast<blood_pressure_t>(alarmThObj["bp_sys_gt"].toInt());

    if(!alarmThObj.contains("bp_delay_s") || !alarmThObj["bp_delay_s"].isDouble())
        return false;

    thresholds.bPDelayS = static_cast<uint8_t>(alarmThObj["bp_delay_s"].toInt());

    return true;
}

/**
 * @brief DatakeeperQueriesParser::parseAlarmsRanges
 * @param msg
 * @param ranges
 *
 * ranges.id will be empty if the message can't be parsed.
 */
bool DatakeeperQueriesParser::parseAlarmsRanges(const mosquitto_message *msg, DatakeeperQueriesParser::AlarmsRanges &ranges)
{
    bool ok = true;
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();
    ranges.id.clear();

    ok &= checkForString(mainObject, "spo2_eq_or_less_perc", ranges.spO2EqOrLessPercBot, ranges.spO2EqOrLessPercTop);
    ok &= checkForString(mainObject, "spo2_delay_s", ranges.spO2DelaySMin, ranges.spO2DelaySMax);

    ok &= checkForString(mainObject, "heart_rate_eq_or_less_bpm", ranges.heartRateEqOrLessBPMBot, ranges.heartRateEqOrLessBPMTop);
    ok &= checkForString(mainObject, "heart_rate_eq_or_more_bpm", ranges.heartRateEqOrMoreBPMBot, ranges.heartRateEqOrMoreBPMTop);
    ok &= checkForString(mainObject, "heart_rate_delay_s", ranges.heartRateDelaySMin, ranges.heartRateDelaySMax);

    ok &= checkForString(mainObject, "body_temperature_eq_or_less_cels", ranges.bodyTempEqOrLessCelsBot, ranges.bodyTempEqOrLessCelsTop);
    ok &= checkForString(mainObject, "body_temperature_eq_or_more_cels", ranges.bodyTempEqOrMoreCelsBot, ranges.bodyTempEqOrMoreCelsTop);
    ok &= checkForString(mainObject, "body_temperature_delay_s", ranges.bodyTempDelaySMin, ranges.bodyTempDelaySMax);

    ok &= checkForString(mainObject, "blood_pressure_sys_eq_or_less", ranges.bloodPressureSysEqOrLessBot, ranges.bloodPressureSysEqOrLessTop);
    ok &= checkForString(mainObject, "blood_pressure_sys_eq_or_more", ranges.bloodPressureSysEqOrMoreBot, ranges.bloodPressureSysEqOrMoreTop);
    ok &= checkForString(mainObject, "blood_pressure_delay_s", ranges.bloodPressureDelaySMin, ranges.bloodPressureDelaySMax);

    if(!ok)
        return false;

    // Finally parse id. This should be already present.
    if(mainObject.contains("id") && mainObject["id"].isString())
        ranges.id = mainObject["id"].toString("");

    return true;
}

bool DatakeeperQueriesParser::parseGenerateReport(const struct mosquitto_message * msg, Report &report)
{
    QByteArray payload(static_cast<char*>(msg->payload), msg->payloadlen);
    QJsonDocument jsonDoc = QJsonDocument::fromJson(payload);

    if(!jsonDoc.isObject())
    {
        qDebug() << "No JSON object received, discarding message with payload " << QString(payload);
        return false;
    }

    QJsonObject mainObject = jsonDoc.object();

    if(!mainObject.contains("id") && !mainObject["id"].isString())
        return false;

    report.id = mainObject["id"].toString();

    if(mainObject.contains("result") && mainObject["result"].isString())
    {
        /*
         * The sole fact that result is present means that an error occurred,
         * return ERROR in url.
         */
        report.url = "ERROR";
        return true;
    }

    if(!mainObject.contains("url") || !mainObject["url"].isString())
        return false;

    report.url = mainObject["url"].toString();

    return true;
}

bool DatakeeperQueriesParser::checkForString(const QJsonObject &object, const QString &string, uint8_t &bottom, uint8_t &top)
{
    bool ok;

    if(!object.contains(string) || !object[string].isString())
        return false;

    auto list = object[string].toString().split(",");
    if(list.size() != 2)
        return false;

    bottom = static_cast<uint8_t>(list.at(0).toInt(&ok));
    if(!ok)
        return false;

    top = static_cast<uint8_t>(list.at(1).toInt(&ok));
    if(!ok)
        return false;

    return true;
}

bool DatakeeperQueriesParser::checkForString(const QJsonObject &object, const QString &string, int32_t &bottom, int32_t &top)
{
    bool ok;

    if(!object.contains(string) || !object[string].isString())
        return false;

    auto list = object[string].toString().split(",");
    if(list.size() != 2)
        return false;

    bottom = list.at(0).toInt(&ok);
    if(!ok)
        return false;

    top = list.at(1).toInt(&ok);
    if(!ok)
        return false;

    return true;
}

db_id_t DatakeeperQueriesParser::checkInternmentId(const QJsonObject &object)
{
    if(!object.contains("internment_id") || !object["internment_id"].isDouble())
        return -1;

    int64_t tmpInt64 = static_cast<int64_t>(object["internment_id"].toDouble(DB_ID_MIN - 1));

    if((tmpInt64 < DB_ID_MIN) || (tmpInt64 > DB_ID_MAX))
        return -1;

    return static_cast<db_id_t>(tmpInt64);
}

time_t DatakeeperQueriesParser::checkTime(const QJsonObject &object)
{
    if(!object.contains("time") || !(object["time"].isDouble()))
        return -1;

    int64_t tmpInt64 = static_cast<int64_t>(object["time"].toDouble(-1.0));
    if(tmpInt64 < 0)
        return -1;

    return static_cast<time_t>(tmpInt64);
}
