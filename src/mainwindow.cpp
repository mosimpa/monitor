/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <QMainWindow>
#include <QStackedWidget>
#include <QApplication>
#include <QTimer>
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QScrollArea>
#include <QStatusBar>
#include <QTableView>
#include <QSortFilterProxyModel>
#include <QIcon>
#include <QAction>
#include <QToolBar>
#include <QHash>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QTimer>
#include <QDateTime>
#include <QPushButton>
#include <QHeaderView>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QProcess>

#include <readsparser.h>

#include "dataengine.h"
#include "devicedatawidget.h"
#include "mutebutton.h"
#include "mainwindow.h"
#include "internmentsmodel.h"
#include "alarmsdialog.h"
#include "alarmssortfilterproxymodel.h"
#include "mqttbrokersetupdialog.h"
#include "internmentsdelegate.h"
#include "requestreportdialog.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), mAlarmsDialog(nullptr)
{
    mRefreshInternmentsTimer = new QTimer(this);
    connect(mRefreshInternmentsTimer, &QTimer::timeout, this, &MainWindow::toggleRefreshIntermentsButtonImage);
    mRefreshImageIsRed = false;

    mMainToolBar = new QToolBar(tr("Main toolbar"), this);

    mDataEngine = new DataEngine(this);
    connect(mDataEngine, &DataEngine::subscribedToMonitor, this, &MainWindow::refreshInternments);
    connect(mDataEngine, &DataEngine::internmentsUpdated, this, &MainWindow::onIntermentsUpdated);
    connect(mDataEngine, &DataEngine::spO2HistoricDataAvailable, this, &MainWindow::onSpO2HistoricDataAvailable);
    connect(mDataEngine, &DataEngine::heartRateHistoricDataAvailable, this, &MainWindow::onHeartRateHistoricDataAvailable);
    connect(mDataEngine, &DataEngine::bloodPressureHistoricDataAvailable, this, &MainWindow::onBloodPressureHistoricDataAvailable);
    connect(mDataEngine, &DataEngine::bodyTemperatureHistoricDataAvailable, this, &MainWindow::onBodyTemperatureHistoricDataAvailable);
    connect(mDataEngine, &DataEngine::readsDataReceived, this, &MainWindow::onReadsDataReceived);
    connect(mDataEngine, &DataEngine::commandResultReceived, this, &MainWindow::handleCommandResult);
    connect(mDataEngine, &DataEngine::alarmsRangesReceived, this, &MainWindow::alarmsRangesReceived);
    connect(mDataEngine, &DataEngine::internmentsChanged, this, &MainWindow::onInternmentsChanged);
    connect(mDataEngine, &DataEngine::reportReceived, this, &MainWindow::downloadReport);
    connect(mDataEngine->internments(), &InternmentsModel::currentSeverity, this, &MainWindow::updateAlarmSeverity);
    connect(mDataEngine, &DataEngine::datakeeperStatusChanged, this, &MainWindow::handleDatakeeperStatusChange);
    mDataEngine->connect();

    mSortFilterProxyModel = new AlarmsSortFilterProxyModel(this);

    mTableView = new QTableView(this);
    mTableView->setItemDelegate(new InternmentsDelegate());
    setCentralWidget(mTableView);

    setMinimumSize(1024, 512);
    setWindowIcon(QIcon(":images/mosimpa-monitor.png"));

    auto statusBar = new QStatusBar();

    setStatusBar(statusBar);
    statusBar->showMessage(tr("Connecting to server..."));

    createMenuAndToolBar();

    // Be sure to set ranges to 0.
    mAlarmsRanges = {};


    auto trayIconMenu = new QMenu(this);

    auto minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    auto maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);

    auto restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addAction(mQuitAction);

    mSysIcon = new QSystemTrayIcon(QIcon(":images/mosimpa-monitor_green.png"), this);
    mSysIcon->setContextMenu(trayIconMenu);
    connect(mSysIcon, &QSystemTrayIcon::activated, this, &MainWindow::systemTrayIconActivated);

    if(QSystemTrayIcon::isSystemTrayAvailable())
        mSysIcon->show();

    // Assume datakeeper is offline.
    handleDatakeeperStatusChange(false);
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    if(mSysIcon->isVisible())
    {
        /*
         * The application is showing the systary icon, avoid closing it trough
         * it's window, ie., minimize to systray.
         */
        event->ignore();
        hide();
        mSysIcon->showMessage(tr("MoSimPa monitor"),
                              tr("Application was minimized to system tray."),
                              QSystemTrayIcon::Information, 2000);

        return;
    }

    // The systray icon is not available, so confirm close here.
    confirmExit();
}

void MainWindow::onIntermentsUpdated()
{
    /*
     * The sort proxy model helps us by proxying the model and letting it be
     * sortable without the need to re arrange the original model.
     */
    mSortFilterProxyModel->setSourceModel(mDataEngine->internments());
    mTableView->setModel(mSortFilterProxyModel);
    addRowButtons();
    mTableView->setSortingEnabled(true);
    mTableView->setShowGrid(true);
    mTableView->resizeColumnsToContents();

    // Allow to select only one single full row at a time.
    mTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    mTableView->setSelectionMode(QAbstractItemView::SingleSelection);

    auto * selectionModel = mTableView->selectionModel();
    connect(selectionModel, &QItemSelectionModel::selectionChanged, this, &MainWindow::setOpenDeviceViewButtonEnabled);

    statusBar()->showMessage(tr("Internments received."), 3000);

    mOpenInternmentViewAction->setEnabled(false);
    mOpenAlarmsSetupAction->setEnabled(false);
    mRequestReportAction->setEnabled(false);

    mTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    mTableView->horizontalHeader()->setSectionResizeMode(InternmentsModel::Columns::InternmentID, QHeaderView::ResizeToContents);
    mTableView->horizontalHeader()->setSectionResizeMode(InternmentsModel::Columns::Age, QHeaderView::ResizeToContents);
    mTableView->horizontalHeader()->setSectionResizeMode(InternmentsModel::Columns::Gender, QHeaderView::ResizeToContents);
}

void MainWindow::openInternmentView()
{
    auto * selectionModel = mTableView->selectionModel();
    QItemSelection selection = mSortFilterProxyModel->mapSelectionToSource(selectionModel->selection());
    QModelIndexList list = selection.indexes();

    if(list.isEmpty())
        return;

    auto devData = mDataEngine->internments()->device(list.at(0).row());

    if(mDevDataWidgets.contains(devData.internmentId))
    {
        mDevDataWidgets[devData.internmentId]->showNormal();
        return;
    }

    auto devDataW = new DeviceDataWidget(mDataEngine, devData, this);
    requestInternmentHistory(devData.internmentId,
                             QDateTime::currentDateTime().toSecsSinceEpoch() - devDataW->historyMinutes() * 60,
                             0);
    devDataW->setModal(false);
    connect(devDataW, &DeviceDataWidget::closed, this, &MainWindow::deleteDeviceDataWidget);
    connect(devDataW, &DeviceDataWidget::requestHistory, this, &MainWindow::requestInternmentHistory);
    mDevDataWidgets.insert(devData.internmentId, devDataW);
    devDataW->show();
}

void MainWindow::openAlarmsSetup()
{
    auto * selectionModel = mTableView->selectionModel();
    QItemSelection selection = mSortFilterProxyModel->mapSelectionToSource(selectionModel->selection());
    QModelIndexList list = selection.indexes();

    if(list.isEmpty())
        return;

    auto devData = mDataEngine->internments()->device(list.at(0).row());

    mAlarmsDialog = new AlarmsDialog(mDataEngine, devData, mAlarmsRanges, this);
    connect(mAlarmsDialog, &AlarmsDialog::dialogClosed, this, &MainWindow::closeAlarmsDialog);
    mAlarmsDialog->setModal(true);
    mAlarmsDialog->show();
}

void MainWindow::setOpenDeviceViewButtonEnabled(const QItemSelection &selected, const QItemSelection &/*deselected*/)
{
    mOpenInternmentViewAction->setEnabled(!selected.isEmpty());
    mOpenAlarmsSetupAction->setEnabled(!selected.isEmpty());
    mRequestReportAction->setEnabled(!selected.isEmpty());
}

void MainWindow::refreshInternments()
{
    mRefreshInternmentsTimer->stop();
    mRefreshImageIsRed = false;
    mRefreshAction->setIcon(QIcon(":/images/refresh.png"));

    mDataEngine->requestInternments();
    this->statusBar()->showMessage(tr("Requesting internments..."));

    mDataEngine->requestAlarmsRanges();
}

void MainWindow::deleteDeviceDataWidget(const int32_t internmentId)
{
    if(!mDevDataWidgets.contains(internmentId))
        return;

    auto dev = mDevDataWidgets.take(internmentId);
    dev->deleteLater();
}

void MainWindow::onSpO2HistoricDataAvailable(const int32_t internmentId)
{
    // Always try to get the data.
    QVector<struct DatakeeperQueriesParser::SpO2> data;
    data = mDataEngine->spO2HistoricData(internmentId);

    /*
     * Check if the internemntId is present. If it's not at least we would have
     * emptied the engine store.
     */
    if(!mDevDataWidgets.contains(internmentId))
        return;

    mDevDataWidgets[internmentId]->addOxigenSaturation(data);
}

void MainWindow::onHeartRateHistoricDataAvailable(const int32_t internmentId)
{
    // Always try to get the data.
    QVector<struct DatakeeperQueriesParser::HeartRate> data;
    data = mDataEngine->heartRateHistoricData(internmentId);

    /*
     * Check if the internemntId is present. If it's not at least we would have
     * emptied the engine store.
     */
    if(!mDevDataWidgets.contains(internmentId))
        return;

    mDevDataWidgets[internmentId]->addHeartRate(data);
}

void MainWindow::onBloodPressureHistoricDataAvailable(const int32_t internmentId)
{
    // Always try to get the data.
    QVector<struct DatakeeperQueriesParser::BloodPressure> data;
    data = mDataEngine->bloodPressureHistoricData(internmentId);

    Q_UNUSED(data)

    // We are currently not showing this data.
}

void MainWindow::onBodyTemperatureHistoricDataAvailable(const int32_t internmentId)
{
    // Always try to get the data.
    QVector<struct DatakeeperQueriesParser::BodyTemperature> data;
    data = mDataEngine->bodyTemperatureHistoricData(internmentId);

    /*
     * Check if the internemntId is present. If it's not at least we would have
     * emptied the engine store.
     */
    if(!mDevDataWidgets.contains(internmentId))
        return;

    mDevDataWidgets[internmentId]->addBodyTemperature(data);
}

void MainWindow::onReadsDataReceived(const ReadsParser &parser)
{
    int32_t internmentsId = mDataEngine->internments()->internmentIdFromMac(parser.mac());

    if(!mDevDataWidgets.contains(internmentsId))
        return;

    mDevDataWidgets[internmentsId]->addData(parser);
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About MoSimPa Monitor %1").arg(MONITOR_VERSION_STRING),
                tr("<p>This application is part of ParaAyudar's MoSimPa, an effort to create devices to fight COVID-19.</p>"
                   "<p>See <a href=\"https://mosimpa.gitlab.io\">the technical documentation</a> for more information.</p>"
                   "<p><a href=\"https://www.instagram.com/paraayudardar/\">ParaAyudar on instagram</a>.</p>"));
}

void MainWindow::closeAlarmsDialog()
{
    mAlarmsDialog->close();
    delete mAlarmsDialog;
    mAlarmsDialog = nullptr;
}

void MainWindow::handleCommandResult(const DatakeeperQueriesParser::CommandResult & result)
{
    /// \todo Consider warning about a possile internments update. Or wait for the datakeeper signal when implemented.
    if(result.result == QStringLiteral("OK"))
        return;

    QString text = QString(tr("The command %1 issued for internment %2 has returned with result %3."))
                          .arg(result.command).arg(result.internmentId).arg(result.result);
    QMessageBox::warning(this, tr("Command failed"), text);
}

void MainWindow::onInternmentsChanged()
{
    mRefreshInternmentsTimer->start(500);
}

void MainWindow::toggleRefreshIntermentsButtonImage()
{
    mRefreshImageIsRed = (mRefreshImageIsRed == true) ? false : true;
    QIcon icon;

    if(mRefreshImageIsRed)
        icon = QIcon(":/images/refresh_red.png");
    else
        icon = QIcon(":/images/refresh.png");

    mRefreshAction->setIcon(icon);
}

void MainWindow::alarmsRangesReceived(const DatakeeperQueriesParser::AlarmsRanges & ranges)
{
    mAlarmsRanges = ranges;
}

void MainWindow::requestInternmentHistory(db_id_t internmentId, time_t fromTime, int32_t numOfSamples)
{
    mDataEngine->requestInternmentHistory(internmentId, fromTime, numOfSamples);
}

void MainWindow::configureMqtt()
{
    auto dialog = new MqttBrokerSetupDialog(this);
    dialog->exec();
}

void MainWindow::showInternmentsWithAlarm(bool checked)
{
    mSortFilterProxyModel->showOnlyInternmentsWithAlarms(checked);
    addRowButtons();
}

void MainWindow::addRowButtons()
{
    for(int i = 0; i < mSortFilterProxyModel->rowCount(); i++)
    {
        auto muteDeviceButton = new MuteButton(mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::InternmentID), Qt::DisplayRole).toInt(),
                                               mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::MuteDeviceButton), Qt::DisplayRole).toBool());
        connect(muteDeviceButton, &QPushButton::clicked, this, &MainWindow::muteDevice);
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::MuteDeviceButton), muteDeviceButton);

        auto spO2MuteButton = new MuteButton(mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::InternmentID), Qt::DisplayRole).toInt(),
                                             mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::SpO2MuteButton), Qt::DisplayRole).toBool());
        connect(spO2MuteButton, &QPushButton::clicked, this, &MainWindow::muteSpO2);
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::SpO2MuteButton), spO2MuteButton);

        auto hrMuteButton = new MuteButton(mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::InternmentID), Qt::DisplayRole).toInt(),
                                           mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::HRMuteButton), Qt::DisplayRole).toBool());
        connect(hrMuteButton, &QPushButton::clicked, this, &MainWindow::muteHR);
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::HRMuteButton), hrMuteButton);

        auto btMuteButton = new MuteButton(mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::InternmentID), Qt::DisplayRole).toInt(),
                                           mSortFilterProxyModel->data(mSortFilterProxyModel->index(i, InternmentsModel::Columns::BTMuteButton), Qt::DisplayRole).toBool());
        connect(btMuteButton, &QPushButton::clicked, this, &MainWindow::muteBT);
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::BTMuteButton), btMuteButton);


        auto plotButton = new QPushButton();
        plotButton->setIcon(QIcon(":/images/fullscreen.png"));
        connect(plotButton, &QPushButton::clicked, [this] { openInternmentView(); });
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::PlotButton), plotButton);

        auto asButton = new QPushButton();
        asButton->setIcon(QIcon(":/images/alarms_setup.png"));
        connect(asButton, &QPushButton::clicked, [this] { openAlarmsSetup(); });
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::AlarmsSetupButton), asButton);

        auto repButton = new QPushButton();
        repButton->setIcon(QIcon(":/images/report.png"));
        connect(repButton, &QPushButton::clicked, [this] { openRequestReportDialog(); });
        mTableView->setIndexWidget(mSortFilterProxyModel->index(i, InternmentsModel::Columns::RequestReportButton), repButton);
    }
}

void MainWindow::openRequestReportDialog()
{
    auto * selectionModel = mTableView->selectionModel();
    QItemSelection selection = mSortFilterProxyModel->mapSelectionToSource(selectionModel->selection());
    QModelIndexList list = selection.indexes();

    if(list.isEmpty())
        return;

    auto devData = mDataEngine->internments()->device(list.at(0).row());

    mRequestReportDialog = new RequestReportDialog(devData, this);
    connect(mRequestReportDialog, &RequestReportDialog::reportRequested, this, &MainWindow::requestReport);
    mRequestReportDialog->setModal(true);
    mRequestReportDialog->show();
}

void MainWindow::requestReport(db_id_t internmentId, time_t fromTime, time_t toTime)
{
    mRequestReportDialog->close();
    delete mRequestReportDialog;
    mRequestReportDialog = nullptr;

    mDataEngine->requestReport(internmentId, fromTime, toTime);
}

void MainWindow::downloadReport(DatakeeperQueriesParser::Report report)
{
    if(report.url == QUrl("ERROR"))
    {
        QMessageBox::information(this, tr("Report generation failed"),
                                 tr("The report generation failed."));
        return;
    }

    QProcess::startDetached(QStringLiteral("okular"), QStringList() << report.url.url());
    statusBar()->showMessage(tr("Report created, available at %1").arg(report.url.url()), 10000);
}

void MainWindow::muteSpO2()
{
    MuteButton * button = qobject_cast<MuteButton*>(sender());

    if(button == nullptr)
        return;

    mDataEngine->internments()->muteSpO2Alarm(button->internmentId(), button->isChecked());
}

void MainWindow::muteHR()
{
    MuteButton * button = qobject_cast<MuteButton*>(sender());

    if(button == nullptr)
        return;

    mDataEngine->internments()->muteHRAlarm(button->internmentId(), button->isChecked());
}

void MainWindow::muteBT()
{
    MuteButton * button = qobject_cast<MuteButton*>(sender());

    if(button == nullptr)
        return;

    mDataEngine->internments()->muteBTAlarm(button->internmentId(), button->isChecked());
}

void MainWindow::muteDevice()
{
    MuteButton * button = qobject_cast<MuteButton*>(sender());

    if(button == nullptr)
        return;

    mDataEngine->internments()->muteDeviceAlarm(button->internmentId(), button->isChecked());
}

void MainWindow::updateAlarmSeverity(const DatakeeperAlarmsParser::AlarmSeverity severity)
{
    switch(severity) {
    case DatakeeperAlarmsParser::AlarmSeverity::Green:
        mSysIcon->setIcon(QIcon(":images/mosimpa-monitor_green.png"));
        break;

    case DatakeeperAlarmsParser::AlarmSeverity::Yellow:
        mSysIcon->setIcon(QIcon(":images/mosimpa-monitor_yellow.png"));
        break;

    case DatakeeperAlarmsParser::AlarmSeverity::Orange:
        mSysIcon->setIcon(QIcon(":images/mosimpa-monitor_orange.png"));
        break;

    case DatakeeperAlarmsParser::AlarmSeverity::Red:
        mSysIcon->setIcon(QIcon(":images/mosimpa-monitor.png"));
        break;
    }
}

void MainWindow::confirmExit()
{
    auto button = QMessageBox::warning(this, tr("MoSimPa monitor"),
                                       tr("If you close monitor you will not be "
                                          "able to see or hear alarms. "
                                          "Are you sure you want to close the "
                                          "application?"),
                                       QMessageBox::Yes|QMessageBox::No,
                                       QMessageBox::No);

    if(button == QMessageBox::Yes)
        qApp->quit();
}

void MainWindow::systemTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch(reason)
    {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        if(!isVisible())
            showNormal();
        break;

    default:
        break;
    }
}

void MainWindow::handleDatakeeperStatusChange(bool isOnline)
{
    mTableView->setEnabled(isOnline);
    mMainToolBar->setEnabled(isOnline);

    if(!isOnline)
        statusBar()->showMessage(tr("Can not connect to datakeeper"));
    else
        statusBar()->showMessage(tr("Connected to datakeeper"), 3000);
}

void MainWindow::createMenuAndToolBar()
{
    // Main toolbar.
    mMainToolBar = addToolBar(tr("File"));

    // Refresh action.
    const QIcon refreshIcon = QIcon(":/images/refresh.png");
    mRefreshAction = new QAction(refreshIcon, tr("&Refresh"), this);
    mRefreshAction->setShortcuts(QKeySequence::Refresh);
    mRefreshAction->setStatusTip(tr("Refresh the internments list"));
    connect(mRefreshAction, &QAction::triggered, this, &MainWindow::refreshInternments);
    mMainToolBar->addAction(mRefreshAction);

    // Open device view.
    const QIcon openInternmentViewIcon = QIcon(":/images/fullscreen.png");
    mOpenInternmentViewAction = new QAction(openInternmentViewIcon, tr("&Open internment view"), this);
    mOpenInternmentViewAction->setShortcuts(QKeySequence::Open);
    mOpenInternmentViewAction->setStatusTip(tr("Open the view for the current internment"));
    connect(mOpenInternmentViewAction, &QAction::triggered, this, &MainWindow::openInternmentView);
    mMainToolBar->addAction(mOpenInternmentViewAction);
    mOpenInternmentViewAction->setEnabled(false);

    // Open alarms setup.
    const QIcon alarmsSetupIcon = QIcon(":/images/alarms_setup.png");
    mOpenAlarmsSetupAction = new QAction(alarmsSetupIcon, tr("Open a&larms setup dialog"), this);
    mOpenAlarmsSetupAction->setStatusTip(tr("Open the alarms setup dialog"));
    connect(mOpenAlarmsSetupAction, &QAction::triggered, this, &MainWindow::openAlarmsSetup);
    mMainToolBar->addAction(mOpenAlarmsSetupAction);
    mOpenAlarmsSetupAction->setEnabled(false);

    // Request report.
    const QIcon requestReportIcon = QIcon(":/images/report.png");
    mRequestReportAction = new QAction(requestReportIcon, tr("Request the internment's report."), this);
    mRequestReportAction->setStatusTip(tr("Requests a report for the selected internment."));
    connect(mRequestReportAction, &QAction::triggered, this, &MainWindow::openRequestReportDialog);
    mMainToolBar->addAction(mRequestReportAction);
    mRequestReportAction->setEnabled(false);

    // Show only internments with alarms.
    const QIcon internmentsWithAlarmsIcon = QIcon(":/images/siren.png");
    mShowInternmentsWithAlarm = new QAction(internmentsWithAlarmsIcon, tr("Show only internme&nts with alarms"), this);
    mShowInternmentsWithAlarm->setStatusTip(tr("Show only internments with alarms"));
    connect(mShowInternmentsWithAlarm, &QAction::triggered, this, &MainWindow::showInternmentsWithAlarm);
    mMainToolBar->addAction(mShowInternmentsWithAlarm);
    mShowInternmentsWithAlarm->setEnabled(true);
    mShowInternmentsWithAlarm->setCheckable(true);

    // Menu bar.
    QAction * configureMqtt = new QAction(tr("Configure MQTT broker..."), this);
    mQuitAction = new QAction(tr("&Quit..."), this);
    QAction *aboutAction = new QAction(tr("&About"), this);
    QAction *aboutQtAction = new QAction(tr("&About Qt"), this);

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(configureMqtt);
    fileMenu->addSeparator();
    fileMenu->addAction(mQuitAction);

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(aboutQtAction);

    connect(configureMqtt, &QAction::triggered, this, &MainWindow::configureMqtt);
    connect(mQuitAction, &QAction::triggered, this, &MainWindow::confirmExit);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::about);
    connect(aboutQtAction, &QAction::triggered, qApp, &QApplication::aboutQt);
}
