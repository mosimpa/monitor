/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef DEVICEDATAWIDGET_H
#define DEVICEDATAWIDGET_H

#include <QDialog>
#include <QPixmap>
#include <qwt_series_data.h>
#include <qwt_point_data.h>

#include <stdint.h>

#include <mosimpaqt/definitions.h>
#include <mosimpaqt/readsparser.h>

#include "internmentsmodel.h"
#include "datakeeperqueriesparser.h"

class QwtPlotCurve;
class QLabel;
class QwtPlotMarker;
class DataEngine;

class QREncode;

using namespace MoSimPa;

namespace Ui {
class DeviceDataWidget;
}

class DeviceDataWidget : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceDataWidget(DataEngine * dataEngine, const InternmentsModel::InternmentData & data, QWidget *parent = nullptr);
    ~DeviceDataWidget();

    void setInformation(const QString &patient, const QString &gender, const QString &bed, const QString &deviceId);

    void addOxigenSaturation(QVector<struct DatakeeperQueriesParser::SpO2> spo2);
    void addHeartRate(QVector<struct DatakeeperQueriesParser::HeartRate> heartRate);
    void addBodyTemperature(QVector<struct DatakeeperQueriesParser::BodyTemperature> bodyTemp);

    void addData(const ReadsParser & parser);

    void closeEvent(QCloseEvent * event);

    int32_t historyMinutes() { return mHistoryMinutes; }

signals:
    void closed(const int32_t internmentId);
    void requestHistory(db_id_t internmentId, int32_t fromTime, int32_t numOfSamples);

private slots:
    void setQr(const QPixmap & image);

private:
    void applyCurrentThresholds(DatakeeperQueriesParser::AlarmsThresholds thresholds);
    void addOxigenSaturation(const time_t time, const spo2_t spo2);
    void addHeartRate(const time_t time, const heart_rate_t heartRate);
    void addBodyTemperature(const time_t time, const body_temp_t temp);
    static const inline QString redBackgroundStyleSheet() { return QStringLiteral("QLabel { background-color : red; color : white; }"); }
    static const inline QString greenBackgroundStyleSheet() { return QStringLiteral("QLabel { background-color : green; color : white; }"); }

    void prepareHorizontalMarker(QwtPlotMarker * marker, double y, QColor color, int width);

    InternmentsModel::InternmentData mData;
    Ui::DeviceDataWidget *ui;

    int32_t mHistoryMinutes;

    QwtPlotCurve * mOxigenSatCurve;
    QwtPlotCurve * mHeartBeatsCurve;
    QwtPlotCurve * mBodyTemperatureCurve;

    QwtPlotMarker * mOxigenLowThresholdMarker;
    QwtPlotMarker * mHeartRateLowThresholdMarker;
    QwtPlotMarker * mHeartRateHighThresholdMarker;
    QwtPlotMarker * mBodyTempLowThresholdMarker;
    QwtPlotMarker * mBodyTempHighThresholdMarker;

    QVector<double> mOxigenTimeData;
    QVector<double> mOxigenData;
    QVector<double> mHeartRateTimeData;
    QVector<double> mHeartRateData;
    QVector<double> mBodyTemperatureTimeData;
    QVector<double> mBodyTemperatureData;

    QREncode * mQR;
    DataEngine * mDataEngine;
    DatakeeperQueriesParser::AlarmsThresholds mThresholds;
};

#endif // DEVICEDATAWIDGET_H
