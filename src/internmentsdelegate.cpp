#include <QtWidgets>
#include <QDebug>

#include "internmentsmodel.h"
#include "datakeeperalarmsparser.h"
#include "internmentsdelegate.h"

InternmentsDelegate::InternmentsDelegate(QObject *parent) : QStyledItemDelegate(parent)
{
    _checkMarkIcon.addFile(":/images/check_mark.png");

    QPixmap battery(":/images/battery_alert.png");
    QPixmap loc(":/images/location_not_found.png");
    QImage image(battery.width() + _gap + loc.width(), battery.height(), QImage::Format_ARGB32_Premultiplied);
    image.fill(QColor(Qt::transparent));
    QPixmap pixmap = QPixmap::fromImage(image);
    QPainter paint(&pixmap);
    paint.drawPixmap(0, 0, battery);
    paint.drawPixmap(battery.width() + _gap, 0, loc);
    QIcon icon(pixmap);
    _batteryAndLocationIcon.addPixmap(pixmap);

    _batteryIcon.addPixmap(battery);
    _locationIcon.addPixmap(loc);
}

void InternmentsDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if((index.column() == InternmentsModel::Columns::LocDesc) ||
       (index.column() == InternmentsModel::Columns::DeviceMAC) ||
       (index.column() == InternmentsModel::Columns::SpO2) ||
       (index.column() == InternmentsModel::Columns::HR) ||
       (index.column() == InternmentsModel::Columns::BT))
    {
        const int LINE_WIDTH = 6;

        // Keep the coordinates to draw the line after drawing the data.
        QPoint topLeft = option.rect.topLeft();
        topLeft.rx() += LINE_WIDTH/2;
        QPoint bottomLeft = option.rect.bottomLeft();
        bottomLeft.rx() += LINE_WIDTH/2;

        // Reduce the rect in which the data will be displayed to avoid collisions.
        QRect newRect = option.rect;
        newRect.setLeft(option.rect.x() + LINE_WIDTH);

        QStyleOptionViewItem newOption = option;
        newOption.rect = newRect;

        QStyledItemDelegate::paint(painter, newOption, index);

        // Paint the border.
        QPen pen(QColor("#000000"));
        pen.setWidth(LINE_WIDTH);
        painter->setPen(pen);
        painter->drawLine(topLeft, bottomLeft);

        return;
    }

    if(index.column() == InternmentsModel::Columns::DeviceAlarms)
    {
        InternmentsModel::AlarmFlag alarmType = qvariant_cast<InternmentsModel::AlarmFlag>(index.data(Qt::DisplayRole));

        if(alarmType == InternmentsModel::AlarmFlag::NoAlarm)
        {
            _checkMarkIcon.paint(painter, option.rect);
        }
        else if((alarmType & InternmentsModel::AlarmFlag::BatteryLow) &&
                (alarmType & InternmentsModel::AlarmFlag::DeviceMissing))
        {
            _batteryAndLocationIcon.paint(painter, option.rect);
        }
        else if(alarmType & InternmentsModel::AlarmFlag::BatteryLow)
        {
            _batteryIcon.paint(painter, option.rect);
        }
        else if(alarmType & InternmentsModel::AlarmFlag::DeviceMissing)
        {
            _locationIcon.paint(painter, option.rect);
        }

        return;
    }

    QStyledItemDelegate::paint(painter, option, index);
}

void InternmentsDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);

    // Avoid the selection to hide the background color in columns with alarms.
    if(((index.column() >= InternmentsModel::Columns::SpO2) &&
        (index.column() <= InternmentsModel::Columns::BT)) ||
       (index.column() == InternmentsModel::Columns::DeviceMAC))
        option->state = option->state & ~QStyle::State_Selected;
}
