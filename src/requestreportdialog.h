#ifndef REQUESTREPORTDIALOG_H
#define REQUESTREPORTDIALOG_H

#include <QDialog>

#include "internmentsmodel.h"

namespace Ui {
class RequestReportDialog;
}

class RequestReportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RequestReportDialog(const InternmentsModel::InternmentData &data, QWidget *parent = nullptr);
    ~RequestReportDialog();

signals:
    void reportRequested(db_id_t internmentId, time_t formTime, time_t toTime);

private slots:
    void checkTimeSpan();
    void confirmRequest();

private:
    Ui::RequestReportDialog *ui;
    const db_id_t _internmentId;
};

#endif // REQUESTREPORTDIALOG_H
